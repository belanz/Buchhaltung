# Buchhaltung

## Description / Beschreibung

This software library provides basic functions for bookkeeping (*Buchhaltung* in German) for very small, limited liability
capital companies (GmbH, UG) in Germany. Symbol names in the source code are in English but the majority of the in-code
documentation is in German, using the legal German nomenclature for business reports and referring to German
trade laws.

Diese Software-Bibliothek enthält Quellcode für Buchhaltungsfunktionen die helfen können die gesetzliche Buchführungspflicht
von **Kleinstkapitalgesellschaften** (siehe [§ 267a Handelsgesetzbuch](https://www.gesetze-im-internet.de/hgb/__267a.html)), 
die nach dem Gesamtkostenverfahren (GKV) Buch führen, zu erfüllen. Die Software ist nicht geeignet für
eingetragene Genossenschaften (eG), eingetragene Vereine (e.V.), kapitalmarktorientierte Kapitalgesellschaften,
Aktiengesellschaften oder Gesellschaften die Teil eines Konzerns sind.
Die Datenstrukturen orientieren sich an den Vorgaben des Handelsgesetzbuches und des GmbH-Gesetzes.

Gebucht kann mit den Konten die von der benutzten Bibliothek [DEGAAP](https://gitlab.com/belanz/DEGAAP)
unterstützt werden, aktuell sind das IKR, DATEV SKR 03, DATEV SKR 04 und XBRL DE-GAAP.

## Disclaimer / Nutzungsbedingungen

See [license text](LICENSE.md).

Die Urheber dieses Quellcodes erheben keinen Anspruch auf die Eignung für jedwede Nutzungszwecke.
Der Anwender der Software, die aus diesem Quellcode hervorgeht, ist verpflichtet, die Eignung dieses Quellcodes
für den Nutzungszweck im Voraus zu prüfen. Der Anwender verzichtet gegenüber den Urhebern dieses Quellcodes
auf jegliche Haftungsansprüche für Schäden die aus der Anwendung der hervorgegangenen Software entstehen
können. Siehe [Lizenztext](LICENSE.md).

## Usage

The `Ledger` type is the main interface to the Buchhaltung API. To create a business report, create an instance of Ledger, add relevant accounting data to via the instance properties, then call its function _`generateReport(...)`_. The resulting `Report` instance, as well as the Ledger instance, can be serialized to human-readable text.

Buchhaltung uses the [DEGAAP](https://gitlab.com/belanz/DEGAAP) package and requires a `DEGAAPEnvironment` instance to be passed to _generateReport()_. Use the DEGAAPEnvironment instance to convert between the accounts of the XBRL DE-GAAP charts and the widely used DATEV SKR 03/04, IKR account charts.

An overview of the software architecture can be found [here](Documentation/architecture.md). For API documentation, refer to the DocC documentation, viewable in Xcode after building the library.

## Command-Line Tool

The Swift package also includes a target for building a [command-line tool](Documentation/commandline.md) that processes Buchhaltung input files according to the command arguments.

## Versioning

Note that versions with major version number "0" are initial development versions with instable API and file formats.
