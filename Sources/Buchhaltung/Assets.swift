// Copyright 2020-2025 Kai Oezer
// swiftlint:disable file_length nesting

import Foundation
import IssueCollection
import TOMLike

/**
	German: Anlagen
 */
public struct Assets : Codable, Sendable
{
	public enum Error : Swift.Error
	{
		case alreadyExists
	}

	public typealias RegistrationID = String

	/// German: Umlaufvermögen
	///
	///	Forderungen, Geldbestand (Kassenbestand, Kontoguthaben)
	public var currentAssets = Set<CurrentAsset>()

	/// German: Anlagevermögen
	///
	/// Gegenstände, die dauernd dem Geschäftsbetrieb dienen
	/// ([§ 247 HGB](https://www.gesetze-im-internet.de/hgb/__247.html))
	/// wie, z.B., Grundstücke, Bauten, Fahrzeuge und andere Sachanlagen
	public var fixedAssets = Set<FixedAsset>()

	private var _registrationCounter : Int = 0

	public init()
	{
	}

	enum CodingKeys : CodingKey
	{
		case assets
		case registrationCounter
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.fixedAssets = try container.decode(Set<FixedAsset>.self, forKey: .assets)
		_registrationCounter = try container.decode(Int.self, forKey: .registrationCounter)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(fixedAssets, forKey: .assets)
		try container.encode(_registrationCounter, forKey: .registrationCounter)
	}

	/// - parameter asset: the asset to add, with the preferred registration ID
	/// - returns: the registration ID actually assigned to the asset
	@discardableResult
	public mutating func add(fixedAsset : FixedAsset) -> RegistrationID
	{
		let id = _nextAvailableID(preferred: fixedAsset.registrationID)
		let newAsset = fixedAsset.copy(withID: id)
		fixedAssets.insert(newAsset)
		return id
	}

	public mutating func add(fixedAssets newAssets : Set<FixedAsset>)
	{
		fixedAssets.formUnion(newAssets)
	}

	/// Finds the inventory asset with registration ID equal to the given asset and applies the property values from the given asset.
	/// - returns: `true` if an asset with the same registration ID was found and its properties updated, `false` otherwise
	public mutating func update(fixedAsset : FixedAsset) -> Bool
	{
		fixedAssets.update(with: fixedAsset) != nil
	}

	@discardableResult
	public mutating func remove(fixedAsset : FixedAsset) -> Bool
	{
		fixedAssets.remove(fixedAsset) != nil
	}

	public mutating func removeAllFixedAssets()
	{
		fixedAssets.removeAll()
	}

	public mutating func add(currentAsset : CurrentAsset)
	{
		currentAssets.insert(currentAsset)
	}

	mutating func add(currentAssets newAssets : Set<CurrentAsset>)
	{
		currentAssets.formUnion(newAssets)
	}

	@discardableResult
	public mutating func remove(currentAsset : CurrentAsset) -> Bool
	{
		currentAssets.remove(currentAsset) != nil
	}

	public mutating func removeAllCurrentAssets()
	{
		currentAssets.removeAll()
	}

	private mutating func _nextAvailableID(preferred : RegistrationID?) -> RegistrationID
	{
		if let preferred, !fixedAssets.contains(FixedAsset(registrationID: preferred))
		{
			return preferred
		}
		while fixedAssets.contains(FixedAsset(registrationID: RegistrationID(_registrationCounter)))
		{
			_registrationCounter += 1
		}
		return RegistrationID(_registrationCounter)
	}

}

extension Assets : Equatable
{
	public static func == (lhs : Assets, rhs : Assets) -> Bool
	{
		(lhs.currentAssets == rhs.currentAssets)
			&& (lhs.fixedAssets == rhs.fixedAssets)
	}
}

extension Assets
{
	public init(from archive : String, issues : inout IssueCollection) throws
	{
		self = try TOMLikeCoder.decode(Self.self, from: archive, issues: &issues)
	}

	public func archive() throws -> String
	{
		try TOMLikeCoder.encode(self)
	}
}

public protocol AssetType : CaseIterable, Codable, Comparable, Equatable, CustomStringConvertible {}

public protocol Asset : Identifiable, Codable, Hashable, Comparable, Equatable, CustomStringConvertible
{
	associatedtype T : AssetType

	var type : T {get set}
	var name : String {get set}
}

extension Assets
{
	/// The classification of fixed assets, in order of increasing liquidity.
	///
	/// Kategorien des Anlagevermögens, nach steigender Liquidität (Umwandelbarkeit in Bargeld, Wiederverkäuflichkeit) sortiert.
	///
	/// **GWG**: Vermögensgegenstände die *beweglich*, *abnutzbar* und *selbstständig* nutzbar sind, sind geringwertige
	/// Vermögensgegenstände (GWG) die bei einem Einzelanschaffungswert (nach Abzug der Umsatzsteuer) **unter 800 Euro**
	/// für das Geschäftsjahr der Anschaffung **komplett abschreibbar** sind
	/// ( [§ 6 EStG](https://www.gesetze-im-internet.de/estg/__6.html) ).
	///
	/// **GWG Sammelposten**: Sofern in einem Jahr ein Pool gebildet wird, sind alle in einem Wirtschaftsjahr angeschafften
	/// oder hergestellten geringwertigen Wirtschaftsgüter (GWG), die unter die Grenze von 1.000 € fallen, darin aufzunehmen
	/// [§ 6 Abs. 2a Satz 5 EStG](https://www.gesetze-im-internet.de/estg/__6.html) . Es ist nicht zulässig einen oder mehrere
	/// angeschaffte oder hergestellte Wirtschaftsgüter sofort abzuschreiben und andere in den Sammelposten einzustellen.
	/// Somit ist in jedem Wirtschaftsjahr ein neuer Pool zu bilden. Seit 2010 besteht ein **Wahlrecht zur Sofortabschreibung
	/// für GWG** je Wirtschaftsjahr. Dafür darf der Preis eines GWG jedoch nicht mehr als 800 € netto (Stand: 2018) betragen.
	/// Hochwertigere GWG bis zu einem Preis von 1.000 €  (Stand: 2018) müssen in einem Sammelposten abgeschrieben werden.
	public enum FixedAssetType : Int, AssetType, Sendable
	{
		/// German: immaterielles Anlagevermögen
		///
		/// Kosten für Herstellung und Entwicklung eines immateriellen Gutes kommen hier rein.
		/// Die Kosten für die Forschung (Suche nach neuen wissenschaftlichen oder technischen Erkenntnissen)
		/// sind keine Anlage sondern eine Ausgabe.
		/// [§ 255 Abs. 2 und 2a HGB](https://www.gesetze-im-internet.de/hgb/__255.html)
		///
		/// Nicht aufgenommen werden dürfen selbst geschaffene Marken, Drucktitel, Verlagsrechte, Kundenlisten
		/// oder vergleichbare immaterielle Vermögensgegenstände des Anlagevermögens.
		/// [§ 248 Abs. 2 HGB](https://www.gesetze-im-internet.de/hgb/__248.html)
		case intangible = 0

		/// Steuerrechtlich darf ein selbsterstellter immaterieller Vermögensgegenstand nicht als veräußerbares
		/// Wirtschaftsgut in die Steuerbilanz aufgenommen ("aktiviert") werden. Nur eingekaufte immaterielle
		/// Güter, dürfen in die Steuerbilanz als Aktiva aufgenommen werden.
		/// [§ 5 Abs. 2 EStG](https://www.gesetze-im-internet.de/estg/__5.html)
		/// Ein Eintrag in der Handelsbilanz ohne Gegenstück in der Steuerbilanz sorgt für eine Diskrepanz die
		/// durch einen Eintrag als aktive oder passive latente Steuer ausgeglichen werden kann.
		/// Zum Beispiel wenn Entwicklungkskosten als Anlage bilanziert werden
		/// ([Beispiel](https://www.rechnungswesen-portal.de/Fachinfo/Verbindlichkeiten/latente-Steuern.html)).
		case intangibleSelfMade

		/// German: Grund und Boden
		case land

		/// German: Bauten
		case building

		/// machinery or technical equipment
		///
		/// German: Technische Anlagen und Maschinen
		case machinery

		/// German: Betriebsausstattung
		case factoryEquipment
		/// German: Geschäftsausstattung
		case officeEquipment
		/// equipment other than for office or factory
		/// German: Sonstige Betriebs- und Geschäftsausstattung
		case equipment

		/// German: PKW Fuhrpark
		case passengerVehicle
		/// German: LKW, Nutzfahrzeuge
		case commercialVehicle
		/// German: Sonstige Transportmittel
		case vehicle

		/// long-term financial assets
		///
		/// German: Finanzanlagen
		case financial

		case other

		public static func < (lhs: Assets.FixedAssetType, rhs: Assets.FixedAssetType) -> Bool
		{
			lhs.rawValue < rhs.rawValue
		}

		public var description: String
		{
			Self._stringMapping[self]!
		}

		init?(from string : String)
		{
			guard let key = Self._stringMapping.first(where:{ $0.1 == string })?.0 else { return nil }
			self = key
		}

		private static let _stringMapping : [Self : String] = [
			.intangible : "intangible",
			.intangibleSelfMade : "intangible-self-made",
			.land : "land",
			.building : "building",
			.machinery : "machinery",
			.equipment : "equipment",
			.officeEquipment : "equipment_office",
			.factoryEquipment : "equipment_factory",
			.vehicle : "vehicle",
			.passengerVehicle : "vehicle_passenger",
			.commercialVehicle : "vehicle_commercial",
			.financial : "financial",
			.other : "other"
		]

		public var isTangible : Bool
		{
			self != .financial && self != .intangible && self != .intangibleSelfMade
		}

		public var isIntangible : Bool
		{
			self == .intangible || self == .intangibleSelfMade
		}

		public var isFinancial : Bool
		{
			self == .financial
		}
	}

	/// Schedule of depreciation of a fixed asset.
	///
	/// German: Abschreibungsplan (Absetzung für Abnutzung)
	public struct DepreciationSchedule : Codable, Equatable, Sendable
	{
		public enum Progression : Int, Codable, Sendable
		{
			case none = 0

			/// Leistungsabhängiger Abschreibungsverlauf
			/// Nur möglich für bewegliche Vermögensgegenstände.
			case usage = 1

			case linear

			/// Degressiver Abschreibungsverlauf mit fixem Prozentsatz auf den Restwert.
			/// Höherer Wertverlust am Anfang der Nutzungsdauer, wie, z.B., bei Neuwagen.
			/// In Deutschland steuerrechtlich nicht erlaubt, in Österreich nur für Neuanschaffungen ab 2020.
			case degressive

			/// Geringwertige Wirtschaftsgüter
			///
			/// Die Anschaffungs- oder Herstellungskosten [...] von **abnutzbaren, beweglichen** Wirtschaftsgütern des Anlagevermögens,
			/// die einer selbständigen Nutzung fähig sind, können im Wirtschaftsjahr der Anschaffung, Herstellung oder Einlage des
			/// Wirtschaftsguts oder der Eröffnung des Betriebs in voller Höhe als Betriebsausgaben abgezogen werden, wenn die
			/// Anschaffungs- oder Herstellungskosten, vermindert um einen darin enthaltenen Vorsteuerbetrag [...] für das **einzelne
			/// Wirtschaftsgut 800 Euro nicht übersteigen**.
			/// [§ 6 Abs. 2 EStG](https://www.gesetze-im-internet.de/estg/__6.html)
			///
			/// Die geringwertigen Wirtschaftsgüter müssen im Inventar und Anlagenspiegel enthalten sein.
			/// Güter mit **Einzelwert unter 250 Euro** werden als Sofortaufwand (Betriebsausgaben) gebucht und nicht inventarisiert.
			case gwg

			/// This asset is pooled with other low-valued assets, with a depreciation period of 5 business years.
			///
			/// Wirtschaftsgüter mit Anschaffungswert von über 250 bis unter 1000 Euro können zu einem **Sammelposten**
			/// für abnutzbare, bewegliche Wirtschaftsgüter, die einer selbständigen Nutzung fähig sind, zusammengeführt werden.
			/// Die Abschreibung erfolgt auf 5 Geschäftsjahre, inklusive dem Anschaffungs-Geschäftsjahr.
			/// [§ 6 Abs. 2a EStG](https://www.gesetze-im-internet.de/estg/__6.html)
			case pooled
		}

		/// The number of fiscal years in which the value of the asset depreciates to zero.
		///
		/// [§ 253 Abs. 3 HGB](https://www.gesetze-im-internet.de/hgb/__253.html):
		/// Bei Vermögensgegenständen des Anlagevermögens, *deren Nutzung zeitlich begrenzt ist*, sind
		/// die Anschaffungs- oder die Herstellungskosten um planmäßige Abschreibungen zu vermindern.
		/// Der Plan muss die Anschaffungs- oder Herstellungskosten auf die Geschäftsjahre verteilen,
		/// in denen der Vermögensgegenstand voraussichtlich genutzt werden kann.
		private var _depreciationYears : UInt = 0
		public var depreciationYears : UInt
		{
			get { (progression == .gwg) ? 0 : ((progression == .pooled) ? 5 : _depreciationYears) }
			set { _depreciationYears = newValue }
		}

		public var progression : Progression

		public init(progression : Progression = .linear, depreciationYears : UInt = 1)
		{
			_depreciationYears = depreciationYears
			self.progression = progression
		}

		/// German: Buchwert
		/// - parameter date: The date for which the book value of the asset is to be calculated.
		/// - returns: the book value (residual value) of this asset
		///
		/// [§ 7 Abs. 1 EStG](https://www.gesetze-im-internet.de/estg/__7.html)
		/// Die Abnutzungsdauer wird nicht tagesgenau sondern in Monaten erfasst. Dabei werden
		/// der Anschaffungsmonat und die vollen Monate bis zur Buchwertbestimmung zum
		/// Abnutzungszeitraum gezählt. Die Aufteilung der Abschreibungen auf die Nutzungsdauer
		/// gilt nur bei einer Abnutzungsdauer über 1 Jahr. Bei einer Nutzungsdauer bis einschliesslich
		/// 1 Jahr wird sofort abgeschrieben.
		public func depreciatedValue(at date : Day, acquisitionDate : Day, acquisitionCost : Money) -> Money
		{
			guard acquisitionDate <= date else { return acquisitionCost }

			guard depreciationYears > 1 else { return FixedAsset.residualValue }
			let monthsDepreciated = 12 * (date.year - acquisitionDate.year) + (date.month - acquisitionDate.month) + (date.isEndOfMonth ? 1 : 0)
			if monthsDepreciated >= (12 * depreciationYears) { return FixedAsset.residualValue }
			// Note that the depreciation is calculated by first determining the
			// depreciation per month, then multiplying with the number of months.
			// We will obtain a different result if we calculate the book value by
			// directly multiplying the acquisition cost with the number of months
			// and dividing by the total number of months until fully depreciated.
			// This is due to performing a floating point division operation.
			let depreciationPerMonth = acquisitionCost / (12 * depreciationYears)
			let depreciation = monthsDepreciated * depreciationPerMonth
			let bookValue = max(FixedAsset.residualValue, acquisitionCost - depreciation)
			return bookValue
		}
	}

	/// Fixed asset
	///
	/// German: Anlagevermögensgegenstand
	public struct FixedAsset : Asset, Sendable
	{
		public let id : UUID
		public var registrationID : RegistrationID
		public var type : FixedAssetType
		public var name : String
		public var acquisitionDate : Day
		public var retirementDate : Day

		/// Die Anschaffungskosten ohne vorabziehbare Steuern
		public var acquisitionCost : Money = .zero

		public var depreciationSchedule = DepreciationSchedule()

		/// The residual value (or salvage value) assigned to a fully depreciated fixed asset.
		///
		/// *Restwert* (Erinnerungswert):
		/// Nach üblicher Praxis wird der Restwert einer komplett abgeschriebenen aber weiter
		/// verwendeten Sachanlage auf 1 EUR gesetzt um den Anspruch an die Vollständigkeit
		/// der Bilanz ([§ 246 HGB](https://www.gesetze-im-internet.de/hgb/__246.html))
		/// zu erfassen. Der Abgang der Sachanlage macht sich somit an der Änderung der
		/// Bilanzsumme bemerkbar (falls im Geschäftsjahr keine anderen Änderungen bei den
		/// Sachanlagen gemacht werden).
		static let residualValue : Money = 1

		public init(
			identifier : UUID? = nil,
			registrationID : RegistrationID,
			type : FixedAssetType = .other,
			name : String = "",
			acquisitionDate : Day = .distantFuture,
			acquisitionCost : Money = .zero,
			depreciationSchedule : DepreciationSchedule = DepreciationSchedule(),
			retirementDate : Day = .distantFuture
		)
		{
			self.id = identifier ?? UUID()
			self.registrationID = registrationID
			self.type = type
			self.name = name
			self.acquisitionDate = acquisitionDate
			self.acquisitionCost = acquisitionCost
			self.depreciationSchedule = depreciationSchedule
			self.retirementDate = retirementDate
		}

		func copy(withID regID : RegistrationID) -> FixedAsset
		{
			return FixedAsset(
				identifier: id,
				registrationID: regID,
				type: type,
				name: name,
				acquisitionDate: acquisitionDate,
				acquisitionCost: acquisitionCost,
				depreciationSchedule: depreciationSchedule,
				retirementDate: retirementDate
			)
		}

		// Hashable implementation

		public func hash(into hasher: inout Hasher)
		{
			hasher.combine(id)
		}

		// Comparable implementation

		public static func < (lhs : FixedAsset, rhs : FixedAsset) -> Bool
		{
			if lhs.type != rhs.type {
				return lhs.type < rhs.type
			}
			if lhs.acquisitionDate != rhs.acquisitionDate {
				return lhs.acquisitionDate < rhs.acquisitionDate
			}
			if !lhs.registrationID.isEmpty
				&& !rhs.registrationID.isEmpty
				&& (lhs.registrationID != rhs.registrationID) {
				return lhs.registrationID < rhs.registrationID
			}
			if lhs.name != rhs.name {
				return lhs.name < rhs.name
			}
			return lhs.id.uuidString < rhs.id.uuidString
		}

		// Equatable implementation

		public static func == (lhs : FixedAsset, rhs : FixedAsset) -> Bool
		{
			lhs.registrationID == rhs.registrationID
				&& lhs.type == rhs.type
				&& lhs.name == rhs.name
				&& lhs.acquisitionCost == rhs.acquisitionCost
				&& lhs.acquisitionDate == rhs.acquisitionDate
				&& lhs.depreciationSchedule == rhs.depreciationSchedule
		}

		// CustomStringConvertible implementation

		public var description: String
		{
			var typeDesc = String(describing: type)
			switch depreciationSchedule.progression
			{
				case .gwg: typeDesc.append("-gwg")
				case .pooled: typeDesc.append("-gwg-sp")
				default: break
			}
			return "# \(registrationID) - \(typeDesc) - \"\(name)\""
				+ ", acquired: \(acquisitionDate)"
				+ (retirementDate < .distantFuture ? ", retired: \(retirementDate)" : "")
				+ ", cost: \(acquisitionCost)"
		}

		/// German: Buchwert
		/// - parameter date: The date for which the book value of the asset is to be calculated.
		/// - returns: the book value (residual value) of this asset or `nil` if no depreciation data is available.
		///
		/// *Restwert* (Erinnerungswert):
		/// Nach üblicher Praxis wird der Restwert einer komplett abgeschriebenen aber weiter
		/// verwendeten Sachanlage auf 1 EUR gesetzt um den Anspruch an die Vollständigkeit
		/// der Bilanz ([§ 246 HGB](https://www.gesetze-im-internet.de/hgb/__246.html))
		/// zu erfassen. Der Abgang der Sachanlage macht sich somit an der Änderung der
		/// Bilanzsumme bemerkbar (falls im Geschäftsjahr keine anderen Änderungen bei den
		/// Sachanlagen gemacht werden).
		public func bookValue(for day : Day) -> Money
		{
			guard depreciationSchedule.progression != .none else { return acquisitionCost }
			guard retirementDate > day else { return .zero }
			return depreciationSchedule.depreciatedValue(at: day, acquisitionDate: acquisitionDate, acquisitionCost: acquisitionCost)
		}

		public func depreciatedValue(for day : Day) -> Money
		{
			depreciationSchedule.progression == .none ? acquisitionCost :
				depreciationSchedule.depreciatedValue(at: day, acquisitionDate: acquisitionDate, acquisitionCost: acquisitionCost)
		}

		public var servicePeriod : Period?
		{
			Period(start: acquisitionDate, end: retirementDate)
		}

		public var isGWG : Bool
		{
			depreciationSchedule.progression == .gwg
		}

		public var isEquipment : Bool
		{
			type == .equipment || type == .officeEquipment || type == .factoryEquipment
				|| type == .vehicle || type == .passengerVehicle || type == .commercialVehicle
		}

		var account : AccountID
		{
			switch type
			{
				case .intangible : return DEGAAPAccounts.ass_fixed_intangible
				case .intangibleSelfMade : return DEGAAPAccounts.ass_fixed_intangible_self
				case .land : return DEGAAPAccounts.ass_fixed_land
				case .building : return DEGAAPAccounts.ass_fixed_buildings
				case .machinery : return DEGAAPAccounts.ass_fixed_machinery
				case .factoryEquipment : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_equipment_factory
				case .officeEquipment : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_equipment_office
				case .equipment : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_equipment_misc
				case .passengerVehicle : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_vehicle_passenger
				case .commercialVehicle : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_vehicle_commercial
				case .vehicle : return isGWG ? DEGAAPAccounts.ass_fixed_equipment_gwg : DEGAAPAccounts.ass_fixed_vehicle_other
				case .financial : return DEGAAPAccounts.ass_fixed_financial
				case .other : return DEGAAPAccounts.ass_fixed_other
			}
		}
	}

	/// Categories of current assets, in increasing order of liquidity.
	///
	/// Kategorien des Anlagevermögens, nach steigender Liquidität (Umwandelbarkeit in Bargeld, Wiederverkäuflichkeit) sortiert.
	public enum CurrentAssetType : Int, AssetType, Sendable
	{
		/// raw materials, supply materials or consumables
		///
		/// German: Roh-, Hilfs- und Betriebsstoffe
		case material = 0

		/// work in progress, unfinished products
		///
		/// German: unfertige Erzeugnisse
		case inProgress

		/// finished goods and merchandise
		///
		/// German: fertige Erzeugnisse und Waren
		case finishedGoods

		/// receivables (from shareholders, trade, prepayments, investors, affiliated companies, business partners, etc.)
		///
		/// German: Forderungen (aus Lieferungen und Leistungen)
		case receivable

		/// German: Wertpapiere des Umlaufvermögens
		case security

		/// cash-in-hand, bank balances and cheques
		///
		/// German: Kassenbestand, Bundesbankguthaben, Guthaben bei Kreditinstituten und Schecks
		case cashEquivalent

		case other

		public static func < (lhs: Assets.CurrentAssetType, rhs: Assets.CurrentAssetType) -> Bool
		{
			lhs.rawValue < rhs.rawValue
		}

		public var description: String
		{
			Self._stringMapping[self]!
		}

		public init?(from string : String)
		{
			guard let key = Self._stringMapping.first(where:{ $0.1 == string })?.0 else { return nil }
			self = key
		}

		private static let _stringMapping : [Self : String] = [
			.material : "material",
			.inProgress : "in-progress",
			.finishedGoods : "goods",
			.receivable : "receivable",
			.security : "security",
			.cashEquivalent : "cash",
			.other : "other"
		]

	}

	/// German: Umlagevermögensgegenstand
	///
	/// Bei Vermögensgegenständen des Umlaufvermögens sind Abschreibungen vorzunehmen,
	/// um diese mit einem niedrigeren Wert anzusetzen, der sich aus einem Börsen- oder Marktpreis
	/// am Abschlussstichtag ergibt. Ist ein Börsen- oder Marktpreis nicht festzustellen und übersteigen
	/// die Anschaffungs- oder Herstellungskosten den Wert, der den Vermögensgegenständen am
	/// Abschlussstichtag beizulegen ist, so ist auf diesen Wert abzuschreiben.
	/// [§ 253 Abs. 4 HGB](https://www.gesetze-im-internet.de/hgb/__253.html)
	public struct CurrentAsset : Asset, Sendable
	{
		public var id : UUID
		public var type : CurrentAssetType
		public var name : String
		public var value : Money

		public init(type : CurrentAssetType = .other, name : String = "", value : Money = .zero)
		{
			self.id = UUID()
			self.type = type
			self.name = name
			self.value = value
		}

		public static func < (lhs : CurrentAsset, rhs : CurrentAsset) -> Bool
		{
			if lhs.type != rhs.type {
				return lhs.type < rhs.type
			}
			if lhs.value != rhs.value {
				return lhs.value > rhs.value
			}
			if lhs.name != rhs.name {
				return lhs.name < rhs.name
			}
			return lhs.id.uuidString < rhs.id.uuidString
		}

		public static func == (lhs : CurrentAsset, rhs : CurrentAsset) -> Bool
		{
			lhs.type == rhs.type
				&& lhs.name == rhs.name
				&& lhs.value == rhs.value
		}

		public var description: String
		{
			"# (\(type)): \(name), \(value)"
		}

	}

}
