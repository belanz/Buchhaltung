//  Copyright 2020-2024 Kai Oezer
// swiftlint:disable nesting

import Foundation

public struct Debt : Codable, Identifiable, Sendable
{
	public var id : UUID
	public var name : String
	public var loan : Transaction
	public var payments : [Transaction]

	public init(name : String = "", loan : Transaction = Transaction(), payments : [Transaction] = [])
	{
		self.id = UUID()
		self.name = name
		self.loan = loan
		self.payments = payments
	}

	public var dueDate : Day?
	{
		payments.last?.date
	}

	/// - returns: the total amount of payments remaining after the given date
	public func value(for date : Day) -> Money
	{
		guard date >= loan.date else { return .zero }
		guard !payments.isEmpty else { return loan.value }
		return payments.filter{ $0.date > date }.reduce(Money.zero){ $0 + $1.value }
	}

	/// - returns: Whether the debt is a long-term debt for the given date.
	///
	/// Long-term debts are due after one year from the given date.
	/// If the given date is before the loan date the test fail.
	/// If there is no due date, the debt is assumed to be long-term and the test passes.
	/// If there is a due date and the given date is after the due date, the test fails.
	public func isLongTerm(for date : Day) -> Bool
	{
		if loan.date > date { return false }
		guard let dueDate else { return true }
		if date > dueDate { return false }
		let dateComponents = date.dateComponents(to: dueDate)
		return (dateComponents.year ?? 0) >= 1
	}

	/// - returns: Whether the debt is a short-term debt for the given date.
	///
	/// Short-term debts are due within a year from the given date.
	/// If there is no due date or the given date is not in the range from loan to due date, the test fails.
	public func isShortTerm(for date : Day) -> Bool
	{
		guard let dueDate, loan.date <= date, date <= dueDate else { return false }
		let dateComponents = date.dateComponents(to: dueDate)
		return (dateComponents.year ?? 0) < 1
	}

	/// - returns: Whether the combination of loan and payment dates is valid
	public var isValid : Bool
	{
		if loan.date >= .distantFuture {
			return false
		}
		for payment in payments {
			if payment.date < loan.date {
				return false
			}
		}
		return true
	}
}

extension Debt : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(name)
		if let dueDate
		{
			hasher.combine(dueDate)
		}
	}
}

extension Debt : Comparable, Equatable
{
	public static func < (lhs : Debt, rhs : Debt) -> Bool
	{
		if let lhsDueDate = lhs.dueDate,
			let rhsDueDate = rhs.dueDate,
			lhsDueDate != rhsDueDate
		{
			return lhsDueDate < rhsDueDate
		}
		if lhs.loan.date == rhs.loan.date {
			return lhs.name < rhs.name
		}
		return lhs.loan.date < rhs.loan.date
	}

	public static func == (lhs : Debt, rhs : Debt) -> Bool
	{
		lhs.name == rhs.name && lhs.loan == rhs.loan && lhs.payments == rhs.payments
	}
}


// MARK: -

public struct Debts : Sendable
{
	public var debts = Set<Debt>()

	public init(debts : Set<Debt> = Set<Debt>())
	{
		self.debts = debts
	}

	public init(debts : [Debt])
	{
		self.debts = Set(debts)
	}

	public var count : Int
	{
		debts.count
	}

	public subscript(index : Int) -> Debt?
	{
		get {
			guard index >= 0 && index < debts.count else { return nil }
			return debts.sorted()[index]
		}
	}

	/// - returns: the total value of all long-term debts that have not been fully paid back on the given date
	public func longTermDebtsTotal(for date : Day) -> Money
	{
		debts.filter{$0.isLongTerm(for: date)}.reduce(Money.zero){ $0 + $1.value(for:date) }
	}

	/// - returns: the total value of all short-term debts that have not been fully paid back on the given date
	public func shortTermDebtsTotal(for date : Day) -> Money
	{
		debts.filter{$0.isShortTerm(for: date)}.reduce(Money.zero){ $0 + $1.value(for:date) }
	}
}

extension Debts : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(debts)
	}
}

extension Debts : Comparable
{
	public static func < (lhs : Debts, rhs : Debts) -> Bool
	{
		return lhs.debts.count < rhs.debts.count
	}

	public static func == (lhs : Debts, rhs : Debts) -> Bool
	{
		lhs.debts == rhs.debts
	}
}

extension Debts : Codable
{
	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.singleValueContainer()
		try container.encode(debts.sorted())
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.singleValueContainer()
		let debts = try container.decode([Debt].self)
		self.init(debts: debts)
	}
}
