[magic]
myebilanz=true
guid=E949F34C-6E81-4297-B5EA-FF9399764D9C

[general]
; Replace the fields below with the contact data of the persons...
; ... responsible for transferring this report to the tax authority,
Transferdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>
; ... and for supplying the report data.
Nutzdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>

[cert]
; Replace <path> below with the absolute path (in Windows notation) to the Elster certificate file.
file=<path to certification file>
; Replace <password> below with the password for your Elster account.
pin=<password>

[csv]
; Replace <path> below with the absolute path (in Windows notation) to the folder containing the CSV account totals file.
filename=#FILENAME#
delimiter=;
fieldKto=1
fieldValue=2
fieldValueDebit=0
fieldValueCredit=0
fieldXBRL=3
fieldName=4

[period]
balSheetClosingDate=#PERIOD_END#

[report]
reportType=JA
reportStatus=E
revisionStatus=E
reportElements=#REPORT_ELEMENTS#
statementType=E
statementTypeTax=-GHB
incomeStatementendswithBalProfit=#WITH_BALANCE_PROFIT#
accountingStandard=HAOE
specialAccountingStandard=K
incomeStatementFormat=GKV
consolidationRange=EA
taxonomy=#TAXONOMY#
MicroBilG=1

[company]
#COMPANY_INFO#

[xbrl]
; Special account "0".
; The value is automatically set to the total of the income (GuV) accounts.
de-gaap-ci:bs.eqLiab.equity.netIncome=0
; The values for the following accounts are in the CSV file.
#ACCOUNTS#

[bal]
#ASSET_CHANGES#

[ini]
AutoSum=1
