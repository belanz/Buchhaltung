// Copyright 2020-2024 Kai Oezer

import IssueCollection

public protocol Archivable
{
	init(fromArchive archive : String, issues : inout IssueCollection) throws

	func archive() throws -> String
}
