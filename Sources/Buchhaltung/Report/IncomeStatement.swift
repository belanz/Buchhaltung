//  Copyright 2020-2024 Kai Oezer

import Foundation

/**
	German: Gewinn- und Verlustrechnung (GuV)

	[§ 275 Abs. 5 HGB](https://www.gesetze-im-internet.de/hgb/__275.html)

	Die Gewinn- und Verlustrechnung ist in __Staffelform__ nach dem Gesamtkostenverfahren
	oder dem Umsatzkostenverfahren aufzustellen. Staffelform bedeutet, dass alle Erträge und
	Aufwendungen des Unternehmens *untereinander* aufgelistet werden, wie ein Kassenbon.
	Ausgehend vom Umsatz des Unternehmens kann dann über mehrere Zwischenschritte das
	Jahresergebnis des Unternehmens berechnet werden.

	In der Staffelform beginnt man mit den Verkaufserlösen (aus hergestellten oder gehandelten
	Produkten oder Dienstleistungen die man erbringt),
	zieht den _Materialaufwand_ ab,
	zieht den _Personalaufwand_ ab,
	zieht sonstige _Baraufwendungen_ ab,
	erhält daraus das _Betriebsergebnis_,
	zieht _Zinsaufwendungen_ ab und addiert _Zinserträge_,
	erhält somit das  Finanzergebnis, dem _Gewinn vor Steuern_,
	zieht die Steuern ab und erhält den _Gewinn nach Steuern_.

	__Kleinstkapitalgesellschaften__ können die Gewinn- und Verlustrechnung wie folgt darstellen:
	* Umsatzerlöse
	* sonstige Erträge
	* Materialaufwand
	* Personalaufwand
	* Abschreibungen
	* sonstige Aufwendungen
	* Steuern
	* Jahresüberschuss/Jahresfehlbetrag

	[§ 277 HGB](https://www.gesetze-im-internet.de/hgb/__277.html)
	* Als Umsatzerlöse sind die Erlöse aus dem Verkauf und der Vermietung oder Verpachtung
		von Produkten sowie aus der Erbringung von Dienstleistungen der Kapitalgesellschaft
		nach Abzug von Erlösschmälerungen und der Umsatzsteuer sowie sonstiger direkt mit dem
		Umsatz verbundener Steuern auszuweisen.
	* Als Bestandsveränderungen sind sowohl Änderungen der Menge als auch solche des Wertes
		zu berücksichtigen; Abschreibungen jedoch nur, soweit diese die in der Kapitalgesellschaft
		sonst üblichen Abschreibungen nicht überschreiten.
  * Außerplanmäßige Abschreibungen nach [§ 253 Absatz 3 Satz 5 und 6](https://www.gesetze-im-internet.de/hgb/__253.html)
		sind jeweils gesondert auszuweisen oder im Anhang anzugeben. Erträge und Aufwendungen
		aus Verlustübernahme und auf Grund einer Gewinngemeinschaft, eines Gewinnabführungs-
		oder eines Teilgewinnabführungsvertrags erhaltene oder abgeführte Gewinne sind jeweils
		gesondert unter entsprechender Bezeichnung auszuweisen.
	* Erträge aus der Abzinsung sind in der Gewinn- und Verlustrechnung gesondert unter dem Posten
		„Sonstige Zinsen und ähnliche Erträge“ und Aufwendungen gesondert unter dem Posten
		„Zinsen und ähnliche Aufwendungen“ auszuweisen. Erträge aus der Währungsumrechnung sind
		in der Gewinn- und Verlustrechnung gesondert unter dem Posten „Sonstige betriebliche Erträge“
		und Aufwendungen aus der Währungsumrechnung gesondert unter dem Posten
		„Sonstige betriebliche Aufwendungen“ auszuweisen.
*/
public struct IncomeStatement : Codable, Equatable, Sendable
{
	public enum Format : Int, Codable
	{
		/// vereinfachtes Format für Kleinstkapitalgesellschaften (nach MicroBilG)
		case micro = 0
		/// Gesamtkostenverfahren
		case totalCost = 1
		/// Umsatzkostenverfahren
		case costOfSales = 2
	}

	public var format : Format { .micro }

	public struct Results : Codable, Equatable, Sendable
	{
		/// German: Umsatzerlöse
		public var earnings : Money = .zero

		/// German: sonstige Erträge
		public var otherEarnings : Money = .zero

		/// German: Materialaufwand
		public var materialCosts : Money = .zero

		/// German: Personalaufwand
		public var salaries : Money = .zero

		/// German: Abschreibungen
		public var depreciations : Money = .zero

		/// German: sonstige Aufwendungen
		public var otherExpenses : Money = .zero

		/// German: Steuern
		///
		/// * Körperschaftsteuer (+ Soli)
		/// * Gewerbesteuer
		/// * Steuernachzahlungen
		/// * Steuererstattungen
		/// * Auflösungen von Steuerrückstellungen
		/// * Veränderungen in latenten Steuern
		///
		/// Nach [§ 274 Abs. 2 Satz 3 HGB](https://www.gesetze-im-internet.de/hgb/__274.html)
		/// ist der Aufwand oder Ertrag aus der Veränderung [bilanzierter latenter Steuern](x-source-tag://bs-latente-steuern)
		/// in der Gewinn- und Verlustrechnung gesondert (als *davon*-Vermerk oder als Angabe
		/// im Anhang) unter dem Posten *Steuern vom Einkommen und vom Ertrag* auszuweisen
		/// (gilt aber nicht für Kleinstkapitalgesellschaften).
		public var tax : Money = .zero

		/// German: Jahresüberschuss/Jahresfehlbetrag
		public var profitLoss : Money
		{
			earnings
			+ otherEarnings
			- materialCosts
			- salaries
			- depreciations
			- otherExpenses
			- tax
		}
	}

	public var results = Results()

	///	[§ 265 Abs. 2 HGB](https://www.gesetze-im-internet.de/hgb/__265.html)
	///	In der Bilanz sowie in der Gewinn- und Verlustrechnung ist __zu jedem Posten der entsprechende__
	///	__Betrag des vorhergehenden Geschäftsjahrs__ anzugeben. Sind die Beträge nicht vergleichbar,
	///	so ist dies im Anhang anzugeben und zu erläutern. Wird der Vorjahresbetrag angepaßt, so ist auch
	///	dies im Anhang anzugeben und zu erläutern.
	public var previousYearResults = Results()
}
