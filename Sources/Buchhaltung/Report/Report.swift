// Copyright 2020-2024 Kai Oezer

import Foundation
import IssueCollection
import TOMLike
import DEGAAP

/**
 The business report structure for very small, limited-liability capital companies.

 German: Bericht (z.B., Jahresbericht)

 Strukturiert für Kleinstkapitalgesellschaften
 ([§ 267a HGB](https://www.gesetze-im-internet.de/hgb/__267a.html)).

 Auf Deutsch und in Euro.
 [§ 244 HGB](https://www.gesetze-im-internet.de/hgb/__244.html)
*/

public enum ReportType : String, CaseIterable, Codable, Equatable, Sendable
{
	case eb = "Eröffnungsbilanz"

	/// Öffentliche Bilanz nach Handelsgesetzbuch.
	/// Public Report
	///
	/// Eine Handelsbilanz wird im Interesse des Unternehmens zur Information der
	/// Unternehmensführung, Kreditgeber, Anteilseigner, Mitarbeiter, Lieferanten und
	/// Kunden erstellt.
	/// Für die Handelsbilanz ist eine Struktur vorgeschrieben:
	/// [§ 266 HGB](https://www.gesetze-im-internet.de/hgb/__266.html)
	case trade = "Handelsbilanz"

	/// Bilanz nach Steuergesetzen.
	///
	/// Eine Steuerbilanz entspricht steuerrechtlichen Vorschriften und wird im
	/// Gegensatz zur Handelsbilanz extra für das Finanzamt erstellt und über
	/// elster.de übermittelt damit das Finanzamt die Beträge für Einkommensteuer,
	/// Gewerbesteuer und Körperschaftssteuer ermitteln kann.
	/// Eine Struktur ist für die Steuerbilanz nicht direkt vorgeschrieben. Aus
	/// [§ 5 Abs. 1 Satz 1 EStG](https://www.gesetze-im-internet.de/estg/__5.html)
	/// ergibt sich aber, dass die Steuerbilanz auf der Handelsbilanz basiert,
	/// mit Anpassungen an steuerrechtliche Anforderungen.
	case fiscal = "Steuerbilanz"

	case ub = "Umwandlungsbilanz"
	case lab = "Liquidationsanfangsbilanz"
	case ab = "Aufgabebilanz" // Am Ende der Liquidation
}

public struct ReportTitle : Equatable, Codable, CustomStringConvertible, Sendable
{
	public var string : String

	public init(_ title : String?)
	{
		self.string = title ?? ""
	}

	public var description: String { string }
}

public enum ReportError : Error, CustomStringConvertible
{
	case invalidReportType(ReportType)
	case invalidBaseReportType(ReportType)
	case invalidChart(String)

	public var description: String
	{
		switch self
		{
			case .invalidReportType(let type): return "Invalid report type. Expected \(type.rawValue)."
			case .invalidBaseReportType(let type): return "Invalid base report type. Expected \(type.rawValue)."
			case .invalidChart(let message) : return message
		}
	}
}

public struct Report : Equatable, Sendable
{
	public var title : ReportTitle
	public internal(set) var type : ReportType
	public internal(set) var taxonomy : DEGAAPTaxonomyVersion
	public internal(set) var period : Period
	public internal(set) var previousPeriod : Period?
	public internal(set) var inventory : Inventory
	public internal(set) var incomeStatement : IncomeStatement
	public internal(set) var balanceSheet : BalanceSheet
	public internal(set) var notesBelowBalanceSheet : NotesBelowBalanceSheet
	public internal(set) var notes : Notes
	public internal(set) var accountTotals : AccountTotals

	/// accounts for *Steuerlicher Betriebsvermögensvergleich*
	public internal(set) var bvv : AccountTotals?

	/// account for *Ergebnisverwendung*
	public internal(set) var incomeUse : AccountTotals?

	/// Although a financial report is based, among other things, on the inventory, published reports do not include the inventory.
	public var exportsInventory = false
}

extension Report
{
	static func defaultTitle(for type : ReportType, period : Period) -> String
	{
		switch type
		{
			case .fiscal, .trade : return "Jahresabschluss \(period.year)"
			default: return "\(type.rawValue) \(period.start)"
		}
	}
}

extension Report : Codable
{
	enum Format : Int, Codable, TOMLikeEmbeddable
	{
		case v1 = 1

		public func tomlikeEmbedded() throws -> String
		{
			String(self.rawValue)
		}

		public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
		{
			guard let content = lines.first?.content,
				let version = Int(content),
				let f = Format(rawValue:version)
				else { throw BHCodingError.invalidArchiveFormat }
			self = f
		}
	}

	enum CodingKeys : String, CodingKey
	{
		case format = "format"
		case title = "title"
		case type = "type"
		case taxonomy = "taxonomy"
		case period = "period"
		case previousPeriod = "previous_period"
		case inventory = "inventory"
		case incomeStatement = "income_statement"
		case balanceSheet = "balance_sheet"
		case notesBelowBalanceSheet = "balance_sheet_notes"
		case notes = "notes"
		case accountTotals = "account_totals"
		case bvv = "bvv"
		case incomeUse = "income_use"
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(Format.v1, forKey: .format)
		try container.encode(title, forKey: .title)
		try container.encode(type, forKey: .type)
		try container.encode(period, forKey: .period)
		try container.encode(previousPeriod, forKey: .previousPeriod)
		try container.encode(taxonomy, forKey: .taxonomy)
		try container.encode(inventory, forKey: .inventory)
		try container.encode(incomeStatement, forKey: .incomeStatement)
		try container.encode(balanceSheet, forKey: .balanceSheet)
		try container.encode(notesBelowBalanceSheet, forKey: .notesBelowBalanceSheet)
		try container.encode(notes, forKey: .notes)
		try container.encode(accountTotals, forKey: .accountTotals)
		try container.encode(bvv, forKey: .bvv)
		try container.encode(incomeUse, forKey: .incomeUse)
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		let format = try container.decode(Format.self, forKey: .format)
		guard format == .v1 else { throw DecodingError.dataCorruptedError(forKey: CodingKeys.format, in: container, debugDescription: "unsupported or invalid file format") }
		self.title = try container.decode(ReportTitle.self, forKey: .title)
		self.type = try container.decode(ReportType.self, forKey: .type)
		self.period = try container.decode(Period.self, forKey: .period)
		self.previousPeriod = container.contains(.previousPeriod) ? try container.decode(Period.self, forKey: .previousPeriod) : nil
		self.taxonomy = try container.decode(DEGAAPTaxonomyVersion.self, forKey: .taxonomy)
		self.inventory = try container.decode(Inventory.self, forKey: .inventory)
		self.incomeStatement = try container.decode(IncomeStatement.self, forKey: .incomeStatement)
		self.balanceSheet = try container.decode(BalanceSheet.self, forKey: .balanceSheet)
		self.notesBelowBalanceSheet = try container.decode(NotesBelowBalanceSheet.self, forKey: .notesBelowBalanceSheet)
		self.notes = try container.decode(Notes.self, forKey: .notes)
		self.accountTotals = try container.decode(AccountTotals.self, forKey: .accountTotals)
		self.bvv = container.contains(.bvv) ? try container.decode(AccountTotals.self, forKey: .bvv) : nil
		self.incomeUse = container.contains(.incomeUse) ? try container.decode(AccountTotals.self, forKey: .incomeUse) : nil
	}
}

extension Report : Archivable
{
	public init(fromArchive archive : String, issues : inout IssueCollection) throws
	{
		self = try TOMLikeCoder.decode(Self.self, from: archive, issues: &issues)
	}

	public func archive() throws -> String
	{
		try TOMLikeCoder.encode(self)
	}
}
