//  Copyright 2020-2024 Kai Oezer

import Foundation

/// Statement of changes in fixed assets.
///
/// German: Anlagenspiegel, Anlagengitter
///
/// [§ 284 Abs. 3 HGB](https://www.gesetze-im-internet.de/hgb/__284.html)
/// Im Anhang ist die Entwicklung der **einzelnen Posten des Anlagevermögens** in einer gesonderten Aufgliederung
/// darzustellen. Dabei sind, ausgehend von den gesamten Anschaffungs- und Herstellungskosten, die **Zugänge,
/// Abgänge, Umbuchungen und Zuschreibungen** des Geschäftsjahrs **sowie die Abschreibungen** gesondert aufzuführen.
/// Zu den Abschreibungen sind gesondert folgende Angaben zu machen:
/// 1. die Abschreibungen in ihrer gesamten Höhe zu Beginn und Ende des Geschäftsjahrs,
/// 2. die im Laufe des Geschäftsjahrs vorgenommenen Abschreibungen und
/// 3. Änderungen in den Abschreibungen in ihrer gesamten Höhe im Zusammenhang mit Zu- und Abgängen sowie
/// 	Umbuchungen im Laufe des Geschäftsjahrs.
/// Sind in die Herstellungskosten Zinsen für Fremdkapital einbezogen worden, ist für jeden Posten des
/// Anlagevermögens anzugeben, welcher Betrag an Zinsen im Geschäftsjahr aktiviert worden ist.
///
/// Der Anlagenspiegel ist Teil des Anhangs eines Jahresabschlusses.
///
/// Klein- und Kleinstkapitalgesellschaften brauchen zwar handelsrechtlich (also nach HGB)
/// keinen Anlagenspiegel aufstellen, müssen es aber steuerrechtlich (nach GmbHG).
///
/// Auch geringwertige Wirtschaftsgüter (GWG) sind in den Anlagenspiegel aufzunehmen.
///
/// Generell muss der Satz and Datenfeldern für jeden Posten erstellt werden.
/// Bei Kleinstkapitalgesellschaften, die eine nach MicroBilG vereinfachte Bilanz
/// aufstellen, enthält die Bilanz nur einen Posten für das Anlagevermögen.
public struct AssetChangesStatement : Codable, Equatable, Sendable
{
	public struct AcquisitionCosts : Codable, Equatable, Sendable
	{
		/// Summe der Anschaffungs- oder Herstellungskosten am Anfang des Jahres
		public var begin : Money = .zero

		/// Zugaenge, new assets
		/// including the amount in ``interestFromBorrowedCapital``
		public var new : Money = .zero

		/// Zinsen für Fremdkapital, die als Anschaffungskosten einberechnet wurden.
		public var interestPaidForBorrowedCapital : Money = .zero

		/// Abgaenge
		public var retirements : Money = .zero

		/// Umbuchungen
		public var rebookings : Money = .zero

		/// Summe Anschaffungs- oder Herstellungskosten am Ende des Jahres
		public var end : Money = .zero

		public mutating func adding(_ other : AcquisitionCosts)
		{
			begin += other.begin
			new += other.new
			retirements += other.retirements
			rebookings += other.rebookings
			end += other.end
		}
	}

	public struct Depreciations : Codable, Equatable, Sendable
	{
		/// Summe der Gesamtabschreibungen pro Anlage am Anfang des Berichtsjahres
		public var begin : Money = .zero

		/// Summe der Abschreibungen im Laufe des Berichtsjahres
		public var new : Money = .zero

		/// Summe der Gesamtabschreibungen auf im Berichtsjahr abgegangene Anlagen
		public var retirements : Money = .zero

		/// Summe der Abschreibungen am Ende des Berichtsjahres
		public var end : Money = .zero

		mutating func adding(_ other : Depreciations)
		{
			begin += other.begin
			new += other.new
			retirements += other.retirements
			end += other.end
		}
	}

	@frozen
	public struct BookValues : Codable, Equatable
	{
		/// Summe der Buchwerte am Ende des Berichtsjahres
		public var current : Money = .zero

		/// Summe der Buchwerte am Ende des Vorjahres
		public var previous : Money = .zero

		mutating func adding(_ other : BookValues)
		{
			current += other.current
			previous += other.previous
		}
	}

	public struct AssetAccountChanges : Codable, Equatable, Sendable
	{
		public var acquisitionCosts = AcquisitionCosts()
		public var depreciations = Depreciations()
		public var bookValues = BookValues()

		mutating func adding(_ other : AssetAccountChanges)
		{
			acquisitionCosts.adding(other.acquisitionCosts)
			depreciations.adding(other.depreciations)
			bookValues.adding(other.bookValues)
		}
	}

	public var total : AssetAccountChanges
	{
		var totalChanges = AssetAccountChanges()
		for change in changes.values
		{
			totalChanges.adding(change)
		}
		return totalChanges
	}

	public var changes = [AccountID : AssetAccountChanges]()
}
