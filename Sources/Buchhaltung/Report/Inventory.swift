//  Copyright 2020-2024 Kai Oezer

import Foundation

/**
	Die Jahresschlussbilanz fängt an mit der Aufstellung eines Inventars gemäß
	[§ 240 HGB](https://www.gesetze-im-internet.de/hgb/__240.html).

	Für **Kleinstkapitalgesellschaften** sind **keine Erleichterungen** für die
	Erstellung des Inventars vorgesehen. Das detailierte Inventar muss jedoch
	nicht im Jahresabschluss beim Bundesanzeiger hinterlegt oder dem Finanzamt
	übermittelt werden.

	Im Inventar aufzuzeichnen sind
	* Grundstücke
	* Forderungen
	* Schulden
	* Bargeld
	* Sonstige Vermögensgegenstände

  Konkrete Gliederungsvorschriften für das Inventar existieren nicht.
	Die Reihenfolge der Aufstellung ist üblicherweise
	
	1. das Vermögen (nach steigender Liquidität sortiert und
		bestehend aus Anlage- und Umlaufvermögen),

	2. die Schulden (nach abnehmender Fälligkeit sortiert,
		die Langfristigen zuerst, die Kurzfristigen zuletzt.),

	3. das Reinvermögen (das in die Bilanz als Eigenkapital
		übernommen und aus Vermögen minus Schulden berechnet wird)
*/
public struct Inventory : Codable, Sendable
{
	/// German: Stichtag
	public let date : Day

	var assets : Assets?

	var debts : Debts?

	/// Anlagevermögen
	/// Auflistung in der Reihenfolge der Langfristigkeit, bzw.,
	/// nach steigender Liquidität (Wiederverkäuflichkeit, Umwandelbarkeit in Bargeld):
	/// * Grundstücke
	/// * Gebäude
	/// * Transporteinrichtungen
	/// * Betriebs- und Geschäftsausstattung
	public let fixedAssetsValuation : [Assets.FixedAssetType : Money]

	public internal(set) var fixedAssetDepreciationSinceLastPeriod : Money?

	/// Auflistung nach steigender Liquidität
	/// * Waren (Vorräte)
	/// * Forderungen aus Lieferungen und Leistungen
	/// * Kassenbestand, Bankguthaben
	public let currentAssetsValuation : [Assets.CurrentAssetType : Money]

	/// Schulden mit Laufzeit laenger als ein Jahr.
	public let longTermDebtsValuation : Money

	/// Schulden mit Laufzeit weniger als ein Jahr.
	public let shortTermDebtsValuation : Money

	internal init(period : Period, assets : Assets? = nil, debts : Debts? = nil)
	{
		var totalDepreciation = Money.zero
		var faItems = [Assets.FixedAssetType : Money]()
		var caItems = [Assets.CurrentAssetType : Money]()
		if let assets
		{
			Assets.FixedAssetType.allCases.forEach { type in
				var total = Money.zero
				assets.fixedAssets.filter{ type == $0.type }.forEach {
					let startValue = $0.bookValue(for: period.start - 1)
					let value = $0.bookValue(for:period.end)
					total += value
					totalDepreciation += startValue - value
				}
				if total > .zero
				{
					faItems[type] = total
				}
			}

			Assets.CurrentAssetType.allCases.forEach { type in
				let total = assets.currentAssets.filter{ type == $0.type }.reduce(Money.zero){ $0 + $1.value }
				if total > .zero
				{
					caItems[type] = total
				}
			}
		}

		self.date = period.end
		self.assets = assets
		self.debts = debts
		self.fixedAssetsValuation = faItems
		self.fixedAssetDepreciationSinceLastPeriod = totalDepreciation
		self.currentAssetsValuation = caItems
		self.longTermDebtsValuation = debts?.longTermDebtsTotal(for: date) ?? .zero
		self.shortTermDebtsValuation = debts?.shortTermDebtsTotal(for: date) ?? .zero
	}

	public var total : Money
	{
		assetsTotal - debtTotal
	}

	public var assetsTotal : Money
	{
		fixedAssetsTotal + currentAssetsTotal
	}

	public var fixedAssetsTotal : Money
	{
		_assetsTotal(fixedAssetsValuation)
	}

	public var currentAssetsTotal : Money
	{
		_assetsTotal(currentAssetsValuation)
	}

	public var debtTotal : Money
	{
		shortTermDebtsValuation + longTermDebtsValuation
	}

	private func _assetsTotal(_ assets : [some AssetType : Money]) -> Money
	{
		assets.reduce(Money.zero){ $0 + $1.value }
	}
}

extension Inventory : Equatable
{
	public static func == (lhs : Inventory, rhs : Inventory) -> Bool
	{
		lhs.date == rhs.date
			&& lhs.fixedAssetsValuation == rhs.fixedAssetsValuation
			&& lhs.currentAssetsValuation == rhs.currentAssetsValuation
			&& lhs.longTermDebtsValuation == rhs.longTermDebtsValuation
			&& lhs.shortTermDebtsValuation == rhs.shortTermDebtsValuation
	}
}
