//  Copyright 2020-2024 Kai Oezer

import Foundation

/**
 Notes below the balance sheet (Angaben unter der Bilanz)

 Am Anfang der Angaben unter der Bilanz werden wie beim Anhang die folgenden Angaben ausgegeben:
 * Handelsregister-Eintragung aus ([§ 264 Abs. 1a S. 1 HGB](https://www.gesetze-im-internet.de/hgb/__264.html)
 * Größenklasse ([§ 267a HGB](https://www.gesetze-im-internet.de/hgb/__267a.html)
 * Angabe falls Gesellschaft in Liquidation/Abwicklung (§ 264 Abs. 1a S. 2 HGB)

 __Kleinstkapitalgesellschaften__, die auf die Erstellung eines Anhangs verzichten, müssen stattdessen
 folgende Angaben unter der Bilanz machen [siehe (§ 264 Abs. 1 HGB)](https://www.gesetze-im-internet.de/hgb/__264.html) :

 * Die Angaben zu nicht auf der Passivseite auszuweisenden Verbindlichkeiten und Haftungsverhältnissen,
 * dabei die Haftungsverhältnisse jeweils gesondert unter Angabe der gewährten Pfandrechte und sonstigen
   Sicherheiten,
 * dabei Verpflichtungen betreffend die Altersversorgung und Verpflichtungen gegenüber verbundenen oder
 	 assoziierten Unternehmen jeweils gesondert vermerkt.

   Definition von __Haftungsverhältnis__:
 	  "Unter der Bilanz sind, sofern sie nicht auf der Passivseite auszuweisen sind, Verbindlichkeiten
 	  aus der Begebung und Übertragung von Wechseln, aus Bürgschaften, Wechsel- und Scheckbürgschaften
 	  und aus Gewährleistungsverträgen sowie Haftungsverhältnisse aus der Bestellung von Sicherheiten
 	  für fremde Verbindlichkeiten zu vermerken; sie dürfen in einem Betrag angegeben werden.
 	  Haftungsverhältnisse sind auch anzugeben, wenn ihnen gleichwertige Rückgriffsforderungen gegenüberstehen."
 	  Siehe [§ 251 HGB](https://www.gesetze-im-internet.de/hgb/__251.html)

    Bei den Haftungsverhältnissen handelt es sich um spezielle Verpflichtungen, die zum Bilanzstichtag
    zwar bekannt sind, mit deren Inanspruchnahme aber zum anderen noch nicht konkret zu rechnen ist.
    Solche Haftungsverhältnisse sind nicht anzugeben wenn in der Bilanz auf der Passivseite dafür eine
    Verbindlichkeit oder Rückstellung ausgewiesen wird.

 * Für die Mitglieder des Geschäftsführungsorgans, eines Aufsichtsrats, eines Beirats oder einer ähnlichen
   Einrichtung jeweils für jede Personengruppe, die gewährten Vorschüsse und Kredite unter Angabe der
	 Zinssätze, der wesentlichen Bedingungen und der gegebenenfalls im Geschäftsjahr zurückgezahlten oder
	 erlassenen Beträge sowie die zugunsten dieser Personen eingegangenen Haftungsverhältnisse.
	 Siehe [§ 285 Abs. 9c HGB](https://www.gesetze-im-internet.de/hgb/__285.html)
*/
public struct NotesBelowBalanceSheet : Codable, Equatable, Sendable
{
	public enum IncomeCalculationMethod : String, Codable, Equatable, Sendable
	{
		case gkv = "GKV" // Gesamtkostenverfahren
		case ukv = "UKV" // Umsatzkostenverfahren
	}

	public var companyInformation = Company()

	public var incomeCalculationMethod : IncomeCalculationMethod = .gkv
}
