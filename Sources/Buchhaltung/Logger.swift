// Copyright 2023-2025 Kai Oezer

import Foundation

#if canImport(OSLog)

import OSLog

struct Logger : Sendable
{
	private let _logger : os.Logger

	init(category : String)
	{
		_logger = os.Logger(subsystem: "belanz.Buchhaltung", category: category)
	}

	func debug(_ message : String)
	{
		_logger.debug("\(message)")
	}

	func warning(_ message : String)
	{
		_logger.warning("\(message)")
	}

	func error(_ message : String)
	{
		_logger.error("\(message)")
	}
}

#elseif canImport(Logging)

import Logging

struct Logger : Sendable
{
	private let _logger : Logging.Logger
	private let _category : String

	init(category : String)
	{
		_logger = Logging.Logger(label: "Buchhaltung")
		_category = category
	}

	func debug(_ message : String)
	{
		_logger.debug(Logging.Logger.Message(stringLiteral: message), metadata: ["category" : .string(_category)])
	}

	func warning(_ message : String)
	{
		_logger.warning(Logging.Logger.Message(stringLiteral: message), metadata: ["category" : .string(_category)])
	}

	func error(_ message : String)
	{
		_logger.error(Logging.Logger.Message(stringLiteral: message), metadata: ["category" : .string(_category)])
	}
}

#endif
