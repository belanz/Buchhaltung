// Copyright 2022-2025 Kai Oezer

import Foundation
import DEGAAP

public typealias AccountID = DEGAAPItemID

/// Collection of XBRL DE-GAAP accounts used for generating business reports.
public struct DEGAAPAccounts
{
	public static let invalid = AccountID("invalid")!

	/// fixed asset accounts used in the inventory
	static let inventoryFixedAssetAccounts : [AccountID] = [
		ass_fixed_intangible,
		ass_fixed_intangible_self,
		ass_fixed_land,
		ass_fixed_buildings,
		ass_fixed_machinery,
		ass_fixed_equipment_factory,
		ass_fixed_equipment_office,
		ass_fixed_equipment_misc,
		ass_fixed_equipment_gwg,
		ass_fixed_vehicle_passenger,
		ass_fixed_vehicle_commercial,
		ass_fixed_vehicle_other,
		ass_fixed_financial,
		ass_fixed_other
	]

	public static let supportedAssetAccounts : [AccountID] = [
		ass_fixed_intangible,
		ass_fixed_intangible_self,
		ass_fixed_land,
		ass_fixed_buildings,
		ass_fixed_machinery,
		ass_fixed_equipment_factory,
		ass_fixed_equipment_office,
		ass_fixed_equipment_misc,
		ass_fixed_equipment_gwg,
		ass_fixed_vehicle_passenger,
		ass_fixed_vehicle_commercial,
		ass_fixed_vehicle_other,
		ass_fixed_financial,
		ass_fixed_other,

		ass_current_receivables_trade,
		ass_current_receivables_vat,
		ass_current_receivables_misc,
		ass_current_securities,
		ass_current_bank,
		ass_current_cash
	]

	public static let supportedEquityLiabilityAccounts : [AccountID] = [
		eqLiab_equity_subscribed,
		eqLiab_equity_capitalReserves,
		eqLiab_equity_revenueReserves_legal,
		eqLiab_equity_revenueReserves_investment,
		eqLiab_equity_revenueReserves_statutory,
		eqLiab_equity_revenueReserves_other,

		income_deprAmort_tangible,
		income_deprAmort_gwg,
		income_deprAmort_gwg_pool,
		income_otherCost,
		income_tax,
		income_disposal_fixed_assets
	]

	// MARK: - Bilanz Aktiva -

	static let ass_fixed                            = AccountID("bs.ass.fixAss")!
	public static let ass_fixed_intangible          = AccountID("bs.ass.fixAss.intan.other")!
	public static let ass_fixed_intangible_self     = AccountID("bs.ass.fixAss.intan.selfmade")!
	public static let ass_fixed_land                = AccountID("bs.ass.fixAss.tan.landBuildings.landWithoutBuildings")!
	public static let ass_fixed_buildings           = AccountID("bs.ass.fixAss.tan.landBuildings.buildingsOnOwnLand.buildings")!
	public static let ass_fixed_machinery           = AccountID("bs.ass.fixAss.tan.machinery.machinery")!
	public static let ass_fixed_equipment_factory   = AccountID("bs.ass.fixAss.tan.otherEquipm.factory")! // Betriebsausstattung
	public static let ass_fixed_equipment_office    = AccountID("bs.ass.fixAss.tan.otherEquipm.office")! // Geschäftsausstattung
	public static let ass_fixed_equipment_gwg       = AccountID("bs.ass.fixAss.tan.otherEquipm.gwg")!
	public static let ass_fixed_equipment_misc      = AccountID("bs.ass.fixAss.tan.otherEquipm.misc")!
	public static let ass_fixed_equipment_other     = AccountID("bs.ass.fixAss.tan.otherEquipm.other")!
	public static let ass_fixed_vehicle_passenger   = AccountID("bs.ass.fixAss.tan.otherEquipm.passengerCars")!
	public static let ass_fixed_vehicle_commercial  = AccountID("bs.ass.fixAss.tan.otherEquipm.comVehicle")!
	public static let ass_fixed_vehicle_other       = AccountID("bs.ass.fixAss.tan.otherEquipm.otherTransportMeans")!
	public static let ass_fixed_financial           = AccountID("bs.ass.fixAss.fin.otherFinAss.misc")!
	public static let ass_fixed_other               = AccountID("bs.ass.fixAss.tan.other.misc")!

	static let ass_current                          = AccountID("bs.ass.currAss")!
	public static let ass_current_receivables_trade = AccountID("bs.ass.currAss.receiv.trade.misc")!
	public static let ass_current_receivables_vat   = AccountID("bs.ass.currAss.receiv.other.vat")!
	public static let ass_current_receivables_misc  = AccountID("bs.ass.currAss.receiv.other.misc")!
	public static let ass_current_securities        = AccountID("bs.ass.currAss.securities.other")!
	public static let ass_current_bank              = AccountID("bs.ass.currAss.cashEquiv.bank")!
	public static let ass_current_cash              = AccountID("bs.ass.currAss.cashEquiv.misc")!

	static let ass_prepaidExp                       = AccountID("bs.ass.prepaidExp")!
	static let ass_defTax                           = AccountID("bs.ass.defTax")!
	static let ass_SurplusFromOffsetting            = AccountID("bs.ass.SurplusFromOffsetting")!

	static let ass_deficit                          = AccountID("bs.ass.deficitNotCoveredByCapital")!

	// MARK: - Bilanz Passiva -

	static let eqLiab_equity                                   = AccountID("bs.eqLiab.equity")!
	public static let eqLiab_equity_subscribed                 = AccountID("bs.eqLiab.equity.subscribed.corp")!
	public static let eqLiab_equity_capitalReserves            = AccountID("bs.eqLiab.equity.capRes")!
	public static let eqLiab_equity_revenueReserves_legal      = AccountID("bs.eqLiab.equity.revenueRes.legal")!
	public static let eqLiab_equity_revenueReserves_investment = AccountID("bs.eqLiab.equity.revenueRes.investment")!
	public static let eqLiab_equity_revenueReserves_statutory  = AccountID("bs.eqLiab.equity.revenueRes.statutory")!
	public static let eqLiab_equity_revenueReserves_other      = AccountID("bs.eqLiab.equity.revenueRes.other")!
	static let eqLiab_equity_netIncome                         = AccountID("bs.eqLiab.equity.netIncome")!
	static let eqLiab_equity_profitLoss                        = AccountID("bs.eqLiab.equity.profitLoss")!

	static let eqLiab_accruals                                 = AccountID("bs.eqLiab.accruals")!
	static let eqLiab_liab                                     = AccountID("bs.eqLiab.liab")!
	static let eqLiab_defIncome                                = AccountID("bs.eqLiab.defIncome")!
	static let eqLiab_defTax                                   = AccountID("bs.eqLiab.defTax")!

	/// Vortrag von Gewinn-/Verlust aus GuV
	static let eqLiab_equity_retainedEarnings = AccountID("bs.eqLiab.equity.retainedEarnings.finalPrev")!
	/// Vortrag von Bilanzgewinn/-verlust.
	/// Bilanzgewinn/verlust muss nicht identisch mit realem Gewinn/Verlust sein
	/// weil eventuelle Entnahmen aus Rücklagen angerechnet werden.
	static let eqLiab_equity_profitLoss_retainedEarnings = AccountID("bs.eqLiab.equity.profitLoss.retainedEarnings")!

	// MARK: - Bilanzübertragskonten. Initialisierung durch Bilanzkontostände der Vorjahresbilanz -

	static let eqLiab_accruals_init       = AccountID("bs.eqLiab.accruals.other.other")!
	static let eqLiab_liab_init           = AccountID("bs.eqLiab.liab.other.other")!
	static let eqLiab_defIncome_init      = eqLiab_defIncome
	static let eqLiab_defTax_init         = eqLiab_defTax

	// MARK: - GuV -

	static let income_netSales                      = AccountID("ismi.netIncome.netSales")!
	static let income_otherOpRevenue                = AccountID("ismi.netIncome.otherOpRevenue")!
	static let income_materialServices              = AccountID("ismi.netIncome.materialServices")!
	static let income_staff                         = AccountID("ismi.netIncome.staff")!
	static let income_deprAmort                     = AccountID("ismi.netIncome.deprAmort")!
	public static let income_deprAmort_tangible     = AccountID("is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc")!
	public static let income_deprAmort_gwg          = AccountID("is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAs")!
	public static let income_deprAmort_gwg_pool     = AccountID("is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAsCollItem")!
	public static let income_otherCost              = AccountID("ismi.netIncome.otherCost")!
	public static let income_tax                    = AccountID("ismi.netIncome.tax")!
	public static let income_disposal_fixed_assets  = AccountID("is.netIncome.regular.operatingTC.otherCost.disposFixAss.misc")!

	// MARK: - Betriebsvermögensvergleich (BVV) -

	static let bvv_profit              = AccountID("BVV.profitLoss")! // Jahresüberschuss/-fehlbetrag (BVV)
	static let bvv_assets_current      = AccountID("BVV.profitLoss.assetsCurrentYear")! // Betriebsvermögen zum Ende des Wirtschaftsjahres
	static let bvv_assets_previous     = AccountID("BVV.profitLoss.assetsPreviousYear.assets")! // Betriebsvermögen zum Ende des vorangegangenen Wirtschaftsjahres
	static let bvv_assets_previous_adj = AccountID("BVV.profitLoss.assetsPreviousYear.capAdjustments")! // Kapitalanpassung
	static let bvv_withdrawals         = AccountID("BVV.profitLoss.withdrawalDistrib")! // Entnahmen / Ausschüttungen im laufenden Wirtschaftsjahr
	static let bvv_contributions       = AccountID("BVV.profitLoss.contribution")! // Einlagen / Kapitalzuführungen im laufenden Wirtschaftsjahr
	static let bvv_estg6b              = AccountID("BVV.profitLoss.EStG6b")! // Kapitaländerung durch Übertragung einer § 6b EStG Rücklage
}
