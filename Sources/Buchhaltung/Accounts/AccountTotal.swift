//  Copyright 2020-2024 Kai Oezer

import Foundation

/// When mapping to/from ``BalanceSheet`` or ``Record.Entry`` fields,
/// money values on the credit side need to be multiplied with -1.
public struct AccountTotals : Codable, Equatable, Sendable
{
	var totals = [AccountID : Money]()
}

extension AccountTotals
{
	static func + (lhs : AccountTotals, rhs : AccountTotals) -> AccountTotals
	{
		AccountTotals(totals: lhs.totals.merging(rhs.totals, uniquingKeysWith: { $0 + $1 }))
	}
}

extension AccountTotals : CustomDebugStringConvertible
{
	public var debugDescription : String
	{
		totals.sorted{ $0.key < $1.key }.map{ "\($0.key.description) \($0.value)" }.joined(separator: "\n")
	}
}
