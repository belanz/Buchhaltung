//  Copyright 2020-2025 Kai Oezer

import Foundation

public enum LaTeXEncodingError : Error, CustomStringConvertible, Equatable
{
	case generalError(String)
	case typeNotEncodable(String)
	case missingTemplate(String)
	case templateContentsInvalid(String)
	case placeholderNotFound

	public var description: String
	{
		switch self
		{
			case .generalError(let message): return message
			case .typeNotEncodable(let type): return "The encountered type \(type) has no LaTeX representation."
			case .missingTemplate(let templateName): return "Failed to load the LaTeX template named \"\(templateName)\"."
			case .templateContentsInvalid(let templateName): return "Invalid contents in the LaTeX template named \"\(templateName)\"."
			case .placeholderNotFound: return "Failed to find the placeholder."
		}
	}
}

public struct LaTeXEncoder
{
	public static func encode(_ value : some Encodable) throws -> String
	{
		guard let latexEncodableValue = value as? LaTeXEncodable else { throw LaTeXEncodingError.typeNotEncodable("\(type(of:value))") }
		return try latexEncodableValue.latexRepresentation()
	}

	static func contents(ofTemplate templateName : String) throws -> String
	{
		guard
			let templateLocation = Bundle.module.url(forResource: templateName, withExtension: "tex", subdirectory: "LaTeX"),
			templateLocation.isFileURL
			else { throw LaTeXEncodingError.missingTemplate(templateName) }
		guard
			let fileData = FileManager.default.contents(atPath: templateLocation.path),
			let fileContents = String(bytes: fileData, encoding: .utf8)
			else { throw LaTeXEncodingError.templateContentsInvalid(templateName) }
		return fileContents
	}

	/// Utility functions for LaTeXEncodable instances, which searches for and replaces occurrences of
	/// the given placeholder within the given template content.
	/// - parameters:
	///   - template: the text in which to search for occurrences
	///   - placeholder: the string whose occurrences shall be replaced
	///   - value: the replacement value for the occurrences
	/// - throws: `LaTeXEncodingError.placeholderNotFound` if the given placeholder does not occurr in the given template.
	static func replace(placeholder : String, in template : inout String, with value : String) throws
	{
		let searchString = "\\BHPlaceholder{\(placeholder)}"
		guard let _ = template.range(of: searchString) else {
			throw LaTeXEncodingError.placeholderNotFound
		}
		template.replace(searchString) { _ in
			value
		}
	}
}

extension Money
{
	public var latex : String
	{
		"\(self.formatted(LaTeXFormatStyle())) \(Currency.euro.latex)"
	}

	struct LaTeXFormatStyle : FormatStyle
	{
		func format(_ money : Money) -> String
		{
			money.bh
		}
	}
}

extension Currency
{
	public var latex : String
	{
		self.formatted(LaTeXFormatStyle())
	}

	struct LaTeXFormatStyle : FormatStyle
	{
		func format(_ value: Currency) -> String
		{
			switch value
			{
				case .euro : return "\\euro"
				case .dollar : return "\\$"
				case .pound: return "\\textsterling"
				case .yen: return "\\textyen"
				default: return value.code
			}
		}
	}
}

extension Day
{
	public var latex : String
	{
		self.formatted(LaTeXFormatStyle())
	}

	struct LaTeXFormatStyle : FormatStyle
	{
		func format(_ day : Day) -> String
		{
			day.midday.formatted(.dateTime.year().month(.twoDigits).day(.twoDigits).locale(.init(identifier: "de_DE")))
		}
	}
}
