//  Copyright 2021 Kai Oezer

import Foundation

extension IncomeStatement : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		var template = try LaTeXEncoder.contents(ofTemplate: "guv-micro")

		let resultValues : (IncomeStatement.Results) -> [Money] = {[
			$0.earnings,
			$0.otherEarnings,
			$0.materialCosts,
			$0.salaries,
			$0.depreciations,
			$0.otherExpenses,
			$0.tax,
			$0.profitLoss
		]}
		let values = resultValues(self.results)
		let prevYearValues = resultValues(self.previousYearResults)
		let valuePlaceholders = [
			"NetSales",
			"OtherRevenue",
			"MaterialCost",
			"Salaries",
			"Depreciation",
			"OtherExpenses",
			"Tax",
			"Total"
		]
		let prevYearValuePlaceholders = valuePlaceholders.map{ "\($0).Prev" }

		do
		{
			for (placeholder, value) in zip(valuePlaceholders + prevYearValuePlaceholders, values + prevYearValues)
			{
				try LaTeXEncoder.replace(placeholder: placeholder, in: &template, with: value.latex)
			}
		}
		catch
		{
			throw LaTeXEncodingError.generalError("Error while generating the LaTeX output of the income statement: \(error)")
		}

		return template
	}
}
