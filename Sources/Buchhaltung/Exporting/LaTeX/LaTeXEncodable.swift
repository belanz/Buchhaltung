//  Copyright 2020 Kai Oezer

import Foundation

public protocol LaTeXEncodable
{
	func latexRepresentation() throws -> String
}
