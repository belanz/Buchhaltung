// Copyright 2020-2022 Kai Oezer

import Foundation

extension AssetChangesStatement : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		var template = try LaTeXEncoder.contents(ofTemplate: "anlagenspiegel")
		let totalChanges = self.total
		try LaTeXEncoder.replace(placeholder: "AcqBeginn", in: &template, with: totalChanges.acquisitionCosts.begin.latex)
		try LaTeXEncoder.replace(placeholder: "AcqZugang", in: &template, with: totalChanges.acquisitionCosts.new.latex)
		try LaTeXEncoder.replace(placeholder: "AcqAbgang", in: &template, with: totalChanges.acquisitionCosts.retirements.latex)
		try LaTeXEncoder.replace(placeholder: "AcqUmbuch", in: &template, with: totalChanges.acquisitionCosts.rebookings.latex)
		try LaTeXEncoder.replace(placeholder: "AcqEnde", in: &template, with: totalChanges.acquisitionCosts.end.latex)
		try LaTeXEncoder.replace(placeholder: "DepBeginn", in: &template, with: totalChanges.depreciations.begin.latex)
		try LaTeXEncoder.replace(placeholder: "DepZugang", in: &template, with: totalChanges.depreciations.new.latex)
		try LaTeXEncoder.replace(placeholder: "DepAbgang", in: &template, with: totalChanges.depreciations.retirements.latex)
		try LaTeXEncoder.replace(placeholder: "DepEnde", in: &template, with: totalChanges.depreciations.end.latex)
		try LaTeXEncoder.replace(placeholder: "Buchwert", in: &template, with: totalChanges.bookValues.current.latex)
		try LaTeXEncoder.replace(placeholder: "BuchwertVj", in: &template, with: totalChanges.bookValues.previous.latex)
		return template
	}
}
