// Copyright 2021 Kai Oezer

import Foundation

extension NotesBelowBalanceSheet : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		var template = try LaTeXEncoder.contents(ofTemplate: "bilanzanhang")

		let fill : (String, String) -> Void = { key, value in
			try? LaTeXEncoder.replace(placeholder: key, in: &template, with: value)
		}
		fill("CompanyName", companyInformation.name.joined(separator: " "))
		fill("CompanySeatSingleLine", companyInformation.seat.latexSingleLine)
		fill("TradeCourt", companyInformation.tradeRegistration.tradeCourt)
		fill("TradeRegisterID", companyInformation.tradeRegistration.tradeRegistrationID)
		fill("SubscribedCapital", companyInformation.subscribedCapital.latex)

		fill("CEO", companyInformation.chiefExecutives.map{"\\textbf{Geschäftsführer/in} & \($0.latexSingleLine) \\\\"}.joined(separator: "\n") )

		fill("NumberOfEmployees", "\(companyInformation.numberOfEmployees)")
		fill("IsInLiquidation", companyInformation.inLiquidation ? "Ja" : "Nein")

		let sumLiab : (Company.ContingentLiability.LiabilityType) -> (Money) = { type in
			companyInformation.contingentLiabilities.filter{$0.type == type}.map{$0.amount}.reduce(.zero){$0 + $1}
		}
		let listLiab : (Company.ContingentLiability.LiabilityType) -> (String) = { type in
			companyInformation.contingentLiabilities.filter{$0.type == type}.map{"\($0.amount.latex) \($0.note ?? "") \\\\"}.joined(separator: "\n")
		}
		let contLiabValue : (Company.ContingentLiability.LiabilityType) -> (String) = { type in
			sumLiab(type) == .zero ? "keine" : listLiab(type)
		}
		fill("ContingenciesGeneral", contLiabValue(.other))
		fill("ContingenciesPensions", contLiabValue(.pension))
		fill("ContingenciesAssociatedCompanies", contLiabValue(.associatedCompanies))

		let executiveLoanDescription : (Company.ExecutiveLoan) -> (String) = {
			"an \($0.name): Vorschuss \($0.advancePayments?.latex ?? "keine"), Kredite \($0.creditsGranted?.latex ?? "keine"), Haftungen \($0.liabilities?.latex ?? "keine")"
		}
		fill("LoansToExecutives", companyInformation.executiveLoans.isEmpty
				 ? "keine"
				 : companyInformation.executiveLoans.map{executiveLoanDescription($0)}.joined(separator: "\\\\")
		)

		return template
	}
}
