// Copyright 2022 Kai Oezer

import Foundation

extension Company.Seat
{
	var latexMultiLine : String
	{
		let streetWithNumber = street == nil ? nil : [street, houseNo].compactMap{$0}.joined(separator: " ")
		let zipAndCity = [zipCode, cityOrRegion].compactMap{$0}.joined(separator: " ")
		return [streetWithNumber, zipAndCity].compactMap{$0}.joined(separator: "\\\\")
	}

	var latexSingleLine : String
	{
		let streetWithNumber = street == nil ? nil : [street, houseNo].compactMap{$0}.joined(separator: " ")
		let zipAndCity = [zipCode, cityOrRegion].compactMap{$0}.joined(separator: " ")
		return [streetWithNumber, zipAndCity].compactMap{$0}.joined(separator: ", ")
	}
}

extension  Company.Executive
{
	var latexSingleLine : String
	{
		"\(name), \(address)"
	}
}
