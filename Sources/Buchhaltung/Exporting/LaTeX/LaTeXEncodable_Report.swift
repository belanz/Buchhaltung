//  Copyright 2021 Kai Oezer

import Foundation

extension Report : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		[
			try _intro(),
			try _billigung(),
			[
				try? balanceSheet.latexRepresentation(),
				try? notesBelowBalanceSheet.latexRepresentation(),
				try? incomeStatement.latexRepresentation(),
				try? notes.latexRepresentation(),
				exportsInventory ? (try? inventory.latexRepresentation()) : nil,
				try? _closing()
			]
			.compactMap{$0}.filter{!$0.isEmpty}.joined(separator:"\n\\newpage\n")
		]
		.compactMap{$0}.filter{!$0.isEmpty}.joined(separator:"\n")
	}

	private func _intro() throws -> String
	{
		let preamble = try LaTeXEncoder.contents(ofTemplate: "preamble")
		var style = try LaTeXEncoder.contents(ofTemplate: "style")
		var opening = try LaTeXEncoder.contents(ofTemplate: "opening")

		let reportTitle = self.title.string.isEmpty ? generateTitle() : self.title.string

		try LaTeXEncoder.replace(placeholder: "CompanyName", in: &style, with: notesBelowBalanceSheet.companyInformation.name.joined(separator:"\\\\\n"))
		try LaTeXEncoder.replace(placeholder: "CompanySeatMultiLine", in: &style, with: notesBelowBalanceSheet.companyInformation.seat.latexMultiLine)
		try LaTeXEncoder.replace(placeholder: "FooterReportTitleAndDate", in: &style, with:"\(reportTitle) / \(Day().latex)")
		try LaTeXEncoder.replace(placeholder: "ReportTitle", in: &opening, with: reportTitle)
		try LaTeXEncoder.replace(placeholder: "PeriodBegin", in: &opening, with: period.start.latex)
		try LaTeXEncoder.replace(placeholder: "PeriodEnd", in: &opening, with: period.end.latex)
		try LaTeXEncoder.replace(placeholder: "ClosingDate", in: &opening, with: period.end.latex)
		try LaTeXEncoder.replace(placeholder: "PeriodBeginPrev", in: &opening, with: previousPeriod?.start.latex ?? "-")
		try LaTeXEncoder.replace(placeholder: "PeriodEndPrev", in: &opening, with: previousPeriod?.end.latex ?? "-")
		try LaTeXEncoder.replace(placeholder: "ClosingDatePrev", in: &opening, with: previousPeriod?.end.latex ?? "-")
		return preamble + style + opening
	}

	private func _billigung() throws -> String
	{
		var billigung = try LaTeXEncoder.contents(ofTemplate: "billigung")
		try LaTeXEncoder.replace(placeholder: "ReportPreparedOn", in: &billigung, with: Day().latex)
		return billigung
	}

	private func _closing() throws -> String
	{
		try LaTeXEncoder.contents(ofTemplate: "closing")
	}

	private func generateTitle() -> String
	{
		let periodIsWholeYear = (period == Period.year(period.year))
		return "\(type.rawValue) \(periodIsWholeYear ? String(describing:period.year) : period.description)"
	}
}
