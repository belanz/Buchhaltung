//  Copyright 2020-2024 Kai Oezer

import Foundation

extension Inventory : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		var inventory = try LaTeXEncoder.contents(ofTemplate: "inventar")

		try LaTeXEncoder.replace(placeholder: "InventoryDate", in: &inventory, with: date.latex)
		try LaTeXEncoder.replace(placeholder: "Inventory", in: &inventory, with: overview)
		if let fixedAssetsListing_ = fixedAssetsListing
		{
			var appendix = try LaTeXEncoder.contents(ofTemplate: "inventaranhang")
			try LaTeXEncoder.replace(placeholder: "FixedAssetList", in: &appendix, with: fixedAssetsListing_)
			inventory += "\n\\newpage\n" + appendix
		}
		return inventory
	}

	var fixedAssetsListing : String?
	{
		assets?.fixedAssets.filter{ ($0.acquisitionDate <= date) && ($0.retirementDate > date) }.sorted().map { asset in
			"""
			\\hline
			\\multicolumn{1}{|r|}{\(asset.registrationID)} & \\multicolumn{2}{l|}{\(asset.name)} \\\\
			\\hline
				& \(asset.acquisitionDate.latex) & \(asset.acquisitionCost.latex) \\\\
				& \(asset.depreciationSchedule.progression == .none ? "" : asset.depreciationSchedule.latexRepresentation) & \(asset.bookValue(for: date).latex) \\\\
			"""
		}.joined(separator: "\n")
	}

	var overview : String
	{
		"""
		\(_assetsOverview)
		\\multicolumn{4}{l|}{} & \\\\
		\(_debtOverview)
		\\multicolumn{4}{l|}{} & \\\\
		\\multicolumn{4}{l|}{\\textbf{C. Errechnen des Reinvermögens}} & \\\\
		& \\multicolumn{3}{l|}{$+$ Summe des Vermögens} & \(assetsTotal.latex) \\\\
		& \\multicolumn{3}{l|}{$-$ Summe der Schulden} & \(debtTotal.latex) \\\\
		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Reinvermögen (Eigenkapital)}} & \\multicolumn{1}{r|}{\(total.latex)} \\\\
		\\hline
		"""
	}

	private var _assetsOverview : String
	{
		"""
		\\multicolumn{4}{l|}{\\textbf{A. Vermögen}} & \\\\
		& \\multicolumn{3}{l|}{\\textbf{I. Anlagevermögen}} & \(fixedAssetsTotal.latex) \\\\
		\(_assetsSummary(for: fixedAssetsValuation))
		& \\multicolumn{3}{l|}{\\textbf{II. Umlaufvermögen}} & \(currentAssetsTotal.latex) \\\\
		\(_assetsSummary(for: currentAssetsValuation))
		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Summe Vermögen}} & \\multicolumn{1}{r|}{\(assetsTotal.latex)} \\\\
		\\hline
		"""
	}

	private func _assetsSummary<T>(for assets : [T : Money] ) -> String where T : (Comparable & LaTeXEncodable)
	{
		assets.sorted{ $0.key < $1.key }.compactMap {
			let totalValueOfAssetsOfCurrentType = $0.value
			guard totalValueOfAssetsOfCurrentType > .zero else { return nil }
			return try? "&  & \($0.key.latexRepresentation()) & \(totalValueOfAssetsOfCurrentType.latex) & \\\\"
		}.joined(separator: "\n")
	}

	private var _debtOverview : String
	{
		"""
		\\multicolumn{4}{l|}{\\textbf{B. Schulden}} & \\\\
		& \\multicolumn{3}{l|}{\\textbf{I. Langfristige Schulden}} & \(longTermDebtsValuation.latex) \\\\
		& \\multicolumn{3}{l|}{\\textbf{II. Kurzfristige Schulden}} & \(shortTermDebtsValuation.latex) \\\\
		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Summe Schulden}} & \\multicolumn{1}{r|}{\(debtTotal.latex)} \\\\
		\\hline
		"""
	}

}

extension Assets.DepreciationSchedule.Progression
{
	fileprivate var latexRepresentation : String
	{
		switch self
		{
			case .none : return ""
			case .usage : return "leistungsabhängig"
			case .linear: return "linear"
			case .degressive: return "degressiv"
			case .gwg: return "GWG"
			case .pooled: return "GWG Sammelposten"
		}
	}
}

extension Assets.DepreciationSchedule
{
	fileprivate var latexRepresentation : String
	{
		if progression == .linear
		{
			return "\(progression.latexRepresentation) auf \(depreciationYears) \(depreciationYears == 1 ? "Jahr" : "Jahre")"
		}
		return progression.latexRepresentation
	}
}

extension Assets.FixedAssetType : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		switch self
		{
			case .intangible: return "Immaterielle Vermögensgegenstände"
			case .intangibleSelfMade: return "Selbst geschaffene immaterielle Vermögensgegenstände"
			case .land: return "Grundstücke"
			case .building: return "Bauten"
			case .machinery: return "Technische Anlagen und Maschinen"
			case .factoryEquipment: return "Betriebsausstattung"
			case .officeEquipment: return "Geschäftsausstattung"
			case .equipment: return "Sonstige Betriebs- und Geschäftsausstattung"
			case .vehicle, .passengerVehicle, .commercialVehicle: return "Fuhrpark"
			case .financial: return "Finanzanlagen"
			case .other: return "Sonstige Anlagen"
		}
	}
}

extension Assets.CurrentAssetType : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		switch self
		{
			case .material: return "Roh-, Hilfs- und Betriebsstoffe"
			case .inProgress: return "Unfertige Erzeugnisse"
			case .finishedGoods: return "Fertige Erzeugnisse, Warenbestand"
			case .receivable: return "Forderungen aus Lieferungen und Leistungen"
			case .security: return "Wertpapiere"
			case .cashEquivalent: return "Kassenbestand, Bankguthaben"
			case .other: return "Sonstige"
		}
	}
}
