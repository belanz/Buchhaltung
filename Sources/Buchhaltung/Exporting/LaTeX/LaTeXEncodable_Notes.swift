// Copyright 2021 Kai Oezer

import Foundation

extension Notes : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		guard let statement = assetChangesStatement else { return "" }
		var template = try LaTeXEncoder.contents(ofTemplate: "anhang")
		try LaTeXEncoder.replace(placeholder: "Anlagenspiegel", in: &template, with: statement.latexRepresentation())
		return template
	}
}
