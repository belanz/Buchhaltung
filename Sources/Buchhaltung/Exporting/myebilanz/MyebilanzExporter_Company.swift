// Copyright 2022 Kai Oezer

import Foundation

extension Company
{
	var myebilanz : String
	{
		"""
		name=\(name.joined(separator: " "))
		legalStatus=\(legalForm.rawValue)
		\(seat.myebilanz)
		\(fiscalRegistration.myebilanz)
		incomeClassification=trade
		business=\(business)
		size=\(size.rawValue)
		"""
	}
}

extension Company.Seat
{
	var myebilanz : String
	{
		"""
		street=\(street ?? "")
		houseNo=\(houseNo ?? "")
		zipCode=\(zipCode ?? "")
		city=\(cityOrRegion ?? "")
		country=\(country ?? "Deutschland")
		"""
	}
}

extension Company.FiscalRegistration
{
	var myebilanz : String
	{
		let lines : [String?] = [
			!st13.isEmpty ? "ST13=\(st13)" : nil,
			!bf4.isEmpty ? "BF4=\(bf4)" : nil
		]
		return lines.compactMap{$0}.joined(separator: "\n")
	}
}
