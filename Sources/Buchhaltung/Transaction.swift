// Copyright 2023-2024 Kai Oezer

public struct Transaction : Codable, Comparable, Sendable
{
	public var date : Day
	public var value : Money

	public init(date : Day = .distantPast, value : Money = .zero)
	{
		self.date = date
		self.value = value
	}

	public static func < (lhs: Transaction, rhs: Transaction) -> Bool
	{
		lhs.date < rhs.date
	}
}
