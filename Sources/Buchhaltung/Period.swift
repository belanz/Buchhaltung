//  Copyright 2020-2024 Kai Oezer

import Foundation

/**
 German: Buchungsperiode, Geschäftsjahr

 The maximum duration of a business reporting period is 12 months
 ([§ 240 Abs. 2 HGB](https://www.gesetze-im-internet.de/hgb/__240.html)).
*/
public struct Period : Codable, Equatable, Sendable
{
	public enum PeriodIssue
	{
		case endPrecedesStart
		case periodExceedsOneYear
	}

	public var start : Day
	public var end : Day

  public init(start : Day, end : Day)
  {
		self.start = start
		self.end = end
	}

	public var interval : DateInterval
	{
		DateInterval(start: start.start, end: end.end)
	}

	public var year : Int
	{
		end.year
	}

	public static var currentYear : Period?
	{
		self.year(Day(date: Date()).year)
	}

	public static func year(_ year : Int) -> Period?
	{
		Period(start: Day(year, 1, 1)!, end: Day(year, 12, 31)!)
	}

	public func contains(_ day : Day) -> Bool
	{
		self.interval.contains(day.midday)
	}

	public func intersects(_ period : Period) -> Bool
	{
		!(start > period.end || end < period.start)
	}

	/// - returns: The corresponding issue identifier if there is a validation issue, otherwise nil.
	public var validationIssue : PeriodIssue?
	{
		guard end > start else { return .endPrecedesStart }
		let dateComponents = start.dateComponents(to: end)
		guard let year = dateComponents.year,
			let month = dateComponents.month,
			(year == 0) && (month <= 12)
			else { return .periodExceedsOneYear }
		return nil
	}

	public var dateRange : ClosedRange<Date>
	{
		start.start...end.end
	}

	public static var maxDateRange : ClosedRange<Date>
	{
		Day.distantPast.start...Day.distantFuture.end
	}
}

extension Period : CustomStringConvertible
{
	public var description : String
	{
		"\(start) - \(end)"
	}
}
