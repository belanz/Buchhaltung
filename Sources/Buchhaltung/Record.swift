// Copyright 2020-2025 Kai Oezer

import Foundation
import OrderedCollections
import DEGAAP

/// user-facing Record type
public typealias JournalRecord = Record<DEGAAPSourceChartItemID>

/// internal Record type for account processing according to DE-GAAP rules.
typealias DEGAAPRecord = Record<DEGAAPItemID>

let RecordGroupTitleDefaultSuffix = ", …"

public typealias RecordEntryAccountType = Comparable & CustomStringConvertible & Sendable

/// Records the transfer of value from one (logically indivisible) set of accounts to another.
///
/// German: Geschäftsfall, Buchungssatz
public struct Record<T> : Identifiable, Sendable where T : RecordEntryAccountType
{
	public private(set) var id = UUID()
	var instantiationTime : ContinuousClock.Instant

	public var date : Day
	public var title : String

	public var subRecords : [Record<T>]?
	{
		didSet { _updateValue() }
	}

	/// Soll
	public var debit : OrderedSet<Entry>
	{
		didSet { _updateValue() }
	}

	/// Haben
	public var credit : OrderedSet<Entry>
	{
		didSet { _updateValue() }
	}

	public var note : String

	public struct Link : Identifiable, Equatable, Sendable
	{
		/// This string is to be interpreted as a `URL` to a file or network resource.
		/// Choosing the type `String` instead of `URL` allows for storing malformed URLs
		/// that the user can fix later.
		public var url : String
		public let id = UUID()
		public init(url : String = "") { self.url = url }
		public static func == (lhs : Link, rhs : Link) -> Bool { lhs.url == rhs.url }
	}

	public var links : [Link]

	public typealias Tag = String
	public var tags : Set<Tag>

	public private(set) var value : Money = .zero

	public init (
		date : Day = Day(),
		title : String = "",
		note : String = "",
		tags : Set<Tag> = [],
		links : [Link] = [],
		debit : OrderedSet<Entry> = [],
		credit : OrderedSet<Entry> = [],
		subRecords : [Record<T>]? = nil
	) {
		self.instantiationTime = .now
		self.date = date
		self.title = title
		self.tags = tags
		self.debit = debit
		self.credit = credit
		self.note = note
		self.links = links
		self.subRecords = subRecords
		_updateValue()
	}

	public init(date : Day, title : String, @RecordBuilder content : () -> [Record<T>])
	{
		self.init(date: date, title: title, subRecords: content())
	}

	public var isBalanced : Bool
	{
		if let subRecords {
			return subRecords.reduce(true, { $0 && $1.isBalanced })
		}
		guard !debit.isEmpty && !credit.isEmpty else { return false }
		let debitTotal = debit.reduce(Money.zero, {$0 + ($1.account.description.isEmpty ? 0 : $1.value)})
		guard debitTotal != .zero else { return false }
		let creditTotal = credit.reduce(Money.zero, {$0 + ($1.account.description.isEmpty ? 0 : $1.value)})
		guard creditTotal != .zero else { return false }
		return debitTotal == creditTotal
	}

	/// - Returns: An equivalent record with a new ID and new timestamp.
	public var duplicated : Record<T>
	{
		_duplicateRecursively(self)
	}

	/// Creates a copy and, if there is a match with one of the given IDs, a duplicate
	/// of this record or its sub-records.
	/// - Returns: A tuple containing the instance itself, its optional duplicate,
	/// and a boolean indicating whether a descendent Record was ``duplicated``.
	///
	/// The first item in the returned tuple is the instance itself, where one of
	/// its sub-records may have been duplicated. The second item in the tuple is
	/// an optional duplicate of the instance itself, depending on whether there is
	/// a match with if one of the given IDs do not match
	/// this record or any of its sub-records.
	///
	/// Sub-records with a matching ID are duplicated within their parent record.
	/// The new sub-record structure is then included in the returned self.
	///
	/// Duplication can occur in only one of the hierarchy levels: Once a sub-record
	/// gets duplicated, none of the ancestors records will be duplicated.
	public func copy(duplicatingRecordsMatching targetRecordIDs : Set<Record<T>.ID>) -> (Record<T>, Record<T>?, Bool)
	{
		guard !targetRecordIDs.isEmpty else { return (self, nil, false) }
		var didDuplicateSomeSubRecords = false
		var newSubRecords = [Record<T>]()
		for subRecord in self.subRecords ?? [] {
			let (copiedSubRecord, duplicatedSubRecord, hasDuplicatedDescendant) = subRecord.copy(duplicatingRecordsMatching: targetRecordIDs)
			newSubRecords.append(copiedSubRecord)
			assert((duplicatedSubRecord == nil) || !hasDuplicatedDescendant)
			if let duplicatedSubRecord, !hasDuplicatedDescendant {
				newSubRecords.append(duplicatedSubRecord)
				didDuplicateSomeSubRecords = true
			}
			didDuplicateSomeSubRecords = hasDuplicatedDescendant || didDuplicateSomeSubRecords
		}

		var copy = self
		var dupe : Record<T>? = nil
		if didDuplicateSomeSubRecords {
			copy.subRecords = newSubRecords.isEmpty ? nil : newSubRecords
		} else {
			if targetRecordIDs.contains(self.id) {
				dupe = self.duplicated
			}
		}
		return (copy, dupe, didDuplicateSomeSubRecords)
	}

	/// Recursively searches sub-records for a Record with the given ID.
	/// - Returns: The path to the found Record or `nil` if the search was not successful
	///
	/// The returned path includes all Record instances from the start to the target, in that order.
	public func pathToRecord(withID recordID : Record<T>.ID) -> [Record<T>]?
	{
		if recordID == self.id {
			return [self]
		}
		if let subRecords {
			for record in subRecords {
				if let matchingRecordPath = record.pathToRecord(withID: recordID) {
					return [self] + matchingRecordPath
				}
			}
		}
		return nil
	}

	/// - Returns: The record with the given ID or `nil` if this record and none of its descendent records match
	public func record(withID recordID : Record<T>.ID) -> Record<T>?
	{
		if self.id == recordID {
			return self
		}
		if let subRecords {
			for subRecord in subRecords {
				if let foundRecord = subRecord.record(withID: recordID) {
					return foundRecord
				}
			}
		}
		return nil
	}

	/// Updates the properties of the target record with the given ID.
	/// - Returns: Whether the record with the given ID was found and updated.
	///
	/// Does not modify the sub-records of the target record.
	@discardableResult
	public mutating func updateRecord(withID recordID : Record<T>.ID, value : Record<T>) -> Bool
	{
		if self.id == recordID {
			let subRecords = self.subRecords
			self = value
			self.subRecords = subRecords
			return true
		}
		if let subRecords {
			var didUpdateSomeSubRecord = false
			var newSubRecords = [Record<T>]()
			for var subRecord in subRecords {
				if subRecord.updateRecord(withID: recordID, value: value) {
					didUpdateSomeSubRecord = true
				}
				newSubRecords.append(subRecord)
			}
			if didUpdateSomeSubRecord {
				self.subRecords = newSubRecords
				return true
			}
		}
		return false
	}

	/// - Returns: Whether a group was formed among the descendent records.
	///
	/// The function returns `false` if
	/// - no sub-records exist,
	/// - no group could be formed with the sub-records or their descendent records,
	/// - fewer than two records IDs are given.
	public mutating func group(records targetRecordIDs : Set<Record<T>.ID>) -> Bool
	{
		guard targetRecordIDs.count > 1,
			let subRecords,
			let groupedSubRecords = groupRecords(withIDs: targetRecordIDs, in: subRecords)
			else { return false }
		self.subRecords = groupedSubRecords
		return true
	}

	/// Ungroups the sub-records with the given record IDs.
	/// - Returns: whether any descendent Record instance was ungrouped
	///
	/// Descendent Record instances are ungrouped first.
	/// If a descendent Record was ungrouped, this Record will not be ungrouped.
	public mutating func ungroup(records recordIDs : Set<Record<T>.ID>) -> Bool
	{
		guard let subRecords,
			let ungroupedSubRecords = ungroupRecords(withIDs: recordIDs, in: subRecords)
			else { return false }
		self.subRecords = ungroupedSubRecords
		return true
	}

	/// Removes sub-records matching the given record IDs.
	/// - Returns: Whether a matching sub-record was found and removed.
	@discardableResult
	public mutating func remove(records recordIDs : Set<Record<T>.ID>) -> Bool
	{
		guard let subRecords else { return false }
		var didRemoveSubRecord = false
		var newSubRecords = [Record<T>]()
		for var subRecord in subRecords {
			if recordIDs.contains(subRecord.id) {
				didRemoveSubRecord = true
			} else {
				if subRecord.remove(records: recordIDs) {
					didRemoveSubRecord = true
				}
				newSubRecords.append(subRecord)
			}
		}
		if didRemoveSubRecord {
			self.subRecords = newSubRecords
		}
		return didRemoveSubRecord
	}

	private func _duplicateRecursively(_ record : Record<T>) -> Record<T>
	{
		var dupe = record
		dupe.id = UUID()
		dupe.instantiationTime = .now
		dupe.subRecords = record.subRecords?.map{ _duplicateRecursively($0) }
		return dupe
	}

	private mutating func _updateValue()
	{
		if let subRecords {
			var totalValue = Money.zero
			for var subRecord in subRecords {
				subRecord._updateValue()
				totalValue = totalValue + subRecord.value
			}
			self.value = totalValue
			return
		}
		let creditTotal = credit.reduce(Money.zero, { $0 + $1.value })
		let debitTotal = debit.reduce(Money.zero, { $0 + $1.value })
		value = max(creditTotal, debitTotal)
	}
}

extension Record : Comparable
{
	public static func < (lhs: Record, rhs: Record) -> Bool
	{
		if lhs.date != rhs.date {
			return lhs.date < rhs.date
		}
		let lhsTrimmedTitle = lhs.title.trimmingCharacters(in: .whitespacesAndNewlines)
		let rhsTrimmedTitle = rhs.title.trimmingCharacters(in: .whitespacesAndNewlines)
		if lhsTrimmedTitle != rhsTrimmedTitle {
			return lhsTrimmedTitle < rhsTrimmedTitle
		}
		if lhs.instantiationTime != rhs.instantiationTime {
			return lhs.instantiationTime < rhs.instantiationTime
		}
		return lhs.id.uuidString < rhs.id.uuidString
	}

	public static func <= (lhs: Record, rhs: Record) -> Bool
	{
		(lhs == rhs) ? true : (lhs < rhs)
	}

	public static func > (lhs: Record, rhs: Record) -> Bool
	{
		!(lhs == rhs) && !(lhs < rhs)
	}
}

extension Record : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(date)
		hasher.combine(title)
	}
}

extension Record : Equatable
{
	public static func == (lhs : Record, rhs : Record) -> Bool
	{
		(lhs.date == rhs.date)
			&& (lhs.title == rhs.title)
			&& (lhs.subRecords == rhs.subRecords)
			&& (lhs.debit == rhs.debit)
			&& (lhs.credit == rhs.credit)
			&& (lhs.tags == rhs.tags)
			&& (lhs.note == rhs.note)
			&& (lhs.links == rhs.links)
	}
}

extension Record
{
	public struct Entry : Hashable, Comparable, Identifiable, Sendable
	{
		public var account : T
		public var value : Money

		public init(account : T, value : Money = .zero)
		{
			self.account = account
			self.value = value
		}

		public func hash(into hasher: inout Hasher)
		{
			hasher.combine(id)
		}

		public static func == (lhs: Entry, rhs: Entry) -> Bool
		{
			lhs.account == rhs.account && lhs.value == rhs.value
		}

		public static func < (lhs: Entry, rhs: Entry) -> Bool
		{
			lhs.account < rhs.account
		}

		public var id : String
		{
			account.description
		}
	}
}

@resultBuilder
struct RecordBuilder
{
	static func buildPartialBlock(first : JournalRecord) -> [JournalRecord]
	{
		[first]
	}

	static func buildPartialBlock(accumulated : [JournalRecord], next : JournalRecord) -> [JournalRecord]
	{
		accumulated + [next]
	}
}

/// Recursively groups the records of the given collection matching the given set of record IDs.
/// - Returns: The collection of grouped records, or `nil` if grouping fails.
///
/// Grouping fails when
/// - no group could be created in the given records or their descendent records,
/// - fewer than two records IDs are given.
///
/// Higher-level records are not grouped if descendent records were grouped.
public func groupRecords<T : RecordEntryAccountType>(
	withIDs recordIDs : Set<Record<T>.ID>,
	in records : [Record<T>]
) -> [Record<T>]?
{
	guard recordIDs.count > 1 else { return nil }
	var didGroupSomeSubRecords : Bool = false
	var groupedRecords = [Record<T>]()
	groupedRecords.reserveCapacity(records.count)
	for var record in records {
		if let subRecords = record.subRecords,
			 let groupedSubRecords = groupRecords(withIDs: recordIDs, in: subRecords) {
			record.subRecords = groupedSubRecords
			didGroupSomeSubRecords = true
		}
		groupedRecords.append(record)
	}
	if didGroupSomeSubRecords {
		return groupedRecords
	}
	var groupedRootLevelRecords = [Record<T>]()
	var ungroupedRootLevelRecords = [Record<T>]()
	ungroupedRootLevelRecords.reserveCapacity(records.count)
	var insertionIndex = records.count
	for (index, record) in records.enumerated() {
		if recordIDs.contains(record.id) {
			insertionIndex = min(insertionIndex, index)
			groupedRootLevelRecords.append(record)
		} else {
			ungroupedRootLevelRecords.append(record)
		}
	}
	guard let firstGroupedRecord = groupedRootLevelRecords.first else { return nil }
	let newGroup = Record<T>(date: firstGroupedRecord.date, title: firstGroupedRecord.title + RecordGroupTitleDefaultSuffix, subRecords: groupedRootLevelRecords)
	if insertionIndex < ungroupedRootLevelRecords.count {
		ungroupedRootLevelRecords.insert(newGroup, at: insertionIndex)
	} else {
		ungroupedRootLevelRecords.append(newGroup)
	}
	return ungroupedRootLevelRecords
}

/// Recursively ungroups records of the given record collection matching the given record IDs.
/// - Returns: the ungrouped records, or `nil` if no records were ungrouped.
///
/// Ungroups descendent records first. If any descendent record was ungrouped,
/// none of the higher-level records will be ungrouped.
public func ungroupRecords<T : RecordEntryAccountType>(
	withIDs recordIDs : Set<Record<T>.ID>,
	in records : [Record<T>]
) -> [Record<T>]?
{
	guard !recordIDs.isEmpty else { return nil }
	var ungroupedRecords : [Record<T>] = []
	ungroupedRecords.reserveCapacity(records.count)
	var didUngroupSomeRecords : Bool = false
	// ungrouping descendent records
	for var record in records {
		if let subRecords = record.subRecords,
			 let ungroupedSubRecords = ungroupRecords(withIDs: recordIDs, in: subRecords) {
			record.subRecords = ungroupedSubRecords
			didUngroupSomeRecords = true
		}
		ungroupedRecords.append(record)
	}
	if !didUngroupSomeRecords {
		// ungrouping top-level records
		ungroupedRecords = []
		for record in records {
			if recordIDs.contains(record.id), let subRecords = record.subRecords {
				ungroupedRecords.append(contentsOf: subRecords)
				didUngroupSomeRecords = true
			} else {
				ungroupedRecords.append(record)
			}
		}
	}
	guard didUngroupSomeRecords else { return nil }
	return ungroupedRecords
}
