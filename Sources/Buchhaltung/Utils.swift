// Copyright 2022-2025 Kai Oezer

import Foundation

/// Restricts value assignments to strings that consist of digits of the given length.
@propertyWrapper public struct FixedLengthDigits : Codable, Equatable, Sendable
{
	private let _fixedLength : UInt8
	private var _internal : String = ""

	public init(_ length: UInt8)
	{
		_fixedLength = length
	}

	public var wrappedValue : String
	{
		get { _internal }
		set {
			if newValue.count <= _fixedLength {
				_internal = newValue
			} else {
				let clippedValue = newValue[newValue.startIndex ..< newValue.index(newValue.startIndex, offsetBy:Int(_fixedLength))]
				_internal = String(clippedValue)
			}
		}
	}

	/// - returns: whether the current value is a valid value
	public var projectedValue : Bool
	{
		_internal.wholeMatch(of: try! Regex("^\\d{\(_fixedLength)}$")) != nil
	}
}
