// Copyright 2022-2024 Kai Oezer

/// Stammdaten der Kapitalgesellschaft
public struct Company : Codable, Equatable, Sendable
{
	/// Sitz der Gesellschaft
	public struct Seat : Codable, Equatable, CustomStringConvertible, Sendable
	{
		public var street : String?
		public var houseNo : String?
		public var zipCode : String?
		public var cityOrRegion : String?
		public var country : String? = "Deutschland"
		public var note : String?

		public var description: String
		{
			let streetWithNumber = [street, houseNo].compactMap{$0}.joined(separator: " ")
			let noteWithStreet = [note, streetWithNumber].compactMap{$0}.joined(separator: ", ")
			let cityWithZip = [zipCode, cityOrRegion].compactMap{$0}.joined(separator: " ")
			let cityWithCountry = [cityWithZip, country].compactMap{$0}.joined(separator: ", ")
			return [noteWithStreet, cityWithCountry].compactMap{$0}.joined(separator: ", ")
		}
	}

	public enum LegalForm : String, Codable, Equatable, Sendable
	{
		case gmbh = "GmbH"
		case ug = "UG"
	}

	public struct TradeRegistration : Codable, Equatable, Sendable
	{
		/// German: Registergericht
		public var tradeCourt : String = ""

		/// German: Handelsregisternummer
		public var tradeRegistrationID : String = ""
	}

	public struct FiscalRegistration : Codable, Equatable, Sendable
	{
		/// 13-digit tax number
		@FixedLengthDigits(13) public var st13 : String

		/// 4-digit tax office ID (Bundesfinanzamtnummer)
		@FixedLengthDigits(4) public var bf4 : String
	}

	/// German: Gesellschafter(in)
	public struct Shareholder : Codable, Equatable, Identifiable, Sendable
	{
		public var id : Int

		public var name : String
		@FixedLengthDigits(13) public var taxNumber : String

		/// Vorname, bei natürlichen Personen
		public var personFirstName : String

		/// Steueridentifikationsnummer (IdNr), bei natürlichen Personen
		@FixedLengthDigits(11) public var personTaxID : String

		public var entry : Day
		public var exit : Day

		public var guaranteedAmount : Money
		public var assets : [String]

		public init(
			id : Int = 0,
			name : String = "",
			taxNumber : String = "",
			personFirstName : String = "",
			personTaxID : String = "",
			entry : Day = .distantFuture,
			exit : Day = .distantFuture,
			guaranteedAmount : Money = .zero,
			assets : [String] = []
		)
		{
			self.id = id
			self.name = name
			self.personFirstName = personFirstName
			self.entry = entry
			self.exit = exit
			self.guaranteedAmount = guaranteedAmount
			self.assets = assets
			self.taxNumber = taxNumber
			self.personTaxID = personTaxID
		}

		public var isMissingEntries : Bool
		{
			name.isEmpty
			|| (!personFirstName.isEmpty && !$personTaxID)
			|| (personFirstName.isEmpty && !$taxNumber)
			|| entry == .distantFuture
		}
	}

	@frozen public struct Executive : Codable, Equatable, Identifiable
	{
		public var id : Int
		public var name : String
		public var address : String

		public init(id : Int = 0, name : String = "", address : String = "")
		{
			self.id = id
			self.name = name
			self.address = address
		}

		public var isMissingEntries : Bool
		{
			name.isEmpty || address.isEmpty
		}
	}

	/// [§ 267 HGB](https://www.gesetze-im-internet.de/hgb/__267.html)
	public enum Size : String, Codable, Sendable
	{
		case tiny   /// Kleinstkapitalgesellschaft
		case small  /// Kleinkapitalgesellschaft
		case medium /// Mittelgroße Kapitalgesellschaft
		case large  /// Große Kapitalgesellschaft
	}

	/// German: Haftungsverhältnis, Eventualverbindlichkeit
	public struct ContingentLiability : Codable, Equatable, Identifiable, Sendable
	{
		// swiftlint:disable nesting
		public enum LiabilityType : String, Codable, Sendable
		{
			case associatedCompanies = "assc" /// gegenüber verbundenen Unternehmen
			case pension             = "pens" /// Altersversorgung
			case other               = "othr" /// alle anderen Arten
		}

		public var id : Int
		public var type : LiabilityType
		public var amount : Money = .zero
		public var note : String?

		public init(id : Int = 0, type : LiabilityType, amount : Money, note : String? = nil)
		{
			self.id = id
			self.type = type
			self.amount = amount
			self.note = note
		}
	}

	/// Vorschuss oder Kredit an Geschäftsführer
	public struct ExecutiveLoan : Codable, Equatable, Sendable
	{
		public let name : String

		/// German: Vorschüsse
		public var advancePayments : Money?

		/// German: gewährte Kredite
		public var creditsGranted : Money?

		/// German: Haftungsverhältnis
		public var liabilities : Money?
	}

	/// German: Firma
	/// array allows multiline names
	public var name : [String] = []

	/// optional path to the company logo image file
	public var logo : String?

	/// German: Firmensitz, Anschrift
	/// array allows multiline address
	public var seat = Seat()

	public var legalForm : LegalForm = .gmbh

	/// field of activity, very short business description
	/// German: Geschäftstätigkeit, Art des Unternehmens
	public var business : String = ""

	/// Gezeichnetes Kapital
	public var subscribedCapital : Money = .zero

	public var tradeRegistration = TradeRegistration()

	public var fiscalRegistration = FiscalRegistration()

	public var chiefExecutives = [Executive]()

	public var shareholders = [Shareholder]()

	/// German: Haftungsverhältnisse
	public var contingentLiabilities = [ContingentLiability]()

	public var executiveLoans = [ExecutiveLoan]()

	public var size : Size = .tiny

	public var numberOfEmployees : UInt = 0

	/// German: in Liquidation bzw. Abwicklung
	public var inLiquidation : Bool = false

	public init() {}
}
