// Copyright 2022-2025 Kai Oezer

import Foundation

extension Money
{
	private static let _decimalMoneyFormatter : NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.allowsFloats = true
		formatter.usesGroupingSeparator = true
		formatter.groupingSize = 3
		// Note: NumberFormatter does NOT follow the convention to omit
		// the grouping separator when there are only four digits (before the decimal marker).
		formatter.groupingSeparator = "." // space (" ") is preferred by ISO but intentionally using dot
		formatter.generatesDecimalNumbers = true
		formatter.decimalSeparator = "," // preferred by ISO
		formatter.minimumFractionDigits = 2
		formatter.maximumFractionDigits = 2
		return formatter
	}()

	public struct BHFormatStyle : ParseableFormatStyle
	{
		public init()
		{}

		public var parseStrategy : BHParseStrategy
		{
			BHParseStrategy()
		}

		public func format(_ money : Money) -> String
		{
			money.decimalStringRepresentation()
		}
	}

	public struct BHParseStrategy : ParseStrategy
	{
		public init()
		{}

		public func parse(_ input: String) throws -> Money
		{
			guard let token = input.components(separatedBy: .whitespaces).first else { return .zero }
			guard !token.contains(".") || token.contains(",") else { throw BHCodingError.invalidArchiveFormat } // "." apparently used as decimal separator
			let strippedToken = token.replacingOccurrences(of: ".", with: "")
			guard let number = _decimalMoneyFormatter.number(from: strippedToken) else {
				throw BHCodingError.invalidArchiveFormat
			}
			return Money(number.doubleValue)
		}
	}

	public var bh : String
	{
		self.formatted(BHFormatStyle())
	}
}

extension FormatStyle where Self == Money.BHFormatStyle
{
	public static var bhMoney : Money.BHFormatStyle
	{
		Money.BHFormatStyle()
	}
}

extension ParseStrategy where Self == Money.BHParseStrategy
{
	public static var bhMoney : Money.BHParseStrategy
	{
		Money.BHParseStrategy()
	}
}
