//  Copyright 2021-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

extension Inventory : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		var lines = [String]()
		lines.append(self.date.bh)
		lines.append(contentsOf:fixedAssetsValuation.sorted{ $0.key < $1.key }.filter{$0.value > .zero}.map{"fixd \($0.key.description) \($0.value.bh)"})
		lines.append(contentsOf:currentAssetsValuation.sorted{ $0.key < $1.key }.filter{$0.value > .zero}.map{"curr \($0.key.description) \($0.value.bh)"})
		if longTermDebtsValuation > .zero
		{
			lines.append("debt long \(longTermDebtsValuation.bh)")
		}
		if shortTermDebtsValuation > .zero
		{
			lines.append("debt short \(shortTermDebtsValuation.bh)")
		}
		return lines.joined(separator: "\n")
	}

	// swiftlint:disable function_body_length
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		enum DecodingState
		{
			case expectingDate
			case expectingAssetOrDebt
		}

		enum EntryType : String
		{
			case fixedAsset = "fixd"
			case currentAsset = "curr"
			case debt = "debt"
		}

		enum DebtType : String
		{
			case long
			case short
		}

		var decodingState = DecodingState.expectingDate
		var decodedDate : Day?
		var fixedAssetValuations = [Assets.FixedAssetType : Money]()
		var currentAssetValuations = [Assets.CurrentAssetType : Money]()
		var longTermDebts = Money.zero
		var shortTermDebts = Money.zero

		let decodeAssetOrDebt : (IndexedLine) -> Void = { line in
			let components = line.content.components(separatedBy: .whitespaces)
			guard components.count > 2,
				let type = EntryType(rawValue: components[0])
				else { return }
			if type == .fixedAsset,
				let fixedAssetType = Assets.FixedAssetType(from: components[1]),
				let value = try? Money(components[2...].joined(separator: " "), strategy:.bhMoney)
			{
				fixedAssetValuations[fixedAssetType] = value + (fixedAssetValuations[fixedAssetType] ?? .zero)
			}
			else if type == .currentAsset,
				let currentAssetType = Assets.CurrentAssetType(from: components[1]),
				let value = try? Money(components[2...].joined(separator: " "), strategy:.bhMoney)
			{
				currentAssetValuations[currentAssetType] = value
			}
			else if type == .debt,
				let debtType = DebtType(rawValue:components[1]),
				let value = try? Money(components[2...].joined(separator: " "), strategy:.bhMoney)
			{
				if debtType == .short {
					shortTermDebts += value
				} else {
					longTermDebts += value
				}
			}
		}

		for line in lines
		{
			if decodingState == .expectingDate
			{
				decodedDate = try? Day(line.content, strategy: .bhDay)
				decodingState = .expectingAssetOrDebt
			}
			else if decodingState == .expectingAssetOrDebt
			{
				decodeAssetOrDebt(line)
			}
		}

		guard let date = decodedDate else { throw BHCodingError.invalidArchiveFormat }

		self.date = date
		self.assets = nil
		self.fixedAssetDepreciationSinceLastPeriod = nil
		self.fixedAssetsValuation = fixedAssetValuations
		self.currentAssetsValuation = currentAssetValuations
		self.longTermDebtsValuation = longTermDebts
		self.shortTermDebtsValuation = shortTermDebts
	}
}
