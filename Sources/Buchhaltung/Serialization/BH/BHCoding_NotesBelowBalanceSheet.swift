//  Copyright 2020-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

extension NotesBelowBalanceSheet : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		"""
		\(try companyInformation.tomlikeEmbedded())
		meth \(incomeCalculationMethod == .ukv ? "UKV" : "GKV")
		"""
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		companyInformation = try Company(tomlikeEmbedded: lines, issues: &issues)
		var calculationMethodEntry = ""
		for line in lines
		{
			let content = line.content
			if content.starts(with: "meth ")
			{
				calculationMethodEntry = content[content.index(content.startIndex, offsetBy: 5)...].trimmingCharacters(in: .whitespaces)
				break
			}
		}
		incomeCalculationMethod = IncomeCalculationMethod(rawValue: calculationMethodEntry) ?? .gkv
	}

}
