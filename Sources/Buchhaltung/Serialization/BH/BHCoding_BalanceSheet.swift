//  Copyright 2021-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

private let id_asset = "asset"
private let id_asset_fixed = "fixed"
private let id_asset_current = "current"
private let id_asset_deferrals = "deferrals"
private let id_asset_deferredTax = "deferred_tax"
private let id_asset_surplus = "surplus"
private let id_eqLiab = "eqlia"
private let id_eqLiab_equity = "equity"
private let id_eqLiab_provisions = "provisions"
private let id_eqLiab_liabilites = "liabilities"
private let id_eqLiab_deferrals = "deferrals"
private let id_eqLiab_deferredTax = "deferred_tax"

extension BalanceSheet : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		var lines = [String]()

		let addAsset : (String, KeyPath<BalanceSheet.Assets, Money>) -> Void = { prefix, keypath in
			var line = "\(id_asset) \(prefix) \(assets[keyPath: keypath].bh)"
			let previousPeriodValue = previousPeriodAssets[keyPath: keypath]
			if previousPeriodValue > .zero
			{
				line += " \(previousPeriodValue.bh)"
			}
			lines.append(line)
		}

		let addEqLiab : (String, KeyPath<BalanceSheet.EquityAndLiabilities, Money>) -> Void = { prefix, keypath in
			var line = "\(id_eqLiab) \(prefix) \(eqLiab[keyPath: keypath].bh)"
			let previousPeriodValue = previousPeriodEqLiab[keyPath: keypath]
			if previousPeriodValue > .zero
			{
				line += " \(previousPeriodValue.bh)"
			}
			lines.append(line)
		}

		addAsset(id_asset_fixed, \.fixedAssets)
		addAsset(id_asset_current, \.currentAssets)
		addAsset(id_asset_deferrals, \.deferrals.monetaryValue)
		addAsset(id_asset_deferredTax, \.deferredTax.monetaryValue)
		addAsset(id_asset_surplus, \.surplus.monetaryValue)
		addEqLiab(id_eqLiab_equity, \.equity)
		addEqLiab(id_eqLiab_provisions, \.provisions)
		addEqLiab(id_eqLiab_liabilites, \.liabilities)
		addEqLiab(id_eqLiab_deferrals, \.deferrals.monetaryValue)
		addEqLiab(id_eqLiab_deferredTax, \.deferredTax.monetaryValue)

		return lines.joined(separator: "\n")
	}

	// swiftlint:disable cyclomatic_complexity function_body_length
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		var assets = Assets()
		var prevAssets = Assets()
		var eqLiab = EquityAndLiabilities()
		var prevEqLiab = EquityAndLiabilities()

		let decodeAsset : ([String]) -> Void = { components in
			guard let value = try? Money(components[2], strategy: .bhMoney) else { return }
			let prevValue =  components.count > 3 ? ((try? Money(components[3], strategy: .bhMoney)) ?? .zero) : .zero
			if components[1] == id_asset_fixed {
				assets.fixedAssets = value
				prevAssets.fixedAssets = prevValue
			} else if components[1] == id_asset_current {
				assets.currentAssets = value
				prevAssets.currentAssets = prevValue
			} else if components[1] == id_asset_deferrals {
				assets.deferrals = Deferrals(monetaryValue: value)
				prevAssets.deferrals = Deferrals(monetaryValue: prevValue)
			} else if components[1] == id_asset_deferredTax {
				assets.deferredTax = DeferredTax(monetaryValue: value)
				prevAssets.deferredTax = DeferredTax(monetaryValue: prevValue)
			} else if components[1] == id_asset_surplus {
				assets.surplus = SurplusFromOffsetting(monetaryValue: value)
				prevAssets.surplus = SurplusFromOffsetting(monetaryValue: prevValue)
			}
		}

		let decodeEqLiab : ([String]) -> Void = { components in
			guard let value = try? Money(components[2], strategy:.bhMoney) else { return }
			let prevValue =  components.count > 3 ? ((try? Money(components[3], strategy: .bhMoney)) ?? .zero) : .zero
			if components[1] == id_eqLiab_equity {
				eqLiab.equity = value
				prevEqLiab.equity = prevValue
			} else if components[1] == id_eqLiab_provisions {
				eqLiab.provisions = value
				prevEqLiab.provisions = prevValue
			} else if components[1] == id_eqLiab_liabilites {
				eqLiab.liabilities = value
				prevEqLiab.liabilities = prevValue
			} else if components[1] == id_eqLiab_deferrals {
				eqLiab.deferrals = Deferrals(monetaryValue: value)
				prevEqLiab.deferrals = Deferrals(monetaryValue: prevValue)
			} else if components[1] == id_eqLiab_deferredTax {
				eqLiab.deferredTax = DeferredTax(monetaryValue: value)
				prevEqLiab.deferredTax = DeferredTax(monetaryValue: prevValue)
			}
		}

		for line in lines
		{
			let components = line.content.components(separatedBy: .whitespaces)
			guard components.count > 1 else { continue }
			if components[0] == id_asset {
				decodeAsset(components)
			}
			else if components[0] == id_eqLiab {
				decodeEqLiab(components)
			}
		}

		self.assets = assets
		self.previousPeriodAssets = prevAssets
		self.eqLiab = eqLiab
		self.previousPeriodEqLiab = prevEqLiab
	}
}
