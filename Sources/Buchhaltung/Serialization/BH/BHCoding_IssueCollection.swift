// Copyright 2023-2024 Kai Oezer

import Foundation
import TOMLike
import IssueCollection
import RegexBuilder

public struct EmbeddableIssueCollection : TOMLikeEmbeddable, Codable, Sendable
{
	public var issues : IssueCollection

	public init(issues : IssueCollection = IssueCollection())
	{
		self.issues = issues
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues initializationIssues : inout IssueCollection) throws
	{
		issues = IssueCollection()
		for line in lines {
			guard let (_, domainAndCodeString, metadataString) = try? _linePattern.wholeMatch(in: line.content)?.output else { continue }
			guard let (domain, code) = _decodeDomainAndCode(domainAndCodeString) else { continue }
			let metadata = metadataString != nil ? _decodeMetadata(metadataString!) : [:]
			issues.append(Issue(domain: domain, code: code, metadata: metadata))
		}
	}

	public func tomlikeEmbedded() throws -> String
	{
		issues.issues.map{ issue in
			"\"\(issue.domain)\".\(issue.code) "
			+ issue.metadata.sorted{ $0.key < $1.key }.map{ "\"\($0.key)\":\"\($0.value)\"" }.joined(separator: ",")
		}.joined(separator: "\n")
	}

	public mutating func append(_ newIssues : IssueCollection)
	{
		issues.append(newIssues)
	}

	public mutating func clear()
	{
		issues.clear()
	}

	public var isEmpty : Bool
	{
		issues.isEmpty
	}

	private func _decodeDomainAndCode(_ input : Substring) -> (IssueDomain, IssueCode)?
	{
		let components = input.components(separatedBy: ".")
		guard components.count == 2 else { return nil }
		let domainString = components[0].trimmingCharacters(in: Self._quote)
		guard !domainString.isEmpty else { return nil }
		guard let code = Int(components[1]) else { return nil }
		return (IssueDomain(domainString), IssueCode(code))
	}

	private func _decodeMetadata(_ input : Substring) -> Issue.Metadata
	{
		var metadata = Issue.Metadata()
		let keyValuePairs = input.components(separatedBy: ",")
		for pair in keyValuePairs {
			let keyAndValue = pair.components(separatedBy: ":")
			guard keyAndValue.count == 2 else { continue }
			let key = keyAndValue[0].trimmingCharacters(in: Self._quote)
			let value = keyAndValue[1].trimmingCharacters(in: Self._quote)
			metadata[IssueMetadataKey(key)] = value
		}
		return metadata
	}

	private var _domainAndCodePattern : Regex<Substring> {
		/"[^"]*"\.\d*/
	}

	private var _metadataKeyValuePairPattern : Regex<Substring> {
		/"[^"]*":"[^"]*"/
	}

	private var _linePattern : Regex<Regex<(Substring, Regex<Regex<Substring>.RegexOutput>.RegexOutput, Regex<Substring>.RegexOutput?)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			ZeroOrMore{ " " }
			Capture {
				_domainAndCodePattern
			}
			Optionally {
				OneOrMore{ " " }
				Capture {
					_metadataKeyValuePairPattern
					ZeroOrMore {
						","
						_metadataKeyValuePairPattern
					}
				}
			}
			ZeroOrMore{ " " }
			Anchor.endOfLine
		}
	}

	private static let _quote = CharacterSet(charactersIn: "\"")
}

extension EmbeddableIssueCollection : Equatable
{
	public static func == (lhs: EmbeddableIssueCollection, rhs: EmbeddableIssueCollection) -> Bool
	{
		lhs.issues == rhs.issues
	}
}
