// Copyright 2021-2024 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

extension ReportType : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		self.rawValue
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		if lines.count > 1
		{
			lines[1...].forEach {
				issues.append(bhDecodingIssue(.unexpectedLine, line: $0.lineNumber, message: "Unexpected entry for report type."))
			}
		}

		guard lines.count > 0, let type = ReportType(rawValue: lines[0].content) else { throw BHCodingError.invalidArchiveFormat }
		self = type
	}
}
