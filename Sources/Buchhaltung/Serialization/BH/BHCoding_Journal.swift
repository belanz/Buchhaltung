// Copyright 2020-2025 Kai Oezer

import Foundation
import DEGAAP
import IssueCollection
import TOMLike
import RegexBuilder
import OrderedCollections
import DequeModule

extension Journal : TOMLikeEmbeddable
{
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		let parser = BHJournalParser(lines: lines)
		(self.records, self.chart) = parser.parse()
		issues.append(parser.issues)
	}

	public func tomlikeEmbedded() throws -> String
	{
		var archive = [String]()
		archive.append(BHJournalParser.chartPrefix + chart.bh)
		archive.append(contentsOf: BHJournalEncoder(records: records).encode())
		return archive.joined(separator: "\n\n")
	}
}

fileprivate class BHJournalEncoder
{
	let records : [JournalRecord]

	init(records : [JournalRecord])
	{
		self.records = records
	}

	func encode() -> [String]
	{
		recursiveEncode(records)
	}

	private func recursiveEncode(_ items : [JournalRecord], indentation ind : String = "") -> [String]
	{
		var result = [String]()
		let sortedItems = items.sorted { lItem, rItem in
			let lDate = lItem.date
			let rDate = rItem.date
			if lDate != rDate { return lDate < rDate }
			let lTitle = lItem.title
			let rTitle = rItem.title
			if lTitle != rTitle { return lTitle < rTitle }
			return lItem.id.uuidString < rItem.id.uuidString
		}
		sortedItems.forEach { record in
			if let subRecords = record.subRecords {
				let indIncrement = String(repeating: " ", count: BHJournalParser.groupLevelIndentationIncrement)
				result.append(ind + record.date.bh + "\n" + ind + BHJournalParser.groupStartPrefix + "\"\(record.title)\"")
				result.append(contentsOf: recursiveEncode(subRecords, indentation: ind + indIncrement))
				result.append(ind + BHJournalParser.groupClose + " # \(record.title)")
			} else {
				let recordLines : [String?] = [
					record.date.bh,
					"\"\(record.title)\"",
					noteLine(record),
					tagsLine(record),
					linkLines(record) ]
					+ entryLines(record.debit, "d")
					+ entryLines(record.credit, "c")
				result.append(recordLines.compactMap{$0}.map{ind + $0}.joined(separator: "\n"))
			}
		}
		return result
	}

	private func entryLines(_ entries : OrderedSet<JournalRecord.Entry>, _ entryPrefix : String) -> [String?]
	{
		entries.isEmpty ? [nil] : entries.map{"\(entryPrefix) \($0.account) \($0.value.bh)"}
	}

	private func noteLine(_ record : JournalRecord) -> String?
	{
		record.note.isEmpty ? nil : BHJournalParser.notePrefix + "\"\(record.note)\""
	}

	private func tagsLine(_ record : JournalRecord) -> String?
	{
		let nonEmptyTags = record.tags.filter{ !$0.trimmingCharacters(in: .whitespaces).isEmpty }
		guard !nonEmptyTags.isEmpty else { return nil }
		return BHJournalParser.tagsPrefix
			+ nonEmptyTags.sorted()
				.map{ "\"\($0)\"" }
				.joined(separator: "\(BHJournalParser.tagSeparator) ")
	}

	private func linkLines(_ record : JournalRecord) -> String?
	{
		record.links.isEmpty ? nil : record.links.map{
			BHJournalParser.linkPrefix + "\"\($0.url)\""
		}.joined(separator: "\n")
	}
}

fileprivate class BHJournalParser
{
	enum ParserState
	{
		case expectingChartOrDate
		case expectingDate
		case expectingDateOrGroupClosing
		case expectingRecordTitleOrGroup
		case expectingNoteOrTagsOrLinkOrDebit
		case expectingTagsOrLinkOrDebit
		case expectingLinkOrDebit
		case expectingDebitOrCredit
		case expectingCreditOrDate
		case expectingCreditOrDateOrGroupClosing
	}

	static let chartPrefix = "chart "
	static let tagsPrefix = "tags "
	static let tagSeparator = ","
	static let notePrefix = "note "
	static let linkPrefix = "link "
	static let groupStartPrefix = "group "
	static let groupClose = "close"
	static let groupLevelIndentationIncrement = 4

	public var issues = IssueCollection()
	private let lines : [IndexedLine]
	private var records = [JournalRecord]()
	private var parsedChart : DEGAAPSourceChart? = nil
	private var currentDate : Day?
	private var currentRecord : JournalRecord? = nil
	private var groupStack = Deque<JournalRecord>()
	private var state : ParserState = .expectingChartOrDate

	init(lines : [IndexedLine])
	{
		self.lines = lines.map{ IndexedLine($0.lineNumber, content: $0.content.trimmingCharacters(in: .whitespaces)) }
	}

	func parse() -> ([JournalRecord], DEGAAPSourceChart)
	{
		records.removeAll(keepingCapacity: true)
		issues.clear()
		parsedChart = nil
		currentDate = nil
		currentRecord = nil
		groupStack.removeAll(keepingCapacity: true)

		state = .expectingChartOrDate

		for line in lines
		{
			var didProcess = false
			switch state {
				case .expectingChartOrDate:
					didProcess = processChart(line) || processDate(line)
				case .expectingCreditOrDate:
					didProcess = processCredit(line) || processDate(line)
				case .expectingCreditOrDateOrGroupClosing:
					didProcess = processCredit(line) || processDate(line) || processGroupClosing(line)
				case .expectingDate:
					didProcess = processDate(line)
				case .expectingDateOrGroupClosing:
					didProcess = processDate(line) || processGroupClosing(line)
				case .expectingRecordTitleOrGroup:
					didProcess = processRecordTitle(line) || processGroup(line)
				case .expectingNoteOrTagsOrLinkOrDebit:
					didProcess = processNote(line) || processTags(line) || processLink(line) || processDebit(line)
				case .expectingTagsOrLinkOrDebit:
					didProcess = processTags(line) || processLink(line) || processDebit(line)
				case .expectingLinkOrDebit:
					didProcess = processLink(line) || processDebit(line)
				case .expectingDebitOrCredit:
					didProcess = processDebit(line) || processCredit(line)
			}
			if !didProcess {
				// skipping lines until next date line (or group closing)
				state = groupStack.isEmpty ? .expectingDate : .expectingDateOrGroupClosing
			}
		}
		// handling end of lines
		if let currentRecord, (state == .expectingCreditOrDate) || (state == .expectingCreditOrDateOrGroupClosing) {
			appendRecord(currentRecord)
		}

		return (records, parsedChart ?? .default)
	}

	private func appendRecord(_ decodedRecord : JournalRecord)
	{
		var foundDupe = false
		if var group = groupStack.popLast() {
			foundDupe = group.subRecords?.contains(decodedRecord) ?? false
			group.subRecords?.append(decodedRecord)
			groupStack.append(group)
		} else {
			foundDupe = records.contains(decodedRecord)
			records.append(decodedRecord)
		}
		if foundDupe {
			issues.append(Issue(domain: .bhCoding, code: .duplicateRecord, message: "Decoded duplicate journal record \"\(decodedRecord.title)\" (date: \(decodedRecord.date))."))
		}
	}

	private func processChart(_ line : IndexedLine) -> Bool
	{
		let content = line.content
		let chartName = content[content.index(content.startIndex, offsetBy: Self.chartPrefix.count)...].trimmingCharacters(in: .whitespaces)
		parsedChart = try? DEGAAPSourceChart(chartName, strategy: .bhChartSource)
		state = .expectingDate
		return parsedChart != nil
	}

	private func processDate(_ line : IndexedLine) -> Bool
	{
		guard let date = try? Day(line.content, strategy: .bhDay) else { return false }
		if let record = currentRecord {
			appendRecord(record)
			currentRecord = nil
		}
		currentDate = date
		state = .expectingRecordTitleOrGroup
		return true
	}

	private func processRecordTitle(_ line : IndexedLine) -> Bool
	{
		guard let recordTitle = BHCoding.decodeTitle(line.content) else { return false }
		guard let currentDate else { return false }
		currentRecord = JournalRecord(date: currentDate, title: recordTitle)
		state = .expectingNoteOrTagsOrLinkOrDebit
		return true
	}

	private func processGroup(_ line : IndexedLine) -> Bool
	{
		guard let groupTitle = extractQuotedString(from: line, prefixedBy: Self.groupStartPrefix),
			let currentDate else { return false }
		groupStack.append(Record(date: currentDate, title: groupTitle, subRecords: []))
		state = .expectingDateOrGroupClosing
		return true
	}

	private func processGroupClosing(_ line : IndexedLine) -> Bool
	{
		guard line.content == BHJournalParser.groupClose else { return false }
		if let record = currentRecord {
			appendRecord(record)
			currentRecord = nil
		}
		if let closedGroup = groupStack.popLast() {
			if var parentGroup = groupStack.popLast() {
				parentGroup.subRecords?.append(closedGroup)
				groupStack.append(parentGroup)
			} else {
				records.append(closedGroup)
			}
		} else {
			issues.append(Issue(domain: .bhCoding, code: .outOfPlaceGroupClosing, metadata: [.lineNumber : String(line.lineNumber.index)]))
		}
		return true
	}

	private func processNote(_ line : IndexedLine) -> Bool
	{
		guard let note = extractQuotedString(from: line, prefixedBy: Self.notePrefix) else { return false }
		currentRecord?.note = note
		state = .expectingTagsOrLinkOrDebit
		return true
	}

	private func processTags(_ line : IndexedLine) -> Bool
	{
		let content = line.content
		let tagTrimmingSet = CharacterSet.whitespaces.union(CharacterSet(charactersIn: "\""))
		guard content.starts(with: Self.tagsPrefix) else { return false }
		let tags = content[content.index(content.startIndex, offsetBy: Self.tagsPrefix.count)...]
			.components(separatedBy: Self.tagSeparator)
			.map { $0.trimmingCharacters(in: tagTrimmingSet) }
		currentRecord?.tags = Set(tags)
		state = .expectingLinkOrDebit
		return true
	}

	private func processLink(_ line : IndexedLine) -> Bool
	{
		guard let urlString = extractQuotedString(from: line, prefixedBy: Self.linkPrefix) else { return false }
		currentRecord?.links.append(Record.Link(url: urlString))
		state = .expectingLinkOrDebit
		return true
	}

	private func processDebit(_ line : IndexedLine) -> Bool
	{
		guard let entry = Self.debitEntry(from: line.content, issues: &issues) else { return false }
		currentRecord?.debit.append(entry)
		state = .expectingDebitOrCredit
		return true
	}

	private func processCredit(_ line : IndexedLine) -> Bool
	{
		guard let entry = Self.creditEntry(from: line.content, issues: &issues) else { return false }
		currentRecord?.credit.append(entry)
		state = groupStack.isEmpty ? .expectingCreditOrDate : .expectingCreditOrDateOrGroupClosing
		return true
	}

	private func extractQuotedString(from line : IndexedLine, prefixedBy prefix : String) -> String?
	{
		let pattern = Regex {
			Anchor.startOfLine
			ZeroOrMore { .whitespace }
			prefix
			ZeroOrMore { .whitespace }
			/"(.+)"/
			ZeroOrMore { .whitespace }
			Anchor.endOfLine
		}
		guard let (_ ,result) = line.content.wholeMatch(of: pattern)?.output else { return nil }
		return String(result)
	}

	private static func creditEntry(from input : String, issues : inout IssueCollection) -> JournalRecord.Entry?
	{
		let creditEntryPrefix = "c "
		guard input.hasPrefix(creditEntryPrefix) else { return nil }
		return entry(from: input, excluding: creditEntryPrefix, issues: &issues)
	}

	private static func debitEntry(from input : String, issues : inout IssueCollection) -> JournalRecord.Entry?
	{
		let debitEntryPrefix = "d "
		guard input.hasPrefix(debitEntryPrefix) else { return nil }
		return entry(from: input, excluding: debitEntryPrefix, issues: &issues)
	}

	private static func entry(from input : String, excluding prefix : String, issues : inout IssueCollection) -> JournalRecord.Entry?
	{
		let entryString = BHCoding.copy(input, excludingPrefix: prefix)
		let entryTokens = entryString.components(separatedBy: .whitespaces)
		guard let accountName = entryTokens.first else { return nil }
		guard let account = DEGAAPSourceChartItemID(accountName) else { return nil }
		guard let money = try? Money(entryTokens[1], strategy: .bhMoney) else {
			issues.append(Issue(domain: .bhCoding, code: .moneyFormat, message: "Unexpected money format in journal entry: \"\(entryTokens[1])\""))
			return nil
		}
		let entry = JournalRecord.Entry(account: account, value: money)
		return entry
	}
}
