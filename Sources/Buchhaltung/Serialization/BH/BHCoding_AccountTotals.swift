// Copyright 2022-2024 Kai Oezer

import IssueCollection
import TOMLike

extension AccountTotals : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		totals.sorted{$0.key < $1.key}.map{"\($0.0) \($0.1.bh)"}.joined(separator: "\n")
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		var totals = [AccountID : Money]()
		for line in lines
		{
			let components = line.content.components(separatedBy: .whitespaces)
			guard components.count == 2,
				let accountID = AccountID(components[0]),
				let value = try? Money(components[1], strategy: .bhMoney)
			else {
				issues.append(bhDecodingIssue(.accountTotalFormat, line: line.lineNumber, message: "Invalid format for account total."))
				continue
			}
			totals[accountID] = value
		}
		self.totals = totals
	}
}
