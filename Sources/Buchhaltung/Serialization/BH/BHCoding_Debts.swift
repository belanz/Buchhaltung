// Copyright 2023-2024 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

let bhDebtPrefix = "-"
let bhDebtPaymentPrefix = "paym"
let bhDebtLoanPrefix = "loan"

extension Debts : TOMLikeEmbeddable
{
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		enum DecodingState
		{
			case waitingForItem
			case expectingTitle
			case expectingLoan
			case expectingPayment
			case expectingPaymentOrNextItem
		}

		var state : DecodingState = .waitingForItem
		var currentDebt : Debt?
		var debts = [Debt]()

		let checkAndAddCurrentDebt : () -> () = {
			if let currentDebt, !currentDebt.name.isEmpty, currentDebt.loan.date.isValid {
				debts.append(currentDebt)
			}
			currentDebt = nil
		}

		for line in lines
		{
			let trimmedLine = line.content.trimmingCharacters(in: .whitespaces)

			switch state {
				case .expectingTitle:
					if let title = BHCoding.decodeTitle(trimmedLine) {
						assert(currentDebt != nil)
						currentDebt?.name = title
						state = .expectingLoan
					} else {
						state = .waitingForItem
					}
				case .expectingLoan:
					if let loan = _transaction(from: trimmedLine, prefixedBy: "\(bhDebtLoanPrefix) ") {
						assert(currentDebt != nil)
						currentDebt?.loan = loan
						state = .expectingPayment
					} else {
						state = .waitingForItem
					}
				case .expectingPayment, .expectingPaymentOrNextItem:
					if let payment = _transaction(from: trimmedLine, prefixedBy: "\(bhDebtPaymentPrefix) ") {
						assert(currentDebt != nil)
						currentDebt?.payments.append(payment)
						state = .expectingPaymentOrNextItem
					} else if state == .expectingPaymentOrNextItem {
						state = .waitingForItem
						fallthrough
					} else {
						state = .waitingForItem
					}
				case .waitingForItem:
					if trimmedLine == bhDebtPrefix {
						checkAndAddCurrentDebt()
						currentDebt = Debt()
						state = .expectingTitle
					}
			}
		}

		checkAndAddCurrentDebt()

		self.debts = Set(debts)
	}

	public func tomlikeEmbedded() throws -> String
	{
		var archive : [String] = []
		debts.sorted().forEach { debt in
			archive.append(
				"""
				\(bhDebtPrefix)
				\"\(debt.name)\"
				\(bhDebtLoanPrefix) \(debt.loan.date.bh) \(debt.loan.value.bh)
				"""
				)
			if !debt.payments.isEmpty {
				archive.append(
					debt.payments.map{"\(bhDebtPaymentPrefix) \($0.date.bh) \($0.value.bh)"}.joined(separator: "\n")
				)
			}
		}
		return archive.isEmpty ? "" : archive.joined(separator: "\n")
	}

	private func _transaction<T : StringProtocol>(from line : T, prefixedBy prefix : T) -> Transaction?
	{
		guard line.count > (prefix.count + 10), line.hasPrefix(prefix) else { return nil }
		let paymentTokens = line[line.index(line.startIndex, offsetBy: prefix.count)...].components(separatedBy: .whitespaces)
		guard paymentTokens.count > 1 else { return nil }
		guard let date = try? Day(paymentTokens[0], strategy: .bhDay) else { return nil }
		guard let value = try? Money(paymentTokens[1], strategy: .bhMoney) else { return nil }
		return .init(date: date, value: value)
	}
}
