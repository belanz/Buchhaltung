//  Copyright 2020-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

private let periodDateSeparator = "-"

extension Period : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		[self.start.bh, periodDateSeparator, self.end.bh].joined(separator: " ")
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		guard let tokens = lines.first?.content.components(separatedBy: .whitespaces),
			tokens.count == 3,
			tokens[1] == periodDateSeparator,
			let decodedStart = try? Day(tokens[0], strategy: .bhDay),
			let decodedEnd = try? Day(tokens[2], strategy: .bhDay)
			else { throw BHCodingError.invalidArchiveFormat }
		self.start = decodedStart
		self.end = decodedEnd
	}
}
