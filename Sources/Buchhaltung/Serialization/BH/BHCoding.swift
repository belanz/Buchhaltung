// Copyright 2020-2024 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

struct BHCoding
{
	/// A title line is a line which starts and ends with a quote symbol.
	static func decodeTitle(_ titleLine : String) -> String?
	{
		// checking for quotes at beginning and end
		guard let (_, title) = titleLine.wholeMatch(of: /^"(.*?)"$/ )?.output else { return nil }
		return String(title)
	}

	static func copy(_ input : String, excludingPrefix prefix : String) -> String
	{
		String(input[input.index(input.startIndex, offsetBy:prefix.count)...])
	}
}

enum BHCodingError : Error, CustomDebugStringConvertible
{
	case emptyArchive
	case noMatchingArchiveEntryFound(String)
	case invalidArchiveFormat
	case unsupportedType(String)

	static func matchingError(for path : [CodingKey]) -> BHCodingError
	{
		.noMatchingArchiveEntryFound("Could not find archive entry for \"\(path.map{$0.stringValue}.joined(separator: "."))\"")
	}

	var debugDescription: String
	{
		switch self
		{
			case .noMatchingArchiveEntryFound(let message) : return message
			case .emptyArchive : return "The archive to decode is empty."
			case .invalidArchiveFormat: return "The contents of the archive have an unexpected format."
			case .unsupportedType(let typeName): return "The type \"\(typeName)\" can not be represented in the BH format."
		}
	}
}

extension IssueDomain
{
	public static let bhCoding : IssueDomain = "BH Coding"
}

extension IssueCode
{
	public static let unexpectedLine : IssueCode = 100

	public static let duplicateRecord : IssueCode = 1010

	/// The given fixed asset is not of type `equipment` but uses GWG depreciation.
	public static let gwgButNotEquipment : IssueCode = 1020

	/// A derived current asset is not allowed to be of type `receivable`.`
	public static let derivedReceivableAsset : IssueCode = 1030
	
	public static let moneyFormat : IssueCode = 1040
	public static let accountTotalFormat : IssueCode = 1050
	public static let invalidCompanyInfoItem : IssueCode = 1060

	/// Encountered a group closing line in the journal without a group start.
	/// Metadata: line number
	public static let outOfPlaceGroupClosing : IssueCode = 1070
}

func bhDecodingIssue(_ code : IssueCode, line : LineNumber = .none, message : String? = nil) -> Issue
{
	let metadata : Issue.Metadata = [
		.lineNumber: String(line.index),
		.message: "Line \(line.index)\(message != nil ? ": \(message!)" : "")"
	]
	return Issue(domain: .bhCoding, code: code, metadata: metadata)
}
