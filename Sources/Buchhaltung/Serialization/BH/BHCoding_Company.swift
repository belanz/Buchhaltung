// Copyright 2022-2023 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

private let co_prefix_name = "name"
private let co_prefix_logo = "logo"
private let co_prefix_seat = "seat"
private let co_prefix_form = "form"
private let co_prefix_business = "does"
private let co_prefix_regc = "regc"
private let co_prefix_regno = "regn"
private let co_prefix_subscribedCap = "scap"
private let co_prefix_st13 = "st13"
private let co_prefix_bf4 = "bf4-"
private let co_prefix_ceo = "ceo-"
private let co_prefix_shareholder = "shrh"
private let co_prefix_size = "size"
private let co_prefix_employees = "empl"
private let co_prefix_liquidation = "liqu"
private let co_prefix_liability = "liab"
private let co_prefix_loan = "loan"
private let co_prefix_loan_advances = "advances"
private let co_prefix_loan_credits = "credits"
private let co_prefix_loan_liabilities = "liabilities"
private let co_separator = ";"

extension Company : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		var lines = [String]()
		lines.append("\(co_prefix_name) \(name.joined(separator: " "))")
		if let logoPath = logo?.trimmingCharacters(in: .whitespaces), !logoPath.isEmpty {
			lines.append("\(co_prefix_logo) \"\(logoPath)\"")
		}
		lines.append((try? seat.tomlikeEmbedded()) ?? "")
		lines.append("\(co_prefix_form) \(legalForm.rawValue)")
		if !business.isEmpty
		{
			lines.append("\(co_prefix_business) \(business)")
		}
		lines.append(tradeRegistration.tradeCourt.isEmpty ? "" : "\(co_prefix_regc) \(tradeRegistration.tradeCourt)")
		lines.append(tradeRegistration.tradeRegistrationID.isEmpty ? "" : "\(co_prefix_regno) \(tradeRegistration.tradeRegistrationID)")
		lines.append("\(co_prefix_subscribedCap) \(subscribedCapital.bh)")
		if !fiscalRegistration.st13.isEmpty
		{
			lines.append("\(co_prefix_st13) \(fiscalRegistration.st13)")
		}
		if !fiscalRegistration.bf4.isEmpty
		{
			lines.append("\(co_prefix_bf4) \(fiscalRegistration.bf4)")
		}
		lines.append(contentsOf: chiefExecutives.map{ "\(co_prefix_ceo) \(_encodeExecutive($0))" })
		lines.append(contentsOf: shareholders.map{ "\(co_prefix_shareholder) \(_encodeShareholder($0))" })
		lines.append("\(co_prefix_size) \(size.rawValue)")
		lines.append("\(co_prefix_employees) \(numberOfEmployees)")
		lines.append("\(co_prefix_liquidation) \(inLiquidation ? "yes" : "no")")
		lines.append(contentsOf: contingentLiabilities.map{ "\(co_prefix_liability) \(_encodeContingentLiability($0))" })
		lines.append(contentsOf: executiveLoans.map{ "\(co_prefix_loan) \(_encodeExecutiveLoan($0))" })
		return lines.filter{ !$0.isEmpty }.joined(separator: "\n")
	}

	// swiftlint:disable cyclomatic_complexity
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		var seatLines = [IndexedLine]()

		for line in lines
		{
			let tokens = line.content.components(separatedBy: " ")
			guard tokens.count > 0 else { return }
			let prefix = tokens[0]
			let content = String(line.content[line.content.index(line.content.startIndex, offsetBy: prefix.count)...]).trimmingCharacters(in: .whitespaces)
			if prefix == co_prefix_name { name.append(content) }
			else if prefix == co_prefix_logo { logo = _decodeLogo(from: content) }
			else if prefix == co_prefix_seat { seatLines.append(line) }
			else if prefix == co_prefix_form { legalForm = LegalForm(rawValue: content) ?? .gmbh }
			else if prefix == co_prefix_business { business = content }
			else if prefix == co_prefix_regc { tradeRegistration.tradeCourt = content }
			else if prefix == co_prefix_regno { tradeRegistration.tradeRegistrationID = content }
			else if prefix == co_prefix_liquidation { inLiquidation = (content == "yes") }
			else if prefix == co_prefix_subscribedCap { subscribedCapital = (try? Money(content, strategy: .bhMoney)) ?? .zero }
			else if prefix == co_prefix_size { size = Size(rawValue: content) ?? .tiny }
			else if prefix == co_prefix_employees { numberOfEmployees = UInt(content) ?? 0 }
			else if prefix == co_prefix_st13 { fiscalRegistration.st13 = content }
			else if prefix == co_prefix_bf4 { fiscalRegistration.bf4 = content }
			else if prefix == co_prefix_shareholder {
				if var shareholder = _decodeShareholder(from: content) {
					shareholder.id = shareholders.count
					shareholders.append(shareholder)
				}
			}
			else if prefix == co_prefix_ceo {
				if let executive = _decodeExecutive(from: content) {
					chiefExecutives.append(executive)
				}
			}
			else if prefix == co_prefix_liability {
				if var contLiab = _decodeContingentLiability(from: content) {
					contLiab.id = contingentLiabilities.count
					contingentLiabilities.append(contLiab)
				}
			}
			else if prefix == co_prefix_loan {
				if let loan = _decodeExecutiveLoan(from: content) {
					executiveLoans.append(loan)
				}
			}
		}

		seat = try Seat(tomlikeEmbedded: seatLines, issues: &issues)
	}

	private func _encodeShareholder(_ sh : Shareholder) -> String
	{
		"\(sh.name)\(co_separator) \(sh.taxNumber)\(co_separator) \(sh.personFirstName)\(co_separator) \(sh.personTaxID)\(co_separator) \(sh.guaranteedAmount.bh)\(co_separator) \(sh.entry)\(sh.exit != .distantFuture ? "\(co_separator) \(sh.exit)" : "")"
	}

	private func _decodeShareholder(from content : String) -> Shareholder?
	{
		let components = content.components(separatedBy: co_separator).map{ $0.trimmingCharacters(in: .whitespaces) }
		guard components.count > 5,
			let amount = try? Money(components[4], strategy: .bhMoney),
			let entryDate = try? Day(components[5], strategy: .bhDay)
			else { return nil }
		let name = components[0]
		let exitDate = ((components.count > 6) ? (try? Day(components[6], strategy: .bhDay)) : nil) ?? .distantFuture
		var shareholder = Shareholder(
			name: name,
			personFirstName: components[2],
			entry: entryDate,
			exit: exitDate,
			guaranteedAmount: amount)
		shareholder.taxNumber = components[1]
		shareholder.personTaxID = components[3]
		return shareholder
	}

	private func _encodeExecutive(_ exec : Executive) -> String
	{
		"\(exec.name)\(co_separator) \(exec.address)"
	}

	private func _decodeExecutive(from content : String) -> Executive?
	{
		let components = content.components(separatedBy: co_separator).map{ $0.trimmingCharacters(in: .whitespaces) }
		guard components.count == 2 else { return nil }
		let name = components[0]
		let address = components[1]
		return Executive(name: name, address: address)
	}

	private func _encodeContingentLiability(_ liability : ContingentLiability) -> String
	{
		"\(liability.type.rawValue)\(co_separator) \(liability.amount.bh)\(co_separator) \(liability.note ?? "")"
	}

	private func _decodeContingentLiability(from content : String) -> ContingentLiability?
	{
		let components = content.components(separatedBy: co_separator).map{ $0.trimmingCharacters(in: .whitespaces) }
		guard components.count > 1,
			let type = ContingentLiability.LiabilityType(rawValue: components[0]),
			let amount = try? Money(components[1], strategy: .bhMoney)
			else { return nil }
		let note : String? = (components.count > 2) ? components[2] : nil
		return ContingentLiability(type: type, amount: amount, note: note)
	}

	private func _encodeExecutiveLoan(_ loan : ExecutiveLoan) -> String
	{
		guard !loan.name.isEmpty else { return "" }
		let advances = loan.advancePayments == nil ? nil : "\(co_prefix_loan_advances) \(loan.advancePayments!.bh)"
		let credits = loan.creditsGranted == nil ? nil : "\(co_prefix_loan_credits) \(loan.creditsGranted!.bh)"
		let liabilities = loan.liabilities == nil ? nil : "\(co_prefix_loan_liabilities) \(loan.liabilities!.bh)"
		let loans = [advances, credits, liabilities].compactMap{$0}.joined(separator: co_separator + " ")
		return "\(loan.name)\(co_separator) \(loans)"
	}

	private func _decodeExecutiveLoan(from content : String) -> ExecutiveLoan?
	{
		let parts = content.components(separatedBy: co_separator).map{ $0.trimmingCharacters(in: .whitespaces) }
		guard parts.count > 1 else { return nil }
		let name = parts[0]
		var loan = ExecutiveLoan(name: name)
		for part in parts[1...]
		{
			let tokens = part.components(separatedBy: " ")
			if tokens.count == 2, let amount = try? Money(tokens[1], strategy: .bhMoney)
			{
				if tokens[0] == co_prefix_loan_advances {
					loan.advancePayments = amount
				}
				else if tokens[0] == co_prefix_loan_credits {
					loan.creditsGranted = amount
				}
				else if tokens[0] == co_prefix_loan_liabilities {
					loan.liabilities = amount
				}
			}
		}
		let hasEntry = loan.advancePayments != nil || loan.creditsGranted != nil || loan.liabilities != nil
		return hasEntry ? loan : nil
	}

	private func _decodeLogo(from content : String) -> String?
	{
		guard let patternMatch = content.wholeMatch(of: /^"(.*)"$/)?.output.1 else { return nil }
		let trimmedPath = patternMatch.trimmingCharacters(in: .whitespaces)
		guard !trimmedPath.isEmpty else { return nil }
		return trimmedPath
	}
}

// MARK: -

private let seat_prefix_street = "street"
private let seat_prefix_houseno = "houseno"
private let seat_prefix_zipcode = "zipcode"
private let seat_prefix_city = "city"
private let seat_prefix_country = "country"
private let seat_prefix_note = "note"

extension Company.Seat : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		let lines : [String?] = [
			note == nil ? nil : seat_prefix_note + " " + note!,
			street == nil ? nil : seat_prefix_street + " " + street!,
			houseNo == nil ? nil : seat_prefix_houseno + " " + houseNo!,
			zipCode == nil ? nil : seat_prefix_zipcode + " " + zipCode!,
			cityOrRegion == nil ? nil : seat_prefix_city + " " + cityOrRegion!,
			country == nil ? nil : seat_prefix_country + " " + country!
		]
		return lines.compactMap{$0}.map{ "seat \($0)" }.joined(separator: "\n")
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		let linePrefix = co_prefix_seat + " "
		var seat = Self.init()
		for indexedLine in lines
		{
			let line = indexedLine.content
			guard line.starts(with: linePrefix) else { continue }
			let tokens = line[line.index(line.startIndex, offsetBy: linePrefix.count)...].components(separatedBy: .whitespaces)
			guard tokens.count > 1 else { continue }
			if tokens[0] == seat_prefix_note {
				seat.note = tokens[1...].joined(separator: " ")
			} else if tokens[0] == seat_prefix_street {
				seat.street = tokens[1...].joined(separator: " ")
			} else if tokens[0] == seat_prefix_houseno {
				seat.houseNo = tokens[1...].joined(separator: " ")
			} else if tokens[0] == seat_prefix_zipcode {
				guard tokens.count == 2,
					tokens[1].wholeMatch(of: /^\d{5}$/) != nil
					else {
						issues.append(.init(domain: .bhCoding, code: .invalidCompanyInfoItem))
						continue
					}
				seat.zipCode = tokens[1]
			} else if tokens[0] == seat_prefix_city {
				seat.cityOrRegion = tokens[1...].joined(separator: " ")
			} else if tokens[0] == seat_prefix_country {
				seat.country = tokens[1...].joined(separator: " ")
			}
		}
		self = seat
	}
}

// MARK: -

extension Company.FiscalRegistration : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		""
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{

	}
}
