// Copyright 2021-2024 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

extension ReportTitle : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		string
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		if lines.count > 1
		{
			lines[1...].forEach {
				issues.append(bhDecodingIssue(.unexpectedLine, line: $0.lineNumber, message: "Unexpected entry for report title."))
			}
		}
		self.string = lines.count > 0 ? lines[0].content : ""
	}
}
