// Copyright 2020-2025 Kai Oezer

import Foundation

/// This money type is suitable for currencies which consist of two integer components;
/// the regular amount and the subamount, where the subamount has a unit value that
/// is a fraction of the regular unit value.
public struct Money : Codable, Sendable
{
	private var _value : Int = 0
	public static let subunitFactor : Int = 100

	// Small value for correcting floating point conversions that produce values below the correct value by some epsilon.
	private static let _floatingPointEpsilonCorrection = 0.001

	private init(value : Int)
	{
		_value = value
	}

	public init(_ number : Double)
	{
		let floatValue = number * Double(Self.subunitFactor)
		let correctedFloatValue = floatValue + Self._floatingPointEpsilonCorrection
		self.init(value: Int(floor(correctedFloatValue)))
	}

	public init(_ amount : Int, _ subAmount : UInt = 0)
	{
		_value = Self.subunitFactor * amount + amount.signum() * (Int(subAmount) % Self.subunitFactor)
	}

	public var negated : Money
	{
		Money(value: -1 * _value)
	}

	public var absolute : Money
	{
		Money(value:abs(_value))
	}

	public var components : (Int, UInt)
	{
		(_value / Self.subunitFactor, UInt(abs(_value) % Int(Self.subunitFactor)))
	}

	public func decimalStringRepresentation(
		usingCommaAsDecimalSeparator : Bool = true,
		withThousandsSeparator : Bool = true,
		suppressingZeroSubamounts : Bool = false
	) -> String
	{
		let comma : Character = ","
		let dot : Character = "."
		let tSep = usingCommaAsDecimalSeparator ? dot : comma
		let (amount, subamount) = components
		let amountString : String = {
			if withThousandsSeparator {
				var tokens = Array(String(abs(amount)).reversed())
				for insertionIndex in stride(from: 3, to: tokens.count, by: 3).reversed() {
					tokens.insert(tSep, at: insertionIndex)
				}
				return String(tokens.reversed())
			} else {
				return String(abs(amount))
			}
		}()
		let subamountString  : String = {
			guard !suppressingZeroSubamounts || subamount > 0 else { return "" }
			let decimalSeparator = String(usingCommaAsDecimalSeparator ? comma : dot)
			return decimalSeparator + subamount.formatted(.number.precision(.integerLength(2)))
		}()
		let sign = amount.signum() < 0 ? "-" : ""
		return sign + amountString + subamountString
	}
}

extension Money : AdditiveArithmetic
{
	public static let zero = Money(0)

	public static func - (lhs: Money, rhs: Money) -> Money
	{
		Money(value: lhs._value - rhs._value)
	}

	public static func -= (lhs: inout Self, rhs: Self)
	{
		lhs._value -= rhs._value
	}

	public static func + (lhs: Money, rhs: Money) -> Money
	{
		return Money(value: lhs._value + rhs._value)
	}

	public static func += (lhs: inout Self, rhs: Self)
	{
		lhs._value += rhs._value
	}
}

extension Money
{
	public static func / (lhs : Money, rhs : Money) -> Double
	{
		guard rhs != .zero else { return .nan }
		return Double(lhs._value) / Double(rhs._value)
	}

	public static func / (lhs : Money, rhs : UInt) -> Money
	{
		Money(value: lhs._value / Int(rhs))
	}

	public static func /= (lhs : inout Money, rhs : UInt)
	{
		lhs._value /= Int(rhs)
	}

	public static func * (lhs : Money, rhs : Int) -> Money
	{
		Money(value: lhs._value * rhs)
	}

	public static func * (lhs : Int, rhs : Money) -> Money
	{
		Money(value: lhs * rhs._value)
	}
}

extension Money : CustomStringConvertible
{
	public var description : String
	{
		decimalStringRepresentation()
	}
}

extension Money : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(_value)
	}
}

extension Money : Comparable
{
	public static func < (lhs: Money, rhs: Money) -> Bool
	{
		lhs._value < rhs._value
	}

	public static func <= (lhs: Money, rhs: Money) -> Bool
	{
		lhs._value <= rhs._value
	}

	public static func > (lhs: Money, rhs: Money) -> Bool
	{
		lhs._value > rhs._value
	}

	public static func >= (lhs: Money, rhs: Money) -> Bool
	{
		lhs._value >= rhs._value
	}
}

extension Money : Equatable
{
	public static func == (lhs: Money, rhs: Money) -> Bool
	{
		lhs._value == rhs._value
	}
}

extension Money : ExpressibleByFloatLiteral
{
	public init(floatLiteral value: Double)
	{
		self.init(value)
	}
}

extension Money : ExpressibleByIntegerLiteral
{
	public init(integerLiteral value: Int)
	{
		self.init(value)
	}
}

extension Money
{
	public func formatted<T : FormatStyle>(_ formatStyle : T) -> String where T.FormatInput == Money, T.FormatOutput == String
	{
		formatStyle.format(self)
	}

	init<T : ParseStrategy>(_ value : String, strategy : T) throws where T.ParseInput == String, T.ParseOutput == Money
	{
		self = try strategy.parse(value)
	}
}

// MARK: -

public protocol MoneyEquivalent
{
	var monetaryValue : Money { get }
}

extension Money : MoneyEquivalent
{
	public var monetaryValue : Money { self }
}

public func + (lhs : MoneyEquivalent, rhs : MoneyEquivalent) -> Money { lhs.monetaryValue + rhs.monetaryValue }
public func - (lhs : MoneyEquivalent, rhs : MoneyEquivalent) -> Money { lhs.monetaryValue - rhs.monetaryValue }
public func / (lhs : MoneyEquivalent, rhs : MoneyEquivalent) -> Double { lhs.monetaryValue / rhs.monetaryValue }
public func * (multiplier : Int, money : MoneyEquivalent) -> Money { multiplier * money.monetaryValue }
public func / (money : MoneyEquivalent, divisor : UInt) -> Money { money.monetaryValue / divisor }

// MARK: -

public typealias Euro = Money

// MARK: -

public enum Currency : Int, Codable
{
	case euro    = 1
	case dollar  = 2
	case rmb     = 3
	case yen     = 4
	case pound   = 5
}

/// Supported currencies for ``Money``.
/// Sub-units (e.g., Cent and Penny) are not smaller than a 100th of the main unit.
extension Currency
{
	public var symbol : String
	{
		switch self
		{
			case .euro   : return "€"
			case .dollar : return "$"
			case .yen    : return "¥"
			case .pound  : return "£"
			case .rmb    : return "元"
		}
	}

	public var code : String
	{
		switch self
		{
			case .euro    : return "EUR"
			case .dollar  : return "USD"
			case .yen     : return "JPY"
			case .pound   : return "GBP"
			case .rmb     : return "CNY"
		}
	}

	public init?(_ code : String)
	{
		if      code == "EUR" { self = .euro }
		else if code == "USD" { self = .dollar }
		else if code == "JPY" { self = .yen }
		else if code == "GBP" { self = .pound }
		else if code == "CNY" { self = .rmb }
		else { return nil }
	}

	/// the number of significant digits after decimal
	var significantDigitsAfterDecimal : Int
	{
		switch self
		{
			case .yen : return 0
			default : return 2
		}
	}
}

extension Currency
{
	func formatted<T : FormatStyle>(_ style : T) -> String where T.FormatInput == Currency, T.FormatOutput == String
	{
		style.format(self)
	}
}
