// Copyright 2020-2024 Kai Oezer

import Foundation
import Buchhaltung
import IssueCollection

struct Utils
{
	static func load<T : Archivable>(_ archivable : T.Type, from path : FilePath, issues : inout IssueCollection) throws -> T
	{
		do {
			let archive = try _contentsOfFile(at: path)
			let result = try T(fromArchive: archive, issues: &issues)
			return result
		} catch let error {
			throw BHCLIError.general("Error while loading instance of \(archivable) from \(path). \(error)")
		}
	}

	private static func _contentsOfFile(at path : FilePath) throws -> String
	{
		guard let fileData = FileManager.default.contents(atPath: path.path),
			let contents = String(bytes: fileData, encoding: .utf8)
			else { throw BHCLIError.general("Could not load the contents of \(path).") }
		return contents
	}

	static func write(contents : String, to fileName : String, in folder : URL, overwrite : Bool) throws
	{
		let filePath = folder.appendingPathComponent(fileName).path
		guard !contents.isEmpty else { throw BHCLIError.noData(filePath) }
		let fm = FileManager.default
		var isDir : ObjCBool = false
		if fm.fileExists(atPath: filePath, isDirectory: &isDir)
		{
			if !overwrite
			{
				throw BHCLIError.fileExists(filePath)
			}
			guard !isDir.boolValue else { throw BHCLIError.general("The specified output file is a directory.") }
			guard fm.isWritableFile(atPath: filePath) else { throw BHCLIError.general("The specified output file exists but is not writable.") }
			try contents.write(toFile: filePath, atomically: false, encoding: .utf8)
		}
		else
		{
			if !fm.createFile(atPath: filePath, contents: contents.data(using: .utf8), attributes: nil)
			{
				throw BHCLIError.fileCouldNotBeCreated(filePath)
			}
		}
	}

}
