// Copyright 2020-2024 Kai Oezer

import Buchhaltung
import ArgumentParser

@main
struct BHCLICommand : AsyncParsableCommand
{
	static var _commandName: String { "buchhalter" }

	static let configuration = CommandConfiguration(
		abstract: "Generates a business report (Abschlussbericht) or partial report for the given Buchhaltung ledger file, in a simplified reporting structure allowed for very small capital companies (Kleinstkapitalgesellschaft).",
		subcommands: [ReportSubcommand.self, AccountTrackingSubcommand.self],
		defaultSubcommand: ReportSubcommand.self
	)
}

enum BHCLIError : Error, CustomStringConvertible
{
	case general(String)
	case noData(String)
	case fileExists(String)
	case fileDoesNotExist(String)
	case folderDoesNotExist(String)
	case fileCouldNotBeCreated(String)

	var description: String
	{
		switch self
		{
			case .general(let message): return message
			case .noData(let filePath): return "No data available to write to \(filePath)"
			case .fileExists(let filePath): return "The file already exists: \(filePath)"
			case .fileDoesNotExist(let filePath): return "The file does not exist: \(filePath)"
			case .folderDoesNotExist(let folderPath): return "The folder does not exist: \(folderPath)"
			case .fileCouldNotBeCreated(let filePath): return "Could not create a file at \(filePath)"
		}
	}
}

struct CommonOptions : ParsableArguments
{
	@Argument(help: "The path to a Buchhaltung ledger file.")
	var ledgerPath : FilePath
}
