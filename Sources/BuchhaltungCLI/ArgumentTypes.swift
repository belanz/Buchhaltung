// Copyright 2020-2023 Kai Oezer

import Foundation
import Buchhaltung
import ArgumentParser

struct FolderPath : ExpressibleByArgument, CustomStringConvertible
{
	let path : String

	init?(argument : String)
	{
		let path = (argument as NSString).expandingTildeInPath
		var isDir : ObjCBool = false
		guard FileManager.default.fileExists(atPath: path, isDirectory: &isDir) && isDir.boolValue else { return nil }
		self.path = path
	}

	var url : URL { URL(fileURLWithPath: path) }
	var description: String { path }

	static var workingDir : FolderPath
	{
		FolderPath(argument: FileManager.default.currentDirectoryPath)!
	}
}

struct FilePath : ExpressibleByArgument, CustomStringConvertible
{
	let path : String

	init?(argument : String)
	{
		let path = (argument as NSString).expandingTildeInPath
		var isDir : ObjCBool = false
		guard FileManager.default.fileExists(atPath: path, isDirectory: &isDir) && !isDir.boolValue && FileManager.default.isReadableFile(atPath: path) else { return nil }
		self.path = path
	}

	var url : URL { URL(fileURLWithPath: path) }
	var description: String { path }

	var folder : FolderPath
	{
		let folder = FolderPath(argument: self.url.deletingLastPathComponent().path)
		assert(folder != nil)
		return folder!
	}

	var fileBaseName : String
	{
		self.url.deletingPathExtension().lastPathComponent
	}
}

struct DayArgument : ExpressibleByArgument, CustomStringConvertible
{
	let day : Day

	init?(argument : String)
	{
		guard let day = Day(string: argument) else { return nil }
		self.day = day
	}

	var description : String
	{
		day.bh
	}
}

struct AccountIDArgument : ExpressibleByArgument, CustomStringConvertible
{
	let accountID : AccountID

	init?(argument : String)
	{
		guard let accountID = AccountID(argument) else { return nil }
		self.accountID = accountID
	}

	var description : String
	{
		accountID.description
	}
}
