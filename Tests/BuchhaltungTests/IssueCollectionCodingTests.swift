// Copyright 2023-2024 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct IssueCollectionCodingTests
{
	@Test("encoding")
	func encoding() throws
	{
		let encodedIssueCollection = try TOMLikeCoder.encode(collection1)
		#expect(encodedIssueCollection == bhCodedCollection1)
	}

	@Test("decoding")
	func Decoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedIssueCollection = try TOMLikeCoder.decode(EmbeddableIssueCollection.self, from: bhCodedCollection1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decodedIssueCollection == collection1)
	}

	private var collection1 : EmbeddableIssueCollection
	{
		var collection = IssueCollection()
		collection.append(Issue(domain: .bhCoding, code: .gwgButNotEquipment, metadata: [.message : "Hello", .taxonomy : "6.6"]))
		collection.append(Issue(domain: .ledger, code: .journalFixedAssetNotListed, metadata: [.message : "Hello again", .day : "2023-07-14"]))
		return EmbeddableIssueCollection(issues: collection)
	}

	private let bhCodedCollection1 =
	"""
	"BH Coding".1020 "degaap_taxonomy":"6.6","message":"Hello"
	"Ledger".11 "day":"2023-07-14","message":"Hello again"
	"""
}
