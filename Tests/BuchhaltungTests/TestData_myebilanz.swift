// Copyright 2022-2023 Kai Oezer

import Foundation
@testable import Buchhaltung

extension TestData
{
	static let myebilanzINI1 =
		"""
		[magic]
		myebilanz=true
		guid=E949F34C-6E81-4297-B5EA-FF9399764D9C

		[general]
		; Replace the fields below with the contact data of the persons...
		; ... responsible for transferring this report to the tax authority,
		Transferdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>
		; ... and for supplying the report data.
		Nutzdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>

		[cert]
		; Replace <path> below with the absolute path (in Windows notation) to the Elster certificate file.
		file=<path to certification file>
		; Replace <password> below with the password for your Elster account.
		pin=<password>

		[csv]
		; Replace <path> below with the absolute path (in Windows notation) to the folder containing the CSV account totals file.
		filename=<path>\\<csv file name>
		delimiter=;
		fieldKto=1
		fieldValue=2
		fieldValueDebit=0
		fieldValueCredit=0
		fieldXBRL=3
		fieldName=4

		[period]
		balSheetClosingDate=2019-12-31

		[report]
		reportType=JA
		reportStatus=E
		revisionStatus=E
		reportElements=-GuV,GuVMicroBilG,B,-SGE,-KS,-STU,-EB,-SGEP,-KKE,-SA,-AV,BAL,-EV,-BVV
		statementType=E
		statementTypeTax=-GHB
		incomeStatementendswithBalProfit=false
		accountingStandard=HAOE
		specialAccountingStandard=K
		incomeStatementFormat=GKV
		consolidationRange=EA
		taxonomy=6.3
		MicroBilG=1

		[company]
		name=My Small Company
		legalStatus=GmbH
		street=Ballindamm
		houseNo=2
		zipCode=20095
		city=Hamburg
		country=Deutschland
		ST13=1234567890123
		BF4=1234
		incomeClassification=trade
		business=Einzelhandel
		size=tiny

		[xbrl]
		; Special account "0".
		; The value is automatically set to the total of the income (GuV) accounts.
		de-gaap-ci:bs.eqLiab.equity.netIncome=0
		; The values for the following accounts are in the CSV file.
		de-gaap-ci:bs.ass.currAss.cashEquiv.bank=1
		de-gaap-ci:bs.ass.currAss.receiv.other.vat=2
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office=3
		de-gaap-ci:bs.eqLiab.equity.subscribed.corp=4
		de-gaap-ci:bs.eqLiab.liab.other.shareholders=5
		de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc=6
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.legalConsulting=7
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc=8

		[bal]
		; bs.ass.fixAss.tan.otherEquipm.office
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:grossCost.beginning="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.addition="[108.40]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.release="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.beginning="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.DeprPeriod.regular="[6.02]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:all_Prev_period="[0]"

		[ini]
		AutoSum=1
		"""

	static let myebilanzCSV1 =
		"""
		1; 6684.07; de-gaap-ci:bs.ass.currAss.cashEquiv.bank; "-"
		2; 44.88; de-gaap-ci:bs.ass.currAss.receiv.other.vat; "-"
		3; 102.38; de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office; "-"
		4; -7000; de-gaap-ci:bs.eqLiab.equity.subscribed.corp; "-"
		5; -150; de-gaap-ci:bs.eqLiab.liab.other.shareholders; "-"
		6; 6.02; de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc; "-"
		7; 127.75; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.legalConsulting; "-"
		8; 184.90; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc; "-"
		"""

	static let myebilanzINI2 =
		"""
		[magic]
		myebilanz=true
		guid=E949F34C-6E81-4297-B5EA-FF9399764D9C

		[general]
		; Replace the fields below with the contact data of the persons...
		; ... responsible for transferring this report to the tax authority,
		Transferdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>
		; ... and for supplying the report data.
		Nutzdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>

		[cert]
		; Replace <path> below with the absolute path (in Windows notation) to the Elster certificate file.
		file=<path to certification file>
		; Replace <password> below with the password for your Elster account.
		pin=<password>

		[csv]
		; Replace <path> below with the absolute path (in Windows notation) to the folder containing the CSV account totals file.
		filename=<path>\\<csv file name>
		delimiter=;
		fieldKto=1
		fieldValue=2
		fieldValueDebit=0
		fieldValueCredit=0
		fieldXBRL=3
		fieldName=4

		[period]
		balSheetClosingDate=2020-12-31

		[report]
		reportType=JA
		reportStatus=E
		revisionStatus=E
		reportElements=-GuV,GuVMicroBilG,B,-SGE,-KS,-STU,-EB,-SGEP,-KKE,-SA,-AV,BAL,-EV,-BVV
		statementType=E
		statementTypeTax=-GHB
		incomeStatementendswithBalProfit=false
		accountingStandard=HAOE
		specialAccountingStandard=K
		incomeStatementFormat=GKV
		consolidationRange=EA
		taxonomy=6.3
		MicroBilG=1

		[company]
		name=My Small Company
		legalStatus=GmbH
		street=Ballindamm
		houseNo=2
		zipCode=20095
		city=Hamburg
		country=Deutschland
		ST13=1234567890123
		BF4=1234
		incomeClassification=trade
		business=Einzelhandel
		size=tiny

		[xbrl]
		; Special account "0".
		; The value is automatically set to the total of the income (GuV) accounts.
		de-gaap-ci:bs.eqLiab.equity.netIncome=0
		; The values for the following accounts are in the CSV file.
		de-gaap-ci:bs.ass.currAss.cashEquiv.bank=1
		de-gaap-ci:bs.ass.currAss.receiv.other.vat=2
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg=3
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office=4
		de-gaap-ci:bs.eqLiab.accruals=5
		de-gaap-ci:bs.eqLiab.defIncome=6
		de-gaap-ci:bs.eqLiab.defTax=7
		de-gaap-ci:bs.eqLiab.equity.retainedEarnings.finalPrev=8
		de-gaap-ci:bs.eqLiab.equity.subscribed.corp=9
		de-gaap-ci:bs.eqLiab.liab=10

		[bal]
		; bs.ass.fixAss.tan.otherEquipm.gwg
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:grossCost.beginning="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.addition="[461.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.release="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.beginning="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.DeprPeriod.regular="[460.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:all_Prev_period="[0]"
		; bs.ass.fixAss.tan.otherEquipm.office
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:grossCost.beginning="[108.40]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.addition="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.release="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.beginning="[6.02]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.DeprPeriod.regular="[36.12]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:all_Prev_period="[102.38]"

		[ini]
		AutoSum=1
		"""

	static let myebilanzCSV2 =
		"""
		1; 5720.27; de-gaap-ci:bs.ass.currAss.cashEquiv.bank; "-"
		2; 1.29; de-gaap-ci:bs.ass.currAss.receiv.other.vat; "-"
		3; 1; de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg; "-"
		4; 66.26; de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office; "-"
		5; 0; de-gaap-ci:bs.eqLiab.accruals; "-"
		6; 0; de-gaap-ci:bs.eqLiab.defIncome; "-"
		7; 0; de-gaap-ci:bs.eqLiab.defTax; "-"
		8; 1211.18; de-gaap-ci:bs.eqLiab.equity.retainedEarnings.finalPrev; "-"
		9; -7000; de-gaap-ci:bs.eqLiab.equity.subscribed.corp; "-"
		10; 0; de-gaap-ci:bs.eqLiab.liab; "-"
		"""

	static let myebilanzINI3 =
		"""
		[magic]
		myebilanz=true
		guid=E949F34C-6E81-4297-B5EA-FF9399764D9C

		[general]
		; Replace the fields below with the contact data of the persons...
		; ... responsible for transferring this report to the tax authority,
		Transferdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>
		; ... and for supplying the report data.
		Nutzdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>

		[cert]
		; Replace <path> below with the absolute path (in Windows notation) to the Elster certificate file.
		file=<path to certification file>
		; Replace <password> below with the password for your Elster account.
		pin=<password>

		[csv]
		; Replace <path> below with the absolute path (in Windows notation) to the folder containing the CSV account totals file.
		filename=<path>\\<csv file name>
		delimiter=;
		fieldKto=1
		fieldValue=2
		fieldValueDebit=0
		fieldValueCredit=0
		fieldXBRL=3
		fieldName=4

		[period]
		balSheetClosingDate=2021-12-31

		[report]
		reportType=JA
		reportStatus=E
		revisionStatus=E
		reportElements=-GuV,GuVMicroBilG,B,-SGE,-KS,-STU,-EB,-SGEP,-KKE,-SA,-AV,BAL,-EV,BVV
		statementType=E
		statementTypeTax=-GHB
		incomeStatementendswithBalProfit=false
		accountingStandard=HAOE
		specialAccountingStandard=K
		incomeStatementFormat=GKV
		consolidationRange=EA
		taxonomy=6.4
		MicroBilG=1

		[company]
		name=My Small Company
		legalStatus=GmbH
		street=Ballindamm
		houseNo=2
		zipCode=20095
		city=Hamburg
		country=Deutschland
		ST13=1234567890123
		BF4=1234
		incomeClassification=trade
		business=Einzelhandel
		size=tiny

		[xbrl]
		; Special account "0".
		; The value is automatically set to the total of the income (GuV) accounts.
		de-gaap-ci:bs.eqLiab.equity.netIncome=0
		; The values for the following accounts are in the CSV file.
		de-gaap-ci:bs.ass.currAss.cashEquiv.bank=1
		de-gaap-ci:bs.ass.currAss.receiv.other.vat=2
		de-gaap-ci:bs.ass.deficitNotCoveredByCapital=3
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg=4
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office=5
		de-gaap-ci:bs.eqLiab.accruals.other.other=6
		de-gaap-ci:bs.eqLiab.defIncome=7
		de-gaap-ci:bs.eqLiab.defTax=8
		de-gaap-ci:bs.eqLiab.equity.profitLoss.retainedEarnings=9
		de-gaap-ci:bs.eqLiab.equity.retainedEarnings.finalPrev=10
		de-gaap-ci:bs.eqLiab.equity.subscribed.corp=11
		de-gaap-ci:bs.eqLiab.liab.other.other=12
		de-gaap-ci:BVV.profitLoss.assetsCurrentYear=13
		de-gaap-ci:BVV.profitLoss.assetsPreviousYear.assets=14
		de-gaap-ci:BVV.profitLoss.contribution=15
		de-gaap-ci:BVV.profitLoss.withdrawalDistrib=16
		de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAs=17
		de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc=18
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.communication=19
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.concessLicenses=20
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.disposFixAss.misc=21
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.leaseFix.shareholders=22
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc=23
		de-gaap-ci:is.netIncome.regular.operatingTC.staff.salaries.managerPartner=24

		[bal]
		; bs.ass.fixAss.tan.otherEquipm.gwg
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:grossCost.beginning="[461.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.addition="[1477.32]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.release="[738.66]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.beginning="[460.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.DeprPeriod.regular="[737.66]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:all_Prev_period="[1]"
		; bs.ass.fixAss.tan.otherEquipm.office
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:grossCost.beginning="[108.40]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.addition="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.release="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.beginning="[42.14]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.DeprPeriod.regular="[36.12]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:all_Prev_period="[66.26]"

		[ini]
		AutoSum=1
		"""

	static let myebilanzCSV3 =
		"""
		1; 3618.73; de-gaap-ci:bs.ass.currAss.cashEquiv.bank; "-"
		2; 0; de-gaap-ci:bs.ass.currAss.receiv.other.vat; "-"
		3; 0; de-gaap-ci:bs.ass.deficitNotCoveredByCapital; "-"
		4; 2; de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg; "-"
		5; 30.14; de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office; "-"
		6; 0; de-gaap-ci:bs.eqLiab.accruals.other.other; "-"
		7; 0; de-gaap-ci:bs.eqLiab.defIncome; "-"
		8; 0; de-gaap-ci:bs.eqLiab.defTax; "-"
		9; 0; de-gaap-ci:bs.eqLiab.equity.profitLoss.retainedEarnings; "-"
		10; 1211.18; de-gaap-ci:bs.eqLiab.equity.retainedEarnings.finalPrev; "-"
		11; -7000; de-gaap-ci:bs.eqLiab.equity.subscribed.corp; "-"
		12; 0; de-gaap-ci:bs.eqLiab.liab.other.other; "-"
		13; -3650.87; de-gaap-ci:BVV.profitLoss.assetsCurrentYear; "-"
		14; -5788.82; de-gaap-ci:BVV.profitLoss.assetsPreviousYear.assets; "-"
		15; 0; de-gaap-ci:BVV.profitLoss.contribution; "-"
		16; 0; de-gaap-ci:BVV.profitLoss.withdrawalDistrib; "-"
		17; 1475.32; de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAs; "-"
		18; 36.12; de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc; "-"
		19; 36; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.communication; "-"
		20; 60; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.concessLicenses; "-"
		21; 1; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.disposFixAss.misc; "-"
		22; 36; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.leaseFix.shareholders; "-"
		23; 492.51; de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc; "-"
		24; 1; de-gaap-ci:is.netIncome.regular.operatingTC.staff.salaries.managerPartner; "-"
		"""

	static let myebilanzINI3WithAccountValues =
		"""
		[magic]
		myebilanz=true
		guid=E949F34C-6E81-4297-B5EA-FF9399764D9C

		[general]
		; Replace the fields below with the contact data of the persons...
		; ... responsible for transferring this report to the tax authority,
		Transferdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>
		; ... and for supplying the report data.
		Nutzdatenlieferant=<name>;<street>;<house no>;<house no additional>;<address additional>;<zip code>;<city or region>;<country>;<phone no>;<e-mail>

		[cert]
		; Replace <path> below with the absolute path (in Windows notation) to the Elster certificate file.
		file=<path to certification file>
		; Replace <password> below with the password for your Elster account.
		pin=<password>

		[csv]
		; Replace <path> below with the absolute path (in Windows notation) to the folder containing the CSV account totals file.
		filename=INI
		delimiter=;
		fieldKto=1
		fieldValue=2
		fieldValueDebit=0
		fieldValueCredit=0
		fieldXBRL=3
		fieldName=4

		[period]
		balSheetClosingDate=2021-12-31

		[report]
		reportType=JA
		reportStatus=E
		revisionStatus=E
		reportElements=-GuV,GuVMicroBilG,B,-SGE,-KS,-STU,-EB,-SGEP,-KKE,-SA,-AV,BAL,-EV,BVV
		statementType=E
		statementTypeTax=-GHB
		incomeStatementendswithBalProfit=false
		accountingStandard=HAOE
		specialAccountingStandard=K
		incomeStatementFormat=GKV
		consolidationRange=EA
		taxonomy=6.4
		MicroBilG=1

		[company]
		name=My Small Company
		legalStatus=GmbH
		street=Ballindamm
		houseNo=2
		zipCode=20095
		city=Hamburg
		country=Deutschland
		ST13=1234567890123
		BF4=1234
		incomeClassification=trade
		business=Einzelhandel
		size=tiny

		[xbrl]
		; Special account "0".
		; The value is automatically set to the total of the income (GuV) accounts.
		de-gaap-ci:bs.eqLiab.equity.netIncome=0
		; The values for the following accounts are in the CSV file.
		de-gaap-ci:bs.ass.currAss.cashEquiv.bank=3618.73
		de-gaap-ci:bs.ass.currAss.receiv.other.vat=0
		de-gaap-ci:bs.ass.deficitNotCoveredByCapital=0
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg=2
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office=30.14
		de-gaap-ci:bs.eqLiab.accruals.other.other=0
		de-gaap-ci:bs.eqLiab.defIncome=0
		de-gaap-ci:bs.eqLiab.defTax=0
		de-gaap-ci:bs.eqLiab.equity.profitLoss.retainedEarnings=0
		de-gaap-ci:bs.eqLiab.equity.retainedEarnings.finalPrev=1211.18
		de-gaap-ci:bs.eqLiab.equity.subscribed.corp=-7000
		de-gaap-ci:bs.eqLiab.liab.other.other=0
		de-gaap-ci:BVV.profitLoss.assetsCurrentYear=-3650.87
		de-gaap-ci:BVV.profitLoss.assetsPreviousYear.assets=-5788.82
		de-gaap-ci:BVV.profitLoss.contribution=0
		de-gaap-ci:BVV.profitLoss.withdrawalDistrib=0
		de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAs=1475.32
		de-gaap-ci:is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc=36.12
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.communication=36
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.concessLicenses=60
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.disposFixAss.misc=1
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.leaseFix.shareholders=36
		de-gaap-ci:is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc=492.51
		de-gaap-ci:is.netIncome.regular.operatingTC.staff.salaries.managerPartner=1

		[bal]
		; bs.ass.fixAss.tan.otherEquipm.gwg
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:grossCost.beginning="[461.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.addition="[1477.32]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.release="[738.66]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.beginning="[460.34]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:accDepr.DeprPeriod.regular="[737.66]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.gwg!de-gaap-ci:all_Prev_period="[1]"
		; bs.ass.fixAss.tan.otherEquipm.office
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:grossCost.beginning="[108.40]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.addition="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.release="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:gross.movements="[0]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.beginning="[42.14]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:accDepr.DeprPeriod.regular="[36.12]"
		de-gaap-ci:bs.ass.fixAss.tan.otherEquipm.office!de-gaap-ci:all_Prev_period="[66.26]"

		[ini]
		AutoSum=1
		"""
}
