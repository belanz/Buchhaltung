// Copyright 2020-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct AssetTests
{
	@Test("inventory fixed asset accounts")
  func inventoryFixedAssetAccounts()
  {
    let fixedAssetAccountsUsedForReports = [DEGAAPAccounts.ass_fixed_equipment_gwg] +
      Assets.FixedAssetType.allCases.map{ Assets.FixedAsset(registrationID: "test ID", type: $0, name: "test").account }
		#expect(Set(fixedAssetAccountsUsedForReports) == Set(DEGAAPAccounts.inventoryFixedAssetAccounts))
  }

	@Test("adding and clearing assets")
	func addingAssets()
	{
		var inventory = Assets()
		TestData.fixedAssets1.forEach{ inventory.add(fixedAsset: $0) }
		#expect(inventory.fixedAssets.count == 2)
		inventory.removeAllFixedAssets()
		#expect(inventory.fixedAssets.count == 0)
		inventory.add(fixedAssets: Set(TestData.fixedAssets1))
		#expect(inventory.fixedAssets.count == 2)
	}

	@Test("sorting fixed assets")
	func sortingFixedAssets()
	{
		let fa : (String, String, Day, Assets.FixedAssetType) -> Assets.FixedAsset = {
			Assets.FixedAsset(registrationID: $0, type: $3, name: $1, acquisitionDate: $2)
		}
		let fa1  = fa("8", "A", Day(2022,03,05)!, .equipment)
		let fa2  = fa("9", "A", Day(2022,03,05)!, .equipment)
		let fa3  = fa("5", "F", Day(2022,03,05)!, .equipment)
		let fa4  = fa("2", "A", Day(2022,03,05)!, .machinery)
		let fa5  = fa("2", "B", Day(2022,03,05)!, .machinery)
		let fa6  = fa("2", "C", Day(2022,03,05)!, .machinery)
		let fa7  = fa("8", "A", Day(2022,03,05)!, .officeEquipment)
		let fa8  = fa("8", "A", Day(2022,03,05)!, .factoryEquipment)
		let fa9 = fa("5", "A", Day(2022,04,10)!, .equipment)
		let fa10 = fa("5", "A", Day(2022,04,12)!, .equipment)
		let fa11 = fa("5", "A", Day(2022,04,13)!, .equipment)
		let fa12 = fa("5", "A", Day(2022,04,14)!, .equipment)
		#expect(Set([fa2, fa1, fa3]).sorted() == [fa3, fa1, fa2])
		#expect(Set([fa6, fa4, fa5]).sorted() == [fa4, fa5, fa6])
		#expect(Set([fa6, fa5, fa8, fa7]).sorted() == [fa5, fa6, fa8, fa7])
		#expect(Set([fa12, fa10, fa11, fa9]).sorted() == [fa9, fa10, fa11, fa12])
	}

	@Test("sorting current assets")
	func sortingCurrentAssets()
	{
		let ca : (String, Money, Assets.CurrentAssetType) -> Assets.CurrentAsset = {
			Assets.CurrentAsset(type: $2, name: $0, value: $1)
		}
		let ca1 = ca("A", 5000, .finishedGoods)
		let ca2 = ca("B", 6000, .finishedGoods)
		let ca3 = ca("B", 7000, .finishedGoods)
		let ca4 = ca("A", 5000, .cashEquivalent)
		let ca5 = ca("A", 5000, .material)
		let ca6 = ca("A", 5000, .receivable)
		let ca7 = ca("A", 5000, .inProgress)
		let ca8 = ca("C", 2000, .receivable)
		let ca9 = ca("D", 2000, .receivable)
		let ca10 = ca("E", 2000, .receivable)
		#expect(Set([ca1, ca2, ca3]).sorted() == [ca3, ca2, ca1])
		#expect(Set([ca4, ca5, ca6, ca7]).sorted() == [ca5, ca7, ca6, ca4])
		#expect(Set([ca10, ca9, ca8]).sorted() == [ca8, ca9, ca10])
	}

	@Test("archiving")
	func archiving() throws
	{
		var inventory = Assets()
		inventory.add(fixedAssets: Set(TestData.fixedAssets1))
		let archivedInventory = try TOMLikeCoder.encode(inventory)
		#expect(archivedInventory == TestData.archivedAssets1)

		inventory.removeAllFixedAssets()
		inventory.add(fixedAssets: Set(TestData.fixedAssets2))
		inventory.add(currentAssets: Set(TestData.currentAssets1))
		let archivedInventory2 = try TOMLikeCoder.encode(inventory)
		#expect(archivedInventory2 == TestData.archivedAssets2)
	}

	@Test("unarchiving")
	func unarchiving() throws
	{
		var issues = IssueCollection()
		let assets = try TOMLikeCoder.decode(Assets.self, from: TestData.archivedAssets2, issues: &issues)
		#expect(issues.count == 0)
		#expect(assets.fixedAssets.count == 3)
		let fixedAsset1 = try #require(assets.fixedAssets.first(where:{$0.registrationID == "5"}))
		#expect(fixedAsset1 == TestData.fixedAssets2[1])
		#expect(fixedAsset1.acquisitionDate == TestData.fixedAssets2[1].acquisitionDate)
		#expect(fixedAsset1.acquisitionCost == TestData.fixedAssets2[1].acquisitionCost)
		let fixedAsset2 = try #require(assets.fixedAssets.first(where:{$0.registrationID == "4"}))
		#expect(fixedAsset2 == TestData.fixedAssets2[0])
		#expect(fixedAsset2.acquisitionDate == TestData.fixedAssets2[0].acquisitionDate)
		#expect(fixedAsset2.acquisitionCost == TestData.fixedAssets2[0].acquisitionCost)
		#expect(assets.currentAssets.count == 1)
		#expect(assets.currentAssets.first! == TestData.currentAssets1[0])
	}

	@Test("fixed asset book value")
	func fixedAssetBookValue()
	{
		let asset = TestData.fixedAssets1.first!
		let value = asset.bookValue(for: Day(2020, 12, 31)!)
		#expect(value == 211.65)
	}

	@Test("Restwert")
	func restwert()
	{
		var asset = Assets.FixedAsset(registrationID: "1")
		asset.type = .equipment
		asset.name = "Computermonitor"
		asset.acquisitionDate = Day(2019, 3, 10)!
		asset.acquisitionCost = 320
		asset.depreciationSchedule = Assets.DepreciationSchedule(progression: .gwg, depreciationYears: 3)
		let reportingDate = Day(2020, 1, 1)!
		let residualValue = asset.bookValue(for: reportingDate)
		#expect(residualValue == 1)
	}
}
