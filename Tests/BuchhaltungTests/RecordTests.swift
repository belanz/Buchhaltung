// Copyright 2023-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import DEGAAP
import OrderedCollections

struct RecordTests
{
	@Test
	func initializationWithResultBuilder() throws
	{
		let record = JournalRecord(date: Day(2023, 7, 6)!, title: "Parent") {
			JournalRecord(date: Day(2023, 7, 6)!, title: "Child 1", debit:[.init(account: .init("1200")!, value: 100)])
			JournalRecord(date: Day(2023, 7, 6)!, title: "Child 2", credit:[.init(account: .init("1200")!, value: 200)])
		}
		let subRecords = try #require(record.subRecords)
		#expect(subRecords.count == 2)
	}

	@Test("copy has the same ID")
	func copyHasSameID()
	{
		let record = JournalRecord(date: Day(2023, 7, 6)!, title: "Original", debit:[.init(account: .init("1200")!, value: 100)])
		let copy = record
		#expect(record.id == copy.id)
	}

	@Test
	func value()
	{
		#expect(_record([200, 1500, 120], [50, 150, 250]).value == 1820)
		#expect(_record([10,20,30], [60]).value == 60)
		#expect(_record([10,20,30], [60, 700, 9]).value == 769)
		#expect(_record([10], []).value == 10)
	}

	@Test
	func isBalanced()
	{
		#expect(_record([60], [50]).isBalanced == false)
		#expect(_record([60], [50, 10]).isBalanced == true)
		#expect(_record([], []).isBalanced == false)

		let recordWithEmptyDesc = JournalRecord(date: .init(2023, 7, 6)!, title: "",
			debit: [.init(account: .none, value: 50)],
			credit: [.init(account: DEGAAPSourceChartItemID("1000")!, value: 50)]
		)
		#expect(recordWithEmptyDesc.isBalanced == false)

		let recordWithEmptyCredit = JournalRecord(date: .init(2023, 7, 6)!, title: "R1",
			debit: [.init(account: DEGAAPSourceChartItemID("1000")!, value: 50)],
			credit: [.init(account: .none, value: 0)]
		)
		#expect(recordWithEmptyCredit.isBalanced == false)

		let recordWithZeroTotal = JournalRecord(date: .init(2023, 7, 6)!, title: "",
			debit: [.init(account: .init("2000")!, value: 0)],
			credit: [.init(account: .init("3000")!, value: 50)]
		)
		#expect(recordWithZeroTotal.isBalanced == false)

		let recordWithBothZeroTotal = JournalRecord(date: .init(2023, 7, 6)!, title: "",
			debit: [.init(account: .init("2000")!, value: 0)],
			credit: [.init(account: .init("3000")!, value: 0)]
		)
		#expect(recordWithBothZeroTotal.isBalanced == false)

		#expect(_record([70, 0], [50, 20]).isBalanced == true)

		let group1 = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Child 1", debit: [.init(account: TestData.skr04_cost_misc, value: 100)], credit: [.init(account: TestData.skr04_bank, value: 100)])
			JournalRecord(date: Day(2025,3,22)!, title: "Child 2", debit: [.init(account: TestData.skr04_equipment_office, value: 3_000)], credit: [.init(account: TestData.skr04_bank, value: 3_000)])
		}
		#expect(group1.isBalanced == true)

		let group2 = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Child 1", debit: [.init(account: TestData.skr04_cost_misc, value: 100)], credit: [.init(account: TestData.skr04_bank, value: 500)])
			JournalRecord(date: Day(2025,3,22)!, title: "Child 2", debit: [.init(account: TestData.skr04_equipment_office, value: 3_000)], credit: [.init(account: TestData.skr04_bank, value: 3_000)])
		}
		#expect(!group2.isBalanced == true)
	}

	@Test
	func comparing()
	{
		let r1 = JournalRecord(date: Day(2023, 7, 26)!, title: "A")
		let r2 = JournalRecord(date: Day(2023, 5, 26)!, title: "A")
		let r3 = JournalRecord(date: Day(2023, 7, 26)!, title: "AB")
		let r4 = JournalRecord(date: Day(2023, 7, 26)!, title: "  A     ")
		#expect(r2 < r1)
		#expect(r1 < r3)
		#expect(r1 != r4)
		#expect(r1.instantiationTime <= r4.instantiationTime)
		#expect(r1 <= r4)
		#expect(r1 <= r1)
		#expect((r3 <= r1) == false)
		#expect(r3 > r2)
		#expect(r3 > r1)

		var r11 = JournalRecord(date: Day(2023, 7, 26)!, title: "A")
		r11.instantiationTime = r1.instantiationTime.advanced(by: .seconds(1))
		#expect(r1 < r11)

		var r12 = JournalRecord(date: Day(2023, 7, 26)!, title: "A")
		r12.instantiationTime = r1.instantiationTime
		#expect((r1.id < r12.id) ? (r1 < r12) : (r12 < r1))
	}

	@Test
	func hashing()
	{
		let r1 = JournalRecord(date: Day(2025,2,13)!, title: "R1")
		let r2 = JournalRecord(date: Day(2025,2,13)!, title: "R2")
		let dict : [JournalRecord : String] = [
			r1 : "A",
			r2 : "B",
		]
		#expect(dict[r1] == "A")
		#expect(dict[r2] == "B")

		let r3 = r1
		#expect(dict[r3] == "A")

		let r4 = JournalRecord(date: Day(2025,2,13)!, title: "R1")
		#expect(dict[r4] == "A")

		var r5 = JournalRecord(date: Day(2025,2,13)!, title: "R1", note: "equivalent to R1 except for this note")
		r5.instantiationTime = r1.instantiationTime
		#expect(dict[r5] == nil)
	}

	@Test
	func duplicating() throws
	{
		let r1 = JournalRecord(
			date: Day(2025,03,20)!,
			title: "Acquisition",
			note: "equipment",
			debit: OrderedSet([
				JournalRecord.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 100)
			]))
		let r2 = r1.duplicated
		#expect(r1 == r2)
		#expect(r1.id != r2.id)

		let group1 = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Child 1", debit: [.init(account: TestData.skr04_cost_misc, value: 100)], credit: [.init(account: TestData.skr04_bank, value: 100)])
			JournalRecord(date: Day(2025,3,22)!, title: "Child 2", debit: [.init(account: TestData.skr04_equipment_office, value: 3_000)], credit: [.init(account: TestData.skr04_bank, value: 3_000)])
			JournalRecord(date: Day(2025,3,21)!, title: "Nested Group") {
				JournalRecord(date: Day(2025,3,21)!, title: "Grandchild 1", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
				JournalRecord(date: Day(2025,3,22)!, title: "Grandchild 2", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
			}
		}
		let group2 = group1.duplicated
		#expect(group1 == group2)
		try #require(group2.subRecords != nil)
		#expect(group1.subRecords!.first! == group2.subRecords!.first!)
		#expect(group1.subRecords!.first!.id != group2.subRecords!.first!.id)
		try #require(group2.subRecords![2].subRecords != nil)
		#expect(group2.subRecords![2].subRecords!.count == 2)
		#expect(group1.subRecords![2].subRecords!.first! == group2.subRecords![2].subRecords!.first!)
		#expect(group1.subRecords![2].subRecords!.first!.id != group2.subRecords![2].subRecords!.first!.id)
	}

	@Test("copying non-group with duplication")
	func copyNonGroupWithDuplication() throws
	{
		let record = JournalRecord(date: Day(2025,4,10)!, title: "Some Record", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 300)])
		let (copy1, duplicate1, didDuplicateSubRecord1) = record.copy(duplicatingRecordsMatching: [])
		#expect(copy1 == record)
		#expect(duplicate1 == nil)
		#expect(didDuplicateSubRecord1 == false)
		let (copy2, duplicate2, didDuplicateSubRecord2) = record.copy(duplicatingRecordsMatching: [record.id])
		#expect(copy2 == record)
		#expect(duplicate2 == record)
		#expect(didDuplicateSubRecord2 == false)
	}

	@Test("copying group with duplication", arguments: [
		(true , false, false, 4, 3, 0, 0, []),
		(false, false, true , 5, 3, 1, 3, ["Target 1"]),
		(false, false, true , 4, 4, 2, 2, ["Target 2"]),
		(false, false, true , 5, 4, 2, 2, ["Target 1", "Target 2"]), /// expecting: "Target 1" and "Target 2" will both be duplicated because they lie on different branches
		(false, false, true , 5, 4, 2, 2, ["Target 1", "Target 2", "Nested Group"]) /// expecting: "Nested Group" will not be duplicated because of its duplicated sub-record "Target 2"
	])
	func copyGroupWithDuplication(
		expectingNewGroupEqualsOld : Bool,
		expectingDuplicatedRecord : Bool,
		expectingDuplicatedSubRecords : Bool,
		expectedNumberOfSubRecords : Int,
		expectedNumberOfSubSubRecords : Int,
		expectedDuplicationLevel : Int, ///< at which hierarchy level the duplication occurs, must be in 0...2
		expectedDuplicationItemIndex : Int, ///< the index of the sub-record that was duplicated
		targetNames : [String]
	) throws
	{
		let targetRecord1 = JournalRecord(date: Day(2025,3,21)!, title: "Target 1", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 300)])
		let targetRecord2 = JournalRecord(date: Day(2025,3,21)!, title: "Target 2", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 150)])
		let nestedGroup = JournalRecord(date: Day(2025,3,20)!, title: "Nested Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Grandchild 1", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
			JournalRecord(date: Day(2025,3,22)!, title: "Grandchild 2", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
			targetRecord2
		}
		let group = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Child 1", debit: [.init(account: TestData.skr04_cost_misc, value: 100)], credit: [.init(account: TestData.skr04_bank, value: 100)])
			JournalRecord(date: Day(2025,3,22)!, title: "Child 2", debit: [.init(account: TestData.skr04_equipment_office, value: 3_000)], credit: [.init(account: TestData.skr04_bank, value: 3_000)])
			nestedGroup
			targetRecord1
		}
		let targets = [targetRecord1, targetRecord2, nestedGroup].filter{ targetNames.contains($0.title) }
		let targetIDs = Set(targets.map{$0.id})

		let (newGroup, duplicateRecord, subRecordWasDuplicated) = group.copy(duplicatingRecordsMatching: targetIDs)
		#expect(expectingNewGroupEqualsOld ? (newGroup == group) : ((newGroup == group) == false))
		#expect(expectingDuplicatedRecord ? (duplicateRecord != nil) : (duplicateRecord == nil))
		#expect(subRecordWasDuplicated == expectingDuplicatedSubRecords)
		let newSubRecords = try #require(newGroup.subRecords)
		try #require(newSubRecords.count == expectedNumberOfSubRecords)
		if expectedDuplicationLevel == 1 {
			#expect(newSubRecords[expectedDuplicationItemIndex] == newSubRecords[expectedDuplicationItemIndex + 1])
		}
		let newSubSubRecords = try #require(newSubRecords[2].subRecords)
		try #require(newSubSubRecords.count == expectedNumberOfSubSubRecords)
		if expectedDuplicationLevel == 2 {
			#expect(newSubSubRecords[expectedDuplicationItemIndex] == newSubSubRecords[expectedDuplicationItemIndex + 1])
		}
	}

	@Test("path to record")
	func pathToRecord() throws
	{
		let targetRecord = JournalRecord(date: Day(2025,4,10)!, title: "R1")
		let otherRecord = JournalRecord(date: Day(2025,4,10)!, title: "X")
		let group = JournalRecord(date: Day(2025,4,10)!, title: "Group1") {
			JournalRecord(date: Day(2025,4,10)!, title: "R2", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
			JournalRecord(date: Day(2025,4,10)!, title: "R3", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
			JournalRecord(date: Day(2025,4,10)!, title: "Group2") {
				JournalRecord(date: Day(2025,4,10)!, title: "R4", debit: [.init(account: TestData.skr04_cost_misc, value: 400)], credit: [.init(account: TestData.skr04_bank, value: 400)])
				JournalRecord(date: Day(2025,4,10)!, title: "R5", debit: [.init(account: TestData.skr04_equipment_office, value: 1000)], credit: [.init(account: TestData.skr04_bank, value: 1000)])
				JournalRecord(date: Day(2025,4,10)!, title: "Group3") {
					JournalRecord(date: Day(2025,4,10)!, title: "R6", debit: [.init(account: TestData.skr04_cost_misc, value: 600)], credit: [.init(account: TestData.skr04_bank, value: 600)])
					JournalRecord(date: Day(2025,4,10)!, title: "R7", debit: [.init(account: TestData.skr04_equipment_office, value: 700)], credit: [.init(account: TestData.skr04_bank, value: 700)])
					targetRecord
				}
			}
		}
		#expect(group.pathToRecord(withID: targetRecord.id)?.map{$0.title} == ["Group1", "Group2", "Group3", "R1"])
		#expect(group.pathToRecord(withID: otherRecord.id) == nil)
	}

	@Test("finding sub-record by ID")
	func findingRecordByID() throws
	{
		let targetRecord = JournalRecord(date: Day(2025,3,21)!, title: "Target", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 300)])
		let nestedGroup = JournalRecord(date: Day(2025,3,21)!, title: "Nested Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Grandchild 1", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
			JournalRecord(date: Day(2025,3,22)!, title: "Grandchild 2", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
			targetRecord
		}
		let group = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			JournalRecord(date: Day(2025,3,21)!, title: "Child 1", debit: [.init(account: TestData.skr04_cost_misc, value: 100)], credit: [.init(account: TestData.skr04_bank, value: 100)])
			JournalRecord(date: Day(2025,3,22)!, title: "Child 2", debit: [.init(account: TestData.skr04_equipment_office, value: 3_000)], credit: [.init(account: TestData.skr04_bank, value: 3_000)])
			nestedGroup
		}
		#expect(group.record(withID: targetRecord.id) == targetRecord)
		let rogueRecord = JournalRecord(date: Day(2050, 1, 1)!, title: "rogue record")
		#expect(group.record(withID: rogueRecord.id) == nil)
	}

	@Test("updating records")
	func updatingRecords() throws
	{
		let record1 = JournalRecord(date: Day(2025,4,10)!, title: "R1")
		let record2 = JournalRecord(date: Day(2025,4,10)!, title: "R2")
		let group = JournalRecord(date: Day(2025,4,10)!, title: "Group") {
			record1
			record2
		}
		let record3 = JournalRecord(date: Day(2025,4,11)!, title: "R3")

		var group1 = group
		var didUpdate = group1.updateRecord(withID: record3.id, value: record2)
		#expect(didUpdate == false)

		let expectedGroup1 = JournalRecord(date: Day(2025,4,10)!, title: "Group") {
			record1
			record3
		}

		didUpdate = group1.updateRecord(withID: record2.id, value: record3)
		#expect(didUpdate == true)
		#expect(group1 == expectedGroup1)
	}

	@Suite("Grouping", .tags(.recordGrouping))
	struct GroupingAndUngrouping
	{
		let r1 = JournalRecord(date: Day(2025,3,21)!, title: "Record 1", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
		let r2 = JournalRecord(date: Day(2025,3,22)!, title: "Record 2", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
		let r3 = JournalRecord(date: Day(2025,3,22)!, title: "Record 3", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 300)])
		let r4 = JournalRecord(date: Day(2025,3,23)!, title: "Record 4", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
		let r5 = JournalRecord(date: Day(2025,3,23)!, title: "Record 5", debit: [.init(account: TestData.skr04_cost_insurance, value: 500)], credit: [.init(account: TestData.skr04_bank, value: 500)])
		lazy var subGroup = JournalRecord(date: Day(2025,3,21)!, title: "Record 1, …") {
			r1
			r2
			r3
		}
		lazy var group1 = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			r1
			r2
			r3
			r4
			r5
		}
		lazy var group2 = JournalRecord(date: Day(2025,3,20)!, title: "Group") {
			subGroup
			r4
			r5
		}

		@Test("grouping has no effect if there are no sub-records")
		func groupingSingleRecord()
		{
			var record = r1
			#expect(record.group(records: Set([r2.id])) == false)
		}

		@Test("grouping has no effect if there are no matching sub-records")
		mutating func groupingNonExistingSubRecords()
		{
			var g11 = group1
			#expect(g11.group(records: Set([subGroup.id])) == false)
		}

		@Test("grouping existing sub-records")
		mutating func groupingSubRecords()
		{
			var group = group1
			let didGroup = group.group(records: Set([r1.id, r2.id, r3.id]))
			#expect(didGroup)
			#expect(group == group2)
		}

		@Test("ungrouping an existing record with sub-records")
		mutating func ungroupingSubRecords()
		{
			var group = group2
			let didUngroup = group.ungroup(records: Set([subGroup.id]))
			#expect(didUngroup)
			#expect(group.subRecords?.count == 5)
			#expect(group == group1)
		}

		@Test
		mutating func ungroupingRecordsWithoutSubRecords()
		{
			var group = group2
			#expect(group.ungroup(records: Set([r1.id, r5.id])) == false)
		}

		@Test("grouping with empty target set")
		mutating func groupingWithEmptyTargetSet()
		{
			let groupedRecords = groupRecords(withIDs: [], in: [group2])
			#expect(groupedRecords == nil)
		}

		@Test("ungrouping with empty target set")
		mutating func ungroupingWithEmptyTargetSet()
		{
			let ungroupedRecords = ungroupRecords(withIDs: [], in: [group2])
			#expect(ungroupedRecords == nil)
		}
	}

	@Test("removing records")
	func removingRecords()
	{
		let record1 = JournalRecord(date: Day(2025,4,10)!, title: "R1")
		let record2 = JournalRecord(date: Day(2025,4,10)!, title: "R2")
		let record3 = JournalRecord(date: Day(2025,4,10)!, title: "R3", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
		let record4 = JournalRecord(date: Day(2025,4,10)!, title: "R4", debit: [.init(account: TestData.skr04_cost_misc, value: 400)], credit: [.init(account: TestData.skr04_bank, value: 400)])
		let record5 = JournalRecord(date: Day(2025,4,10)!, title: "R5", debit: [.init(account: TestData.skr04_equipment_office, value: 1000)], credit: [.init(account: TestData.skr04_bank, value: 1000)])
		let record6 = JournalRecord(date: Day(2025,4,10)!, title: "R6", debit: [.init(account: TestData.skr04_cost_misc, value: 600)], credit: [.init(account: TestData.skr04_bank, value: 600)])
		let record7 = JournalRecord(date: Day(2025,4,10)!, title: "R7", debit: [.init(account: TestData.skr04_equipment_office, value: 700)], credit: [.init(account: TestData.skr04_bank, value: 700)])

		let group2 = JournalRecord(date: Day(2025,4,10)!, title: "Group2") {
			record3
			JournalRecord(date: Day(2025,4,10)!, title: "Group3") {
				record5
				record6
			}
			record4
		}

		let group1 = JournalRecord(date: Day(2025,4,10)!, title: "Group1") {
			record1
			record2
			group2
		}

		var group11 = group1
		var didRemove = group11.remove(records: [record7.id])
		#expect(didRemove == false)

		let expectedGroup1 = JournalRecord(date: Day(2025,4,10)!, title: "Group1") {
			JournalRecord(date: Day(2025,4,10)!, title: "Group2") {
				record3
				JournalRecord(date: Day(2025,4,10)!, title: "Group3") {
					record5
					record6
				}
				record4
			}
		}

		didRemove = group11.remove(records: [record1.id, record2.id])
		#expect(didRemove == true)
		#expect(group11 == expectedGroup1)

		let expectedGroup2 = JournalRecord(date: Day(2025,4,10)!, title: "Group1") {
			record1
			record2
			JournalRecord(date: Day(2025,4,10)!, title: "Group2") {
				record3
				JournalRecord(date: Day(2025,4,10)!, title: "Group3") {
					record6
				}
				record4
			}
		}

		var group12 = group1
		didRemove = group12.remove(records: [record5.id])
		#expect(didRemove == true)
		#expect(group12 == expectedGroup2)

		let expectedGroup3 = JournalRecord(date: Day(2025,4,10)!, title: "Group1") {
			record1
			record2
		}

		var group13 = group1
		didRemove = group13.remove(records: [group2.id])
		#expect(didRemove == true)
		#expect(group13 == expectedGroup3)
	}

	@Test("Journal record entry comparison")
	func recordEntryComparison()
	{
		let entry1 = JournalRecord.Entry(account: DEGAAPSourceChartItemID("1000")!, value: 50_000)
		let entry2 = JournalRecord.Entry(account: DEGAAPSourceChartItemID("2000")!, value: 50_000)
		let entry3 = JournalRecord.Entry(account: DEGAAPSourceChartItemID("2000")!, value: 10_000)
		#expect(entry1 < entry2)
		#expect((entry2 < entry3) == false)
		#expect((entry2 > entry3) == false)
	}

	@Test("Journal record entry description")
	func recordEntryDescription()
	{
		let entry1 = JournalRecord.Entry(account: DEGAAPSourceChartItemID("1000")!, value: 50_000)
		let entry2 = JournalRecord.Entry(account: DEGAAPSourceChartItemID("1000")!, value: 99)
		#expect(entry1.id == "1000")
		#expect(entry1.id == entry2.id)
	}

	private func _record(_ debitValues : [Money], _ creditValues : [Money]) -> JournalRecord
	{
		JournalRecord(date: .init(2023, 7, 6)!, title: "",
			debit: OrderedSet(debitValues.enumerated().map{
				JournalRecord.Entry(account: .init("1000")!, value: $0.1)}),
			credit: OrderedSet(creditValues.enumerated().map{
				JournalRecord.Entry(account: .init("2000")!, value: $0.1)})
		)
	}

}
