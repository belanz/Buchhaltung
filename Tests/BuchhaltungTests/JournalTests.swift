// Copyright 2020-2025 Kai Oezer

import Foundation
import Testing
@testable import Buchhaltung
import DEGAAP
import TOMLike
import OrderedCollections
import IssueCollection

struct JournalTests
{
	@Test("booking date and time")
  func bookingDateAndTime() throws
  {
		let expectedOutputArchive =
			"""
			chart SKR03

			2020-03-06
			"date/time test"
			d 0480 2,00
			c 1200 2,00
			"""
		let inputArchive =
			"""
			chart SKR03

			2020-10-15
			"date/time test"
			d 0320 3,00
			c 1200 3,00
			"""
		let journal = Journal(records: [Record(
			date: Day(2020, 03, 6)!,
			title: "date/time test",
			debit: [Record.Entry(account:DEGAAPSourceChartItemID("0480")!, value: 2)],
			credit: [Record.Entry(account:DEGAAPSourceChartItemID("1200")!, value: 2)])
			])
		let archivedRecord = try journal.tomlikeEmbedded()
		#expect(archivedRecord == expectedOutputArchive)
		var decodingIssues = IssueCollection()
		let unarchivedJournal = try TOMLikeCoder.decode(Journal.self, from: inputArchive, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		let unarchivedRecord = try #require(unarchivedJournal.records.first)
		#expect(unarchivedRecord.date == Day(2020, 10, 15))
	}

	@Test
	func addingRecords()
	{
		var journal = Journal(records: [], chart: .skr03)
		var record = JournalRecord(date: Day(2021, 10, 1)!, title: "New Record", debit: [], credit: [])
		record.debit.append(Record.Entry(account: .init("1000")!, value: 1000))
		journal.records.append(record)
		#expect(journal.records.count == 1)
		#expect((journal.records.first!).debit.count == 1)
		#expect((journal.records.first!).debit.first!.value == 1000)
	}

	@Test
	func archiving() throws
	{
		let journal = Journal(records: [
			Record(
				date: Day(2019, 11, 6)!,
				title: "Bank deposit for subscribed capital of company",
				debit: [Record.Entry(account: TestData.skr03_bank, value: 7000)],
				credit: [Record.Entry(account: TestData.skr03_equity_subscribed, value: 7000)]),
			Record(
				date: Day(2019, 11, 7)!,
				title: "lawyer and notarization expenses",
				debit: [
					Record.Entry(account: TestData.skr03_receivable_vat_19, value: 10.45),
					Record.Entry(account: TestData.skr03_cost_legal_consulting, value: 55)
				],
				credit: [Record.Entry(account: TestData.skr03_bank, value: 65.45)]),
			Record(
				date: Day(2019, 11, 15)!,
				title: "business registration (Handelsregisteranmeldung)",
				debit: [Record.Entry(account: TestData.skr03_cost_misc, value: 150)],
				credit: [Record.Entry(account: TestData.skr03_bank, value: 150)]),
			Record(
				date: Day(2019, 11, 26)!,
				title: "business registration (Gewerbeanmeldung)",
				debit: [Record.Entry(account: TestData.skr03_cost_misc, value: 15)],
				credit: [Record.Entry(account: TestData.skr03_bank, value: 15)]),
			Record(
				date: Day(2019, 11, 13)!,
				title: "HP LaserJet printer",
				debit: [
					Record.Entry(account: TestData.skr03_receivable_vat_19, value: 19),
					Record.Entry(account: TestData.skr03_equipment_office, value: 100)
				],
				credit: [Record.Entry(account: TestData.skr03_bank, value: 119)]),
			Record(
				date: Day(2019, 11, 13)!,
				title: "HP LaserJet printer warranty extension insurance",
				debit: [Record.Entry(account: TestData.skr03_cost_insurance, value: 12)],
				credit: [Record.Entry(account: TestData.skr03_bank, value: 12)])
		])
		let archivedJournal = try journal.tomlikeEmbedded()
		#expect(archivedJournal == TestData.archivedJournal_startingACompany)
	}

	@Test("unarchiving a simple JournalRecord")
	func unarchivingSimpleRecord() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedSimpleRecord, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal.records.count == 1)
		let record = try #require(journal.records.first)
		#expect(record == TestData.simpleRecord)
	}

	@Test("unarchiving a JournalRecord with multiple entries")
	func unarchivingCompoundRecord() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedCompoundRecord, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal.records.count == 1)
		let record = 	try #require(journal.records.first)
		#expect(record == TestData.compoundRecord)
	}

	@Test("unarchiving a JournalRecord with tags")
	func unarchivingRecordWithTags() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedRecord_tags, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal.records.count == 1)
		let record = try #require(journal.records.first)
		#expect(record.date == Day(2023, 7, 10)!)
		#expect(record.title == "Computer Vision Test Machine")
		#expect(record.tags == Set(["Project GrandSlam", "Building 5", "#101"]))
		#expect(record.tags == Set(["Building 5", "Project GrandSlam", "#101"]))
	}

	@Test("archive a JournalRecord with tags")
	func archivingRecordWithTags() throws
	{
		let journal = Journal(records:[JournalRecord(date: Day(2023, 7, 10)!,
			title: "Computer Vision Test Machine",
			tags: ["#101", "Project GrandSlam", "Building 5"],
			debit: OrderedSet([
				.init(account: TestData.skr03_equipment_office, value: 2000),
				.init(account: TestData.skr03_receivable_vat_19, value: 380)
			]),
			credit: OrderedSet([
				.init(account: TestData.skr03_bank, value: 2380),
			])
		)])
		let encodedJournal = try TOMLikeCoder.encode(journal)
		#expect(encodedJournal == TestData.archivedRecord_tags)
	}

	@Test("archiving a JournalRecord with a note, tags, and links")
	func archivingRecordWithNoteAndLinks() throws
	{
		let journal = Journal(
			records: [
				JournalRecord(date: Day(2023, 7, 10)!,
				title: "Computer Vision Test Machine",
				note: "Payment is evenly split with our project partner.",
				tags: ["#101", "Project GrandSlam", "Building 5"],
				links: [
					Record.Link(url:"file://Volumes/Accounting/Receipts/2023/07/star_computer_23455.pdf"),
					Record.Link(url:"file://Volumes/Legal/Agreements/VisionProject.pdf")
				],
				debit: OrderedSet([
					.init(account: TestData.skr04_equipment_office, value: 2000),
					.init(account: TestData.skr04_receivable_vat_19, value: 380)
				]),
				credit: OrderedSet([
					.init(account: TestData.skr04_bank, value: 2380),
				])
				)
			],
			chart: .skr04
		)
		let encodedJournal = try TOMLikeCoder.encode(journal)
		#expect(encodedJournal == TestData.archivedRecord_tags_note_links)
	}

	@Test("unarchiving a JournalRecord with tags, a note, and links")
	func unarchivingRecordWithNoteAndLinks() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedRecord_tags_note_links, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal.records.count == 1)
		let record = try #require(journal.records.first)
		#expect(record.date == Day(2023, 7, 10)!)
		#expect(record.title == "Computer Vision Test Machine")
		#expect(record.note == "Payment is evenly split with our project partner.")
		#expect(record.links.count == 2)
		#expect(record.links.first!.url == "file://Volumes/Accounting/Receipts/2023/07/star_computer_23455.pdf")
		#expect(record.links.last!.url == "file://Volumes/Legal/Agreements/VisionProject.pdf")
		#expect(record.tags.count == 3)
		#expect(record.tags.contains("Project GrandSlam"))
	}

	@Test("archiving nested groups")
	func archivingNestedGroups() throws
	{
		let archivedJournal = try TOMLikeCoder.encode(TestData.journal_nested_groups)
		#expect(archivedJournal == TestData.archivedJournal_nested_groups)
	}

	@Test("unarchiving nested groups")
	func unarchivingNestedGroups() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedJournal_nested_groups, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal == TestData.journal_nested_groups)
	}

	@Test("flattening nested groups")
	func flatteningNestedGroups() throws
	{
		#expect(TestData.journal_nested_groups.flattened == TestData.journal_flattened_groups)
	}

	@Test("chart mapping")
	func chartMapping() throws
	{
		var decodingIssues = IssueCollection()
		let journal = try TOMLikeCoder.decode(Journal.self, from: TestData.archivedJournal_buyAndSellFixedAsset_IKR, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(journal.records.count == 7)
		#expect(journal.chart == .ikr)
		let embedded = try journal.tomlikeEmbedded()
		#expect(embedded == TestData.archivedJournal_buyAndSellFixedAsset_IKR)
	}

	@Test("adding a simple JournalRecord")
	func addingSimpleRecord() throws
	{
		let journal = Journal(records: [TestData.simpleRecord])
		let archivedJournal = try #require(try? TOMLikeCoder.encode(journal))
		#expect(archivedJournal == TestData.archivedSimpleRecord)
	}

	@Test("adding a JournalRecord with multiple entries")
	func addingCompoundRecord() throws
	{
		let journal = Journal(records: [TestData.compoundRecord])
		let archivedJournal = try #require(try? TOMLikeCoder.encode(journal))
		#expect(archivedJournal == TestData.archivedCompoundRecord)
	}

	@Suite("Grouping Records", .tags(.recordGrouping))
	struct RecordGrouping
	{
		let r1 = JournalRecord(date: Day(2025,3,21)!, title: "Record 1", debit: [.init(account: TestData.skr04_cost_misc, value: 200)], credit: [.init(account: TestData.skr04_bank, value: 200)])
		let r2 = JournalRecord(date: Day(2025,3,22)!, title: "Record 2", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
		let r3 = JournalRecord(date: Day(2025,3,22)!, title: "Record 3", debit: [.init(account: TestData.skr04_cost_insurance, value: 300)], credit: [.init(account: TestData.skr04_bank, value: 300)])
		let r4 = JournalRecord(date: Day(2025,3,23)!, title: "Record 4", debit: [.init(account: TestData.skr04_equipment_office, value: 6_000)], credit: [.init(account: TestData.skr04_bank, value: 6_000)])
		let r5 = JournalRecord(date: Day(2025,3,23)!, title: "Record 5", debit: [.init(account: TestData.skr04_cost_insurance, value: 500)], credit: [.init(account: TestData.skr04_bank, value: 500)])
		lazy var subGroup = JournalRecord(date: Day(2025,3,21)!, title: "Record 1, …") {
			r1
			r2
			r3
		}
		lazy var group1 = JournalRecord(date: Day(2025,3,21)!, title: "Group") {
			r1
			r2
			r3
			r4
			r5
		}
		lazy var group2 = JournalRecord(date: Day(2025,3,21)!, title: "Group") {
			subGroup
			r4
			r5
		}
		lazy var flatJournal = Journal(records: [r1, r2, r3, r4, r5])
		lazy var journal1 = Journal(records: [group1])
		lazy var journal2 = Journal(records: [group2])

		@Test("grouping a single record has no effect")
		func groupingSingleRecord()
		{
			let singleRecordJournal = Journal(records: [r4])
			var groupedSingleRecordJournal = singleRecordJournal
			groupedSingleRecordJournal.groupRecords(withIDs: Set([r4.id]))
			#expect(groupedSingleRecordJournal == singleRecordJournal)
		}

		@Test("grouping multiple records in a flat journal")
		mutating func groupingInFlatJournal() throws
		{
			var groupedJournal = flatJournal
			groupedJournal.groupRecords(withIDs: Set([r1.id, r2.id, r3.id, r4.id, r5.id]))
			try #require(groupedJournal.records.count == 1)
			groupedJournal.records[0].title = "Group"
			#expect(groupedJournal == journal1)
		}

		@Test("grouping records inside a root group")
		mutating func groupingInsideOfGroup() throws
		{
			#expect(journal1 != journal2)
			var journal = journal1
			journal.groupRecords(withIDs: Set([r1.id, r2.id, r3.id]))
			try #require(journal.records.count == 1)
			try #require((journal.records[0].subRecords?.count ?? 0) > 0)
			#expect(journal.records[0].subRecords?.count == 3)
			try #require((journal.records[0].subRecords?[0].subRecords?.count ?? 0) > 0)
			#expect(journal == journal2)
		}

		@Test("ungrouping non-group records has no effect")
		mutating func ungroupingNonGroupRecords()
		{
			var journal = journal2
			journal.ungroupRecords(withIDs: Set([r1.id, r2.id, r3.id]))
			#expect(journal == journal2)
		}

		@Test("ungrouping ")
		mutating func ungroupingGroupRecords() throws
		{
			var journal = journal2
			try #require(journal.records.count == 1)
			#expect(journal.records[0].subRecords?.count == 3)
			journal.ungroupRecords(withIDs: Set([subGroup.id]))
			#expect(journal.records[0].subRecords?.count == 5)
			#expect(journal == journal1)
		}

	}

}
