// Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct IncomeStatementTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedStatement = try #require(try? TOMLikeCoder.encode(TestData.incomeStatement1))
		#expect(archivedStatement == TestData.archivedIncomeStatement1)
		let archivedStatement2 = try #require(try? TOMLikeCoder.encode(TestData.incomeStatement2))
		#expect(archivedStatement2 == TestData.archivedIncomeStatement2)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedStatement = try #require(try? TOMLikeCoder.decode(IncomeStatement.self, from: TestData.archivedIncomeStatement1, issues: &decodingIssues))
		#expect(decodedStatement == TestData.incomeStatement1)
		let decodedStatement2 = try #require(try? TOMLikeCoder.decode(IncomeStatement.self, from: TestData.archivedIncomeStatement2, issues: &decodingIssues))
		#expect(decodedStatement2 == TestData.incomeStatement2)
	}
}
