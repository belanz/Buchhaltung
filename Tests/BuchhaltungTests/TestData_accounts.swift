// Copyright 2025 Kai Oezer

import DEGAAP

extension TestData
{
	static let skr03_equipment_business    = DEGAAPSourceChartItemID("0410")! /// Geschäftsausstattung
	static let skr03_equipment_office      = DEGAAPSourceChartItemID("0420")! /// Büroeinrichtung
	static let skr03_equity_subscribed     = DEGAAPSourceChartItemID("0800")! /// Gezeichnetes Kapital
	static let skr03_bank                  = DEGAAPSourceChartItemID("1200")! /// Bank
	static let skr03_receivable_vat_19     = DEGAAPSourceChartItemID("1576")! /// Abziehbare Vorsteuer 19%
	static let skr03_cost_misc             = DEGAAPSourceChartItemID("2300")! /// Sonstige Aufwendungen
	static let skr03_cost_insurance        = DEGAAPSourceChartItemID("4360")! /// Versicherungen
	static let skr03_cost_legal_consulting = DEGAAPSourceChartItemID("4950")! /// Rechts- und Beratungskosten

	static let skr04_equipment_business    = DEGAAPSourceChartItemID("0635")! /// Geschäftsausstattung
	static let skr04_equipment_office      = DEGAAPSourceChartItemID("0650")! /// Büroeinrichtung
	static let skr04_equity_subscribed     = DEGAAPSourceChartItemID("2900")! /// Gezeichnetes Kapital
	static let skr04_bank                  = DEGAAPSourceChartItemID("1800")! /// Bank
	static let skr04_receivable_vat_19     = DEGAAPSourceChartItemID("1406")! /// Abziehbare Vorsteuer 19%
	static let skr04_cost_misc             = DEGAAPSourceChartItemID("6304")! /// Sonstige Aufwendungen betrieblich und regelmäßig
	static let skr04_cost_insurance        = DEGAAPSourceChartItemID("6400")! /// Versicherungen
	static let skr04_cost_legal_consulting = DEGAAPSourceChartItemID("6825")! /// Rechts- und Beratungskosten
}
