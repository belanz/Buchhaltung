// Copyright 2023-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import DEGAAP
import TOMLike
import IssueCollection

struct TaxonomyTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		#expect(try TOMLikeCoder.encode(DEGAAPTaxonomyVersion.v6_4) == "6.4")
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var issues = IssueCollection()
		#expect(try TOMLikeCoder.decode(DEGAAPTaxonomyVersion.self, from: "6.5", issues: &issues) == .v6_5)
		#expect(throws: (any Error).self) { try TOMLikeCoder.decode(DEGAAPTaxonomyVersion.self, from: "9.99", issues: &issues) }
		#expect(throws: (any Error).self) { try TOMLikeCoder.decode(DEGAAPTaxonomyVersion.self, from: "xyz", issues: &issues) }
	}
}
