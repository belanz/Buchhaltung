// Copyright 2025 Kai Oezer

import Testing
import OrderedCollections
@testable import Buchhaltung
import DEGAAP

struct MiscellaneousTests
{
	@Test
	func transactions() throws
	{
		#expect(Transaction(date: Day(2026, 1, 1)!, value: 5_000) < Transaction(date: Day(2026, 5, 1)!, value: 2_000))
		#expect((Transaction(date: Day(2026, 1, 1)!, value: 5_000) < Transaction(date: Day(2025, 12, 1)!, value: 10_000)) == false)
	}

	@Test
	func recordGroupComparisons() throws
	{
		let group1 = JournalRecord(date: Day(2025, 2, 1)!, title: "A", subRecords: [
			JournalRecord(date: Day(2025, 2, 1)!, title: "A1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 150)])),
			JournalRecord(date: Day(2025, 2, 1)!, title: "A2", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 150)]))
		])
		let group11 = JournalRecord(date: Day(2025, 2, 1)!, title: "A", subRecords: [
			JournalRecord(date: Day(2025, 2, 1)!, title: "A1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 150)])),
			JournalRecord(date: Day(2025, 2, 1)!, title: "A2", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 150)]))
		])
		let group2 = JournalRecord(date: Day(2025, 2, 1)!, title: "B", subRecords: [
			JournalRecord(date: Day(2025, 2, 1)!, title: "B1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 150)])),
			JournalRecord(date: Day(2025, 2, 1)!, title: "B2", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 150)]))
		])
		let group3 = JournalRecord(date: Day(2025, 2, 2)!, title: "C", subRecords: [
			JournalRecord(date: Day(2025, 2, 2)!, title: "CA", subRecords: [
				JournalRecord(date: Day(2025, 2, 2)!, title: "CA1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 10_000)])),
				JournalRecord(date: Day(2025, 2, 2)!, title: "CA2", debit: OrderedSet([JournalRecord.Entry(account: .init("1576")!, value: 1_900)])),
				JournalRecord(date: Day(2025, 2, 4)!, title: "CA3", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 11_900)]))
			]),
			JournalRecord(date: Day(2025, 2, 3)!, title: "CB", subRecords: [
				JournalRecord(date: Day(2025, 2, 3)!, title: "CB1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 10_000)])),
				JournalRecord(date: Day(2025, 2, 3)!, title: "CB2", debit: OrderedSet([JournalRecord.Entry(account: .init("1576")!, value: 1_900)])),
				JournalRecord(date: Day(2025, 2, 4)!, title: "CB3", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 11_900)]))
			])
		])
		let group4 = JournalRecord(date: Day(2025, 2, 2)!, title: "C", subRecords: [
			JournalRecord(date: Day(2025, 2, 2)!, title: "CA", subRecords: [
				JournalRecord(date: Day(2025, 2, 2)!, title: "CA1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 10_000)])),
				JournalRecord(date: Day(2025, 2, 2)!, title: "CA2", debit: OrderedSet([JournalRecord.Entry(account: .init("1576")!, value: 1_900)])),
				JournalRecord(date: Day(2025, 2, 4)!, title: "CA3", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 11_900)]))
			]),
			JournalRecord(date: Day(2025, 5, 3)!, title: "CB", subRecords: [
				JournalRecord(date: Day(2025, 5, 3)!, title: "CB1", debit: OrderedSet([JournalRecord.Entry(account: .init("2300")!, value: 10_000)])),
				JournalRecord(date: Day(2025, 5, 3)!, title: "CB2", debit: OrderedSet([JournalRecord.Entry(account: .init("1576")!, value: 1_900)])),
				JournalRecord(date: Day(2025, 5, 4)!, title: "CB3", credit: OrderedSet([JournalRecord.Entry(account: .init("1200")!, value: 11_900)]))
			])
		])
		#expect(group1 < group2)
		#expect(group1 < group3)
		#expect(group1 == group11)
		#expect(group3 != group4)
	}

	@Test("BH parsing of DEGAAPSourceChart", .tags(.bhCoding))
	func sourceChartBHParsing() throws
	{
		let chartType = try DEGAAPSourceChart("SKR04", strategy: .bhChartSource)
		#expect(chartType == .skr04)
		#expect(throws: BHCodingError.self) {
			try DEGAAPSourceChart.BHFormatStyle().parseStrategy.parse("Blabla")
		}
	}

	@Test("BH formatting of DEGAAPSourceChart", .tags(.bhCoding))
	func sourceChartBHFormatting() throws
	{
		let formattedChartType = DEGAAPSourceChart.skr03.formatted(.bhChartSource)
		#expect(formattedChartType == "SKR03")
	}
}
