// Copyright 2020-2025 Kai Oezer

import Foundation
import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct PeriodTests
{
	@Test("valid period")
	func validPeriod()
	{
		let startDate = Day(2020, 1, 1)!
		let nearEndDate = Day(2020, 12, 31)!
		#expect(Period(start: startDate, end: nearEndDate).validationIssue == nil)
		let farEndDate = Day(2021, 1, 1)!
		#expect(Period(start: startDate, end: farEndDate).validationIssue == .periodExceedsOneYear)
		let invalidEndDate = Day(2019,11,5)!
		#expect(Period(start: startDate, end: invalidEndDate).validationIssue == .endPrecedesStart)
	}

	@Test("initialization with current year")
	func currentYear() throws
	{
		let currentYear = try #require(Period.currentYear)
		#expect(currentYear.interval.contains(Date()))
	}

	@Test("get year of period")
	func year()
	{
		let period = Period(start: Day(2020, 1, 1)!, end: Day(2022, 12, 31)!)
		#expect(period.year == 2022)
	}

	@Test
	func dateRange()
	{
		let startDay = Day(2023,6,1)!
		let endDay = Day(2023,10,13)!
		let range = Period(start: startDay, end: endDay).dateRange
		#expect(range.lowerBound == startDay.start)
		#expect(range.upperBound == endDay.end)

		let maxRange = Period.maxDateRange
		#expect(maxRange.lowerBound == Day.distantPast.start)
		#expect(maxRange.upperBound == Day.distantFuture.end)
	}

	@Test
	func intersection()
	{
		let day1 = Day(2020,3,14)!
		let day2 = Day(2022,9,10)!
		let day3 = Day(2023,4,6)!
		let day4 = Day(2025,2,1)!
		let p1 = Period(start: day1, end: day2)
		let p2 = Period(start: day3, end: day4)
		let p3 = Period(start: day1, end: day3)
		let p4 = Period(start: day2, end: day4)
		#expect(p1.intersects(p2) == false)
		#expect(p3.intersects(p4))
	}

	@Test
	func containsDate()
	{
		let period = Period(start: Day(2024,1,1)!, end: Day(2024,12,31)!)
		let dayInsidePeriod = Day(2024,07,03)!
		let dayOutsidePeriod = Day(2021,05,03)!
		#expect(period.contains(dayInsidePeriod))
		#expect(period.contains(dayOutsidePeriod) == false)
	}

	@Test("string conversion")
	func stringConversion()
	{
		#expect(String(describing:_example1) == _archivedExample1)
	}

	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedPeriod = try TOMLikeCoder.encode(_example1)
		#expect(archivedPeriod == _archivedExample1)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let period = try TOMLikeCoder.decode(Period.self, from: _archivedExample1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(period == _example1)
	}

	private let _example1 = Period(start: Day(2020, 1, 15)!, end: Day(2020, 4, 7)!)

	private let _archivedExample1 = "2020-01-15 - 2020-04-07"
}
