// Copyright 2023-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

@Suite
struct DebtTests
{
	@Test
	func validity()
	{
		#expect(debt1.isValid)
		let invalidDebt1 = Debt(name: "invalid",
			loan: Transaction(date: .distantFuture, value: 1000),
			payments: [Transaction(date: Day(2026,1,1)!, value: 1000)])
		#expect(invalidDebt1.isValid == false)
		let invalidDebt2 = Debt(name: "invalid",
			loan: Transaction(date: Day(2024,1,1)!, value: 1000),
			payments: [Transaction(date: Day(2023,1,1)!, value: 1000)])
		#expect(invalidDebt2.isValid == false)
	}

	@Test
	func archiving() throws
	{
		let debts = Debts(debts: TestData.debts1)
		let archivedDebts = try TOMLikeCoder.encode(debts)
		#expect(archivedDebts == TestData.archivedDebts1)
	}

	@Test
	func unarchiving() throws
	{
		var decodingIssues = IssueCollection()
		let debts = try TOMLikeCoder.decode(Debts.self, from: TestData.archivedDebts1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(debts.count == 1)
		#expect(debts[0] == TestData.debts1[0])
		#expect(debts[0]?.payments.count == 3)
		#expect(debts[0]?.dueDate == Day(2020, 7, 14))
	}

	@Test
	func value()
	{
		#expect(debt1.value(for: Day(2019, 1, 1)!) == .zero)
		#expect(debt1.value(for: Day(2020, 9, 1)!) == 3800)
		#expect(debt1.value(for: Day(2021, 9, 1)!) == 2800)
		#expect(debt1.value(for: Day(2022, 9, 1)!) == 1300)
		#expect(debt1.value(for: Day(2023, 9, 1)!) == .zero)
		#expect(debt2.value(for: Day(2030, 1, 1)!) == debt2.loan.value)
		#expect(debt2.value(for: Day(2020, 1, 1)!) == .zero)
	}

	@Test
	func longTerm()
	{
		#expect(debt1.isLongTerm(for: Day(2021, 12, 31)!))
		#expect(!debt1.isLongTerm(for: Day(2022, 12, 31)!))
		#expect(!debt1.isLongTerm(for: Day(2018, 5, 5)!))

		#expect(debt2.isLongTerm(for: Day(2030, 1, 1)!))
		#expect(debt2.isLongTerm(for: Day(2023, 6, 2)!))
		#expect(!debt2.isLongTerm(for: Day(2023, 1, 1)!))
	}

	@Test
	func shortTerm()
	{
		#expect(debt1.isShortTerm(for: Day(2022, 12, 31)!))
		#expect(!debt1.isShortTerm(for: Day(2021, 12, 31)!))
		#expect(!debt1.isShortTerm(for: Day(2018, 5, 5)!))

		#expect(!debt2.isShortTerm(for: Day(2023, 6, 10)!))
		#expect(!debt2.isShortTerm(for: Day(2023, 1, 1)!))
	}

	@Test
	func totals() throws
	{
		var decodingIssues = IssueCollection()
		let debts = try TOMLikeCoder.decode(Debts.self, from: TestData.archivedDebts2, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(debts.count == 4)
		// dates outside of loan-due range
		#expect(debts.shortTermDebtsTotal(for: Day(2021,1,1)!) == .zero)
		#expect(debts.shortTermDebtsTotal(for: Day(2024,1,1)!) == .zero)
		#expect(debts.longTermDebtsTotal(for: Day(2024,1,1)!) == .zero)
		// no long-term debts (left) on this date
		#expect(debts.longTermDebtsTotal(for: Day(2023,1,1)!) == .zero)
		#expect(debts.longTermDebtsTotal(for: Day(2021,12,31)!) == .zero)
		//
		#expect(debts.shortTermDebtsTotal(for: Day(2023,1,1)!) == 108000)
		#expect(debts.shortTermDebtsTotal(for: Day(2021,12,31)!) == 10000)
		#expect(debts.longTermDebtsTotal(for: Day(2022,6,1)!) == 45000)
		#expect(debts.shortTermDebtsTotal(for: Day(2022,6,1)!) == 5000)
		#expect(debts.longTermDebtsTotal(for: Day(2022,10,1)!) == 96000)
	}

	@Test
	func comparing()
	{
		let debtNoName1 = Debt(loan: .init(date: Day(2025, 1, 1)!, value: 1000))
		let debtNoName2 = Debt(loan: .init(date: Day(2025, 1, 1)!, value: 1000), payments: [.init(date: Day(2026,1,1)!, value: 1000)])
		let debtNoName3 = Debt(loan: .init(date: Day(2025, 1, 2)!, value: 1000))
		let debtWithName1 = Debt(name: "A", loan: .init(date: Day(2025, 1, 1)!, value: 1000), payments: [.init(date: Day(2026,1,1)!, value: 1000)])
		let debtWithName2 = Debt(name: "B", loan: .init(date: Day(2025, 1, 1)!, value: 1000), payments: [.init(date: Day(2026,1,1)!, value: 1000)])
		let debtWithName3 = Debt(name: "B", loan: .init(date: Day(2025, 1, 1)!, value: 1000), payments: [.init(date: Day(2025,12,31)!, value: 1000)])
		#expect(debtNoName1 != debtNoName2)
		#expect((debtNoName1 < debtNoName2) == false)
		#expect((debtNoName1 > debtNoName2) == false)
		#expect(debtNoName1 < debtNoName3)
		#expect(debtWithName1 < debtWithName2)
		#expect(debtNoName1 < debtWithName1)
		#expect(debtNoName1 < debtWithName2)
		#expect(debtWithName3 < debtWithName2)
	}

	@Test
	func debtsInitialization()
	{
		let debts = Debts(debts: Set([debt1, debt2]))
		#expect(debts.count == 2)
	}

	private var debt1 : Debt {
		Debt(loan: .init(date: Day(2020, 4, 15)!, value: 3800),
			payments: [
				.init(date: Day(2021, 5, 1)!, value: 1000),
				.init(date: Day(2022, 5, 1)!, value: 1500),
				.init(date: Day(2023, 5, 1)!, value: 1300)
			])
	}

	@Test
	func debtsComparison()
	{
		let debts1 = Debts(debts: Set([debt1, debt2]))
		let debts2 = Debts(debts: Set([debt1]))
		let debts3 = Debts(debts: Set([debt1, debt2]))
		#expect(debts2 < debts1)
		#expect((debts1 < debts3) == false)
		#expect(debts1 == debts3)
	}

	private var debt2 : Debt {
		Debt(loan: .init(date: Day(2023, 6, 1)!, value: 200))
	}
}

