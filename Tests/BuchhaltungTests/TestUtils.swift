//  Copyright 2020-2025 Kai Oezer

import Foundation

struct TestUtils
{
	static let calendar = Calendar.init(identifier: .gregorian)

	static func date(_ year: Int, _ month: Int, _ day: Int, h hour : Int = 12, m minute : Int = 0, s second : Int = 0) -> Date?
	{
		calendar.date(from: DateComponents(
			timeZone: TimeZone(secondsFromGMT: 3600),
			year: year,
			month: month,
			day: day,
			hour: hour,
			minute: minute,
			second: second)
		)
	}

}
