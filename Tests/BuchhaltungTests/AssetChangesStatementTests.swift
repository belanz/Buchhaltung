// Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import DEGAAP
import IssueCollection

struct AssetChangesStatementTests
{
	private let _degaap = DEGAAPEnvironment()

	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedStatement = try #require(try? TOMLikeCoder.encode(TestData.assetChangesStatement1))
		#expect(archivedStatement == TestData.archivedAssetChangesStatement1)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedStatement = try #require(try? TOMLikeCoder.decode(AssetChangesStatement.self, from: TestData.archivedAssetChangesStatement1, issues: &decodingIssues))
		#expect(decodingIssues.count == 0)
		#expect(decodedStatement == TestData.assetChangesStatement1)
	}

	// swiftlint:disable function_body_length
	@Test("generating asset changes statement")
	func generating() async throws
	{
		var decodingIssues = IssueCollection()
		let assets1 = try #require(try? TOMLikeCoder.decode(Assets.self, from: TestData.archivedAssets3, issues: &decodingIssues))
		#expect(assets1.fixedAssets.count == 2)
		let ledger1 = Ledger(period: .year(2019)!, company: Company(), assets: assets1)
		var issues = IssueCollection()
		let generated1 = try? await ledger1.generateReport(type: .fiscal, using: _degaap, issues: &issues)
		let report1 = try #require(generated1)
		let accountChanges = try #require(report1.notes.assetChangesStatement?.total)
		#expect(accountChanges.acquisitionCosts.begin == 4800)
		#expect(accountChanges.acquisitionCosts.new == 0)
		#expect(accountChanges.acquisitionCosts.retirements == 0)
		#expect(accountChanges.acquisitionCosts.end == 4800)
		#expect(accountChanges.depreciations.begin == 1180)
		#expect(accountChanges.depreciations.new == 1320)
		#expect(accountChanges.depreciations.retirements == 0)
		#expect(accountChanges.depreciations.end == 2500)
		#expect(accountChanges.bookValues.current == 2300)
		#expect(accountChanges.bookValues.previous == 0)
		_checkValues(accountChanges)

		let assets2 = try #require(try? TOMLikeCoder.decode(Assets.self, from: TestData.archivedAssets4, issues: &decodingIssues))
		#expect(assets2.fixedAssets.count == 3)
		let ledger2 = Ledger(period: .year(2020)!, company: Company(), assets: assets2, basedOn: report1)
		issues.clear()
		let generated2 = try? await ledger2.generateReport(type: .fiscal, using: _degaap, issues: &issues)
		let report2 = try #require(generated2)
		let accountChanges2 = try #require(report2.notes.assetChangesStatement?.total)
		#expect(accountChanges2.acquisitionCosts.begin == 4800)
		#expect(accountChanges2.acquisitionCosts.new == 560)
		#expect(accountChanges2.acquisitionCosts.retirements == 1200)
		#expect(accountChanges2.acquisitionCosts.end == 4160) // retired equipment does not contribute
		#expect(accountChanges2.depreciations.begin == Money(2000 /* server */ + 500 /* server rack */))
		#expect(accountChanges2.depreciations.new == Money(20 /* server rack */ + 559 /* tablet */ + 1200 /* server */))
		#expect(accountChanges2.depreciations.retirements == 520 /* server rack */) // the total depreciation of the retired equipment before it got retired
		#expect(accountChanges2.depreciations.end == Money(3200 /* server */ + 559 /* tablet */)) // retired equipment does not contribute
		#expect(accountChanges2.bookValues.current == Money(400) + Assets.FixedAsset.residualValue)
		#expect(accountChanges2.bookValues.previous == Money(2300))
		_checkValues(accountChanges2)

		let ledger3 = Ledger(period: .year(2021)!, company: Company(), assets: assets2, basedOn: report2)
		let generated3 = try? await ledger3.generateReport(type: .fiscal, using: _degaap, issues: &decodingIssues)
		let report3 = try #require(generated3)
		let accountChanges3 = try #require(report3.notes.assetChangesStatement?.total)
		#expect(accountChanges3.acquisitionCosts.begin == accountChanges2.acquisitionCosts.end)
		#expect(accountChanges3.acquisitionCosts.new == 0)
		#expect(accountChanges3.acquisitionCosts.retirements == 0)
		#expect(accountChanges3.acquisitionCosts.end == accountChanges2.acquisitionCosts.end)
		#expect(accountChanges3.depreciations.begin == accountChanges2.depreciations.end)
		#expect(accountChanges3.depreciations.new == 399)
		#expect(accountChanges3.depreciations.retirements == 0)
		#expect(accountChanges3.depreciations.end == Money(3599 + 559))
		#expect(accountChanges3.bookValues.current == Money(1 + 1)) // two fully depriciated items with residual value 1 each
		#expect(accountChanges3.bookValues.previous == accountChanges2.bookValues.current)
		_checkValues(accountChanges3)

		#expect(decodingIssues.count == 0)
	}

	private func _checkValues(_ accountChanges : AssetChangesStatement.AssetAccountChanges)
	{
		#expect(accountChanges.acquisitionCosts.begin + accountChanges.acquisitionCosts.new - accountChanges.acquisitionCosts.retirements == accountChanges.acquisitionCosts.end)
		#expect(accountChanges.depreciations.begin + accountChanges.depreciations.new - accountChanges.depreciations.retirements == accountChanges.depreciations.end)
	}
}
