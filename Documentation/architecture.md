# Architecture Overview

## Core Classes and Serialization

![Core Classes](core.svg)

![Serialization](serialization.svg)

## Report Generation and Exporting

![Business Report](report.svg)

![LaTeX](latex.svg)

## Commandline Interface

![Commandline Interface Classes](cli.svg)
