# Command-Line Tool

In addition to the `Buchhaltung` library, the Buchhaltung package provides the command-line tool `buchhalter`, which processes ledger archive files to create a financial report archive file. The ledger and the report archive files are human-readable text files in the *BH* format, described in the section below.

The path to the input ledger file is a required argument. The following other arguments are optional:

* _output_ : The folder into which the generated files are placed. The current working directory is the default folder.
* _title_ : The title of the generated report. The title *Jahresabschluss* followed by the year of the period closing date.
* _previous-report_ : The path to a previously generated report archive file, from which to extract the results of the previous business period.
* _fiscal_ : A flag for making the program generate a fiscal report instead of the default public (trade) report.
* _inventory_ : A flag for adding an inventory statement to the report.
* _latex_ : A flag for also generating a LaTeX document that can be used to create a paper printable report.
* _myebilanz_ : A flag for also generating [myebilanz](https://www.myebilanz.de) files (the _INI_ file and a _CSV_ file containing account totals).

Run `buchhalter` without arguments or with the *--help* argument to view the built-in usage help text.

## Examples

Processing the *ledger-2021.bh* ledger archive file to generate the *ledger-2021.bhreport* report archive file:
```
buchhalter ledger-2021.bh
```

Using a custom title:
```
buchhalter --title "Special Report 2021" ledger-2021.bh
```

Placing the generated files into a specific folder:
```
buchhalter --output ../reports/ ledger-2021.bh
```

Creating a fiscal report according to the requirements of the tax authority:
```
buchhalter --fiscal ledger-2021.bh
```

Generating an additional LaTeX document (_ledger-2021.tex_):
```
buchhalter --fiscal --latex ledger-2021.bh
```

Using a previously generated report as the results from the previous business period:
```
buchhalter --previous-report ../2020/ledger-2020.bhreport --fiscal ledger-2021.bh
```

Additionally generating the INI and CSV files for submitting the business report online to the tax authority via myebilanz:
```
buchhalter --previous-report ../2020/ledger-2020.bhreport --fiscal --myebilanz ledger-2021.bh
```
